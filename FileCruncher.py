"""
Created on Apr 17, 2013

@author: Kieran

Calls PepperPot.py on a series of images taken in a day, and plots the various values
"""

import pepperpot_kam
import numpy as np
from matplotlib import pyplot as plt

# Specify the day's directory, and the first characters of the image file names
dir_address = "./130508/images/130508"
day = "130508, uncompressed"
# run through the series of images and collect their emittances
starting_img = 35
ending_img = 39
bkgd_imgs = [50, 51, 52]
emittancesX = []
emittancesY = []
slopesX = []
slopesY = []
images = []
# Create the output file and start the headers; the columns are separated by tabs
print([day + " ppot results.txt"])
output = open(day + " ppot results.txt", "w")
output.write(day + "\n")
output.write(
    "Img Num\tX emit\tY emit\tX slope\tY slope\tBeam Mass (pixels)\tBeam Center X\tBeam Center Y"
    + "\tNumber of Blobs Detected\n"
)


def conv_to_str(img_num):
    if img_num < 100:
        if img_num < 10:
            return "00" + str(img_num)
        else:
            return "0" + str(img_num)
    else:
        return str(img_num)


bkgd_strngs = [conv_to_str(bkgd_img_num) for bkgd_img_num in bkgd_imgs]
for img_num in range(starting_img, ending_img + 1):
    img_strng = conv_to_str(img_num)

    print("Accessing file " + dir_address + img_strng + ".SPE")
    images.append(img_num)
    emX, emY, slopeX, slopeY, mass, centerX, centerY, numBlobs = pepperpot_kam.crunch(
        [dir_address + img_strng + ".SPE"],
        [dir_address + bkgd_img_strng + ".SPE" for bkgd_img_strng in bkgd_strngs],
    )
    output.write(
        str(img_num)
        + "\t"
        + str(emX)
        + "\t"
        + str(emY)
        + "\t"
        + str(slopeX)
        + "\t"
        + str(slopeY)
        + "\t"
        + str(mass)
        + "\t"
        + str(centerX)
        + "\t"
        + str(centerY)
        + "\t"
        + str(numBlobs)
        + "\n"
    )
