"""NDCXII python module

This module contains python routines to make handling
NDCXII data easier. It allows reading and writing to the
cassandra database. Uploading new settings to the controls.
It also provides interfaces to codes like warp.
"""

import matplotlib.pyplot as plt
import sys
import os
import pkg_resources

import logging

from . import db
from . import data
from . import readSPE
from . import helper
from . import control

logging.basicConfig(level=logging.CRITICAL)

# control should be imported manually
__all__ = ["db", "data", "readSPE", "helper", "control"]

# patch figure to always include the git version
oldfigure = plt.figure

# define in setup.py (this way it can access the git version easily)
# and gets the correct version when setup.py is run and we don't have to use
# commit hooks to update a file
version = pkg_resources.require("NDCXII")[0].version
__version__ = version


def print_versions():
    """Make debugging easier"""
    print("modules used:")
    print("python      ", sys.version)
    for lib in [data.np, data.scipy, control.zmq, data.matplotlib, db.cassandra]:
        print("{:12s} {} ".format(lib.__name__, lib.__version__))


def newfigure(*args, **kwargs):
    """Add the git version and commandline to the current plot"""
    ret = oldfigure(*args, **kwargs)
    cmd = " ".join(sys.argv)
    if cmd.startswith("/usr/bin/"):
        cmd = cmd[9:]
    cmdline = " cmdline: " + cmd
    if (
        "EMACS" in os.environ
        or "INSIDE_EMACS" in os.environ
        or "ipykernel_launcher" in cmdline
    ):
        info = version
    else:
        info = version + cmdline
    if ret:
        plt.figtext(0.01, 0.01, info, size=10)
    return ret


plt.figure = newfigure
