import scipy.constants as SC
from datetime import datetime
from collections import namedtuple
from .db import get_setting, get_comment_from_shot
import matplotlib.pyplot as plt
from functools import lru_cache


@lru_cache(maxsize=1000)
def species(shotnumber):
    """Return the ion species for this shot"""
    if shotnumber > 3636485291:  # started using species in comments
        c = get_comment_from_shot(shotnumber)
        if c is not None:
            return c.species
        else:
            print(f"[Error] error for sn= {shotnumber} Assuming Helium")
            return "He"
    print(
        "Warning: species might be incorrect. Overwrite NDCXII.config.species function if needed"
    )
    if shotnumber > 3600000000:
        print("Assuming H")
        return "H"
    else:
        print("Assuming He")
        return "He"


@lru_cache(maxsize=1000)
def mass(shotnumber):
    """Return the mass of the ion used in kg"""
    s = species(shotnumber)
    if s == "H":
        mass = SC.m_p  # protons
    elif s == "He":
        mass = SC.physical_constants["alpha particle mass"][0]  # helium
    else:
        print(f"Warning: unkonwn species {s} in {sn}. Assuming helium mass")
        mass = SC.physical_constants["alpha particle mass"][0]  # helium
    return mass


def config(shotnumber, devicename, species="He"):
    """Return the configuration of a device for a certain shotnumber

    devices: list of PXI units
    position: beam line position measured from source
    tcable: time delay due to cable from diagnostic to scope. See logbook p. 114 (2016-04-16)
    cell: beam line cell
    attenuator: <1 means the signal has a gain, e.g. 10 = 10x attenuator, 0.5 = 2x gain
    calibration: convertion factor from measured volt to unit
    unit: string of final unit (after gain and calibration have been applied)
    """
    ret = {
        "settinghash": [],
        "settings": [],
        "tcable": 0,
        "attenuator": 1.0,
        "calibration": 1.0,
    }
    if devicename == "BPM1":
        ret["devices"] = [PXI(3, 8, 0), PXI(3, 8, 1), PXI(3, 9, 0), PXI(3, 9, 1)]
        ret["position"] = 1.276  # in [m]
        ret["tcable"] = [132.2e-9, 132.2e-9, 132.2e-9, 132.2e-9]
        ret["timing"] = [
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
        ]
        ret["cell"] = 3
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3541523906:
            ret["attenuator"] = 0.5 * 1.0  # 0.5 from box, 1 from attenuator
        elif shotnumber > 3518295866:
            ret["attenuator"] = 0.1 * 5.0  # 0.1 from box, 5 from attenuator
        else:
            ret["attenuator"] = 0.1 * 1
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [1.0e-6, 1.7e-6]
        ret["window-compressed"] = [0.8e-6, 2.45e-6]
        ret["window-Li-uncompressed"] = [1.3e-6, 2.5e-6]
        ret["window-K-uncompressed"] = [2.3e-6, 3.5e-6]
    elif devicename == "BPM2":
        ret["devices"] = [PXI(3, 10, 0), PXI(3, 10, 1), PXI(3, 11, 0), PXI(3, 11, 1)]
        ret["position"] = 2.393  # in [m]
        ret["tcable"] = [130.7e-9, 130.7e-9, 130.2e-9, 131.2e-9]
        ret["timing"] = [
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
        ]
        ret["cell"] = 7
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3518356796:
            ret["attenuator"] = 0.1 * 2.0
        elif shotnumber > 3518295866:
            ret["attenuator"] = 0.1 * 1.0
        elif shotnumber > 3456889200:
            ret["attenuator"] = 0.1
        else:
            ret["attenuator"] = 0.5
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [1.6e-6, 2.2e-6]
        ret["window-compressed"] = [1.0e-6, 2.9e-6]
        ret["window-Li-uncompressed"] = [2.0e-6, 3.2e-6]
        ret["window-K-uncompressed"] = [3.5e-6, 5.0e-6]
    elif devicename == "BPM3":
        ret["devices"] = [PXI(3, 12, 0), PXI(3, 12, 1), PXI(3, 14, 0), PXI(3, 14, 1)]
        ret["position"] = 3.511  # in [m]
        ret["tcable"] = [133.2e-9, 133.2e-9, 133.2e-9, 133.2e-9]
        ret["timing"] = [
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 1, 0)},
            {"aux timing": Timing(3, 2, 0)},
            {"aux timing": Timing(3, 2, 0)},
        ]
        ret["cell"] = 11
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3541523906:
            ret["attenuator"] = 0.1 * 2.0
        elif shotnumber > 3518457788:
            ret["attenuator"] = 0.1 * 10.0
        elif shotnumber > 3453778800:
            ret["attenuator"] = 0.1 * 1
        else:
            ret["attenuator"] = 0.5 * 1
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [2.0e-6, 2.9e-6]
        ret["window-compressed"] = [1.3e-6, 3.75e-6]
        ret["window-Li-uncompressed"] = [2.5e-6, 3.9e-6]
        ret["window-K-uncompressed"] = [4.6e-6, 7.0e-6]
    elif devicename == "BPM4":
        ret["devices"] = [PXI(4, 4, 0), PXI(4, 4, 1), PXI(4, 5, 0), PXI(4, 5, 1)]
        ret["position"] = 4.908  # in [m]
        ret["tcable"] = [96.7e-9, 96.7e-9, 96.2e-9, 97.2e-9]
        ret["timing"] = [
            {"aux timing": Timing(4, 0, 0)},
            {"aux timing": Timing(4, 0, 0)},
            {"aux timing": Timing(4, 0, 0)},
            {"aux timing": Timing(4, 0, 0)},
        ]
        ret["cell"] = 16
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        else:
            ret["attenuator"] = 0.5 * 1
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [2.5e-6, 3.5e-6]
        ret["window-compressed"] = [1.57e-6, 4.5e-6]
        ret["window-Li-uncompressed"] = [3.3e-6, 4.5e-6]
        ret["window-K-uncompressed"] = [6.3e-6, 10.0e-6]
    elif devicename == "BPM5":
        ret["devices"] = [PXI(4, 6, 0), PXI(4, 6, 1), PXI(4, 8, 0), PXI(4, 8, 1)]
        ret["position"] = 6.026  # in [m]
        ret["tcable"] = [98.6e-9, 98.6e-9, 99.2e-9, 98.0e-9]
        ret["timing"] = [
            {"aux timing": Timing(4, 0, 0)},
            {"aux timing": Timing(4, 0, 0)},
            {"aux timing": Timing(4, 1, 0)},
            {"aux timing": Timing(4, 1, 0)},
        ]
        ret["cell"] = 20
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3541523906:
            ret["attenuator"] = 0.5 * 1
        elif shotnumber > 3456889200:
            ret["attenuator"] = 1.0
        else:
            ret["attenuator"] = 10.0
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [3.0e-6, 4.0e-6]
        ret["window-compressed"] = [1.78e-6, 5.3e-6]
        ret["window-Li-uncompressed"] = [3.8e-6, 4.9e-6]
        ret["window-K-uncompressed"] = [7.5e-6, 10.0e-6]
    elif devicename == "BPM6":
        ret["devices"] = [PXI(4, 9, 0), PXI(4, 9, 1), PXI(4, 10, 0), PXI(4, 10, 1)]
        ret["position"] = 7.143  # in [m]
        ret["tcable"] = [97.0e-9, 97.0e-9, 96.0e-9, 98.0e-9]
        ret["timing"] = [
            {"aux timing": Timing(4, 1, 0)},
            {"aux timing": Timing(4, 1, 0)},
            {"aux timing": Timing(4, 1, 0)},
            {"aux timing": Timing(4, 1, 0)},
        ]
        ret["cell"] = 24
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3541523906:
            ret["attenuator"] = 0.5 * 1
        elif shotnumber > 3526587933:
            ret["attenuator"] = 2.0
        elif shotnumber > 3505851932:
            ret["attenuator"] = 1.0
        else:
            ret["attenuator"] = 10.0
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 0.5 * SC.inch
        ret["capacitance"] = 25e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [3.3e-6, 4.5e-6]
        # ret["window-compressed"] = [2.0e-6, 5.6e-6]
        if shotnumber > 3650546528:
            if species == "He":
                ret["window-compressed"] = [2.0e-6, 5.6e-6]
            elif species == "H":
                ret["window-compressed"] = [2.05e-6, 2.15e-6]
            else:
                print("Warning: unkonwn species")
        else:
            ret["window-compressed"] = [2.0e-6, 5.6e-6]

        ret["window-Li-uncompressed"] = [4.3e-6, 5.5e-6]
        ret["window-K-uncompressed"] = [7.2e-6, 10.0e-6]
    elif devicename == "BPM7":
        if shotnumber > 3649192606:
            ret["devices"] = [
                PXI(4, 17, 0),
                PXI(4, 17, 1),
                PXI(4, 16, 0),
                PXI(4, 16, 1),
            ]
        elif shotnumber > 3498796800:
            ret["devices"] = [
                PXI(4, 17, 0),
                PXI(4, 17, 1),
                PXI(4, 18, 0),
                PXI(4, 18, 1),
            ]
        else:
            ret["devices"] = [
                PXI(4, 16, 0),
                PXI(4, 17, 0),
                PXI(4, 17, 1),
                PXI(4, 18, 0),
            ]
        ret["position"] = 8.3074  # in [m]
        ret["tcable"] = [119.0e-9, 121.0e-9, 124.0e-9, 119.0e-9]
        ret["timing"] = [
            {"aux timing": Timing(4, 2, 0)},
            {"aux timing": Timing(4, 2, 0)},
            {"aux timing": Timing(4, 2, 0)},
            {"aux timing": Timing(4, 2, 0)},
        ]
        ret["cell"] = 28
        if shotnumber > 3542036466:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"][0])
        elif shotnumber > 3526587933:
            ret["attenuator"] = 10.0
        elif shotnumber > 3522612029:
            ret["attenuator"] = 2.0
        elif shotnumber > 3518356796:
            ret["attenuator"] = 1.0
        elif shotnumber > 3500932038:
            ret["attenuator"] = 2.0
        elif shotnumber < 3456889200:
            ret["attenuator"] = None  # not installed
        else:
            ret["attenuator"] = None
        ret["radius"] = 4.3e-2  # 4.167e-2 # ~4cm radius
        ret["length"] = 1.5 * 0.5 * SC.inch
        ret["capacitance"] = 100e-12  # Farads, button capacitance to gnd
        ret["zterm"] = 50  # Ohm
        ret["window-uncompressed"] = [3.5e-6, 5.0e-6]
        if species == "H":
            ret["window-compressed"] = [2.0e-6, 2.16e-6]
        elif species == "He":
            ret["window-compressed"] = [3e-6, 3.4e-6]
        else:
            print("Warning: unkonwn species")
        ret["window-Li-uncompressed"] = [5.0e-6, 6.2e-6]
        ret["window-K-uncompressed"] = None  # only installed after K runs
    elif devicename == "Injector":
        ret["devices"] = PXI(0, 9, 0)
        ret["timing"] = {
            "trigger": Timing(0, 1, 1),
            "reset": Timing(0, 1, 0),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 0.0  # in [m]
        ret["tcable"] = 168e-9
        ret["cell"] = 0
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 10.0
        ret["calibration"] = 10100.0  # 1V = 10100 V
    elif devicename == "Cell1":
        ret["devices"] = PXI(0, 9, 1)
        ret["timing"] = {
            "trigger": Timing(0, 1, 3),
            "reset": Timing(0, 1, 2),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 0.716  # in [m]
        ret["tcable"] = 182e-9  # not measrued, used Cell2 value
        ret["cell"] = 1
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell2":
        ret["devices"] = PXI(0, 10, 0)
        ret["timing"] = {
            "trigger": Timing(0, 2, 1),
            "reset": Timing(0, 2, 0),
            "aux timing": Timing(0, 2, 0),
        }
        ret["tcable"] = 182e-9
        ret["position"] = 0.995  # in [m]
        ret["cell"] = 2
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell3":
        ret["devices"] = PXI(0, 10, 1)
        ret["timing"] = {
            "trigger": Timing(0, 2, 3),
            "reset": Timing(0, 2, 2),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 2.113  # in [m]
        ret["tcable"] = 181e-9
        ret["cell"] = 6
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell4":
        ret["devices"] = PXI(0, 11, 0)
        ret["timing"] = {
            "trigger": Timing(0, 3, 1),
            "reset": Timing(0, 3, 0),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 3.230  # in [m]
        ret["tcable"] = 178e-9
        ret["cell"] = 10
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell5":
        ret["devices"] = PXI(0, 11, 1)
        ret["timing"] = {
            "trigger": Timing(0, 3, 3),
            "reset": Timing(0, 3, 2),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 4.627  # in [m]
        ret["tcable"] = 178e-9
        ret["cell"] = 15
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        elif (
            shotnumber > 3510676140
        ):  # this is only roughly estimated from the git timestamp
            ret["attenuator"] = 4
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell6":
        ret["devices"] = PXI(0, 12, 0)
        ret["timing"] = {
            "trigger": Timing(0, 4, 1),
            "reset": Timing(0, 4, 0),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 5.186  # in [m]
        ret["tcable"] = 178e-9
        ret["cell"] = 17
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        elif (
            shotnumber > 3510676140
        ):  # this is only roughly estimated from the git timestamp
            ret["attenuator"] = 4
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    elif devicename == "Cell7":
        ret["devices"] = PXI(0, 12, 1)
        ret["timing"] = {
            "trigger": Timing(0, 4, 3),
            "reset": Timing(0, 4, 2),
            "aux timing": Timing(0, 2, 0),
        }
        ret["position"] = 5.745  # in [m]
        ret["tcable"] = 178e-9
        ret["cell"] = 19
        if shotnumber > 3550775916:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        elif (
            shotnumber > 3510676140
        ):  # this is only roughly estimated from the git timestamp
            ret["attenuator"] = 4
        else:
            ret["attenuator"] = 2
        ret["calibration"] = -10000.0
    # TODO: check distances for BL
    elif devicename == "BL1":
        ret["devices"] = PXI(4, 12, 0)
        ret["timing"] = {"trigger": Timing(4, 0, 0), "aux timing": Timing(4, 1, 0)}
        ret["position"] = 6.5847  # in [m]
        ret["tcable"] = 104e-9
        ret["cell"] = 22
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "BL1-chargeV":
        ret["devices"] = PXI(5, 4, 0)
        ret["timing"] = {"trigger": Timing(5, 0, 0), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 6.5847  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 22
        ret["attenuator"] = 10.0
        ret["calibration"] = 1.0
    elif devicename == "BL1-chargeI":
        ret["devices"] = PXI(5, 5, 0)
        ret["timing"] = {"trigger": Timing(5, 0, 0), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 6.5847  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 22
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
    elif devicename == "BL2":
        ret["devices"] = PXI(4, 12, 1)
        ret["timing"] = {"trigger": Timing(4, 0, 1), "aux timing": Timing(4, 1, 0)}
        ret["position"] = 6.8641  # in [m]
        ret["tcable"] = 103e-9
        ret["cell"] = 23
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "BL2-chargeV":
        ret["devices"] = PXI(5, 4, 1)
        ret["timing"] = {"trigger": Timing(5, 0, 1), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 6.8641  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 23
        ret["attenuator"] = 10.0
        ret["calibration"] = 1.0
    elif devicename == "BL2-chargeI":
        ret["devices"] = PXI(5, 5, 1)
        ret["timing"] = {"trigger": Timing(5, 0, 1), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 6.8641  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 23
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
    elif devicename == "BL3":
        ret["devices"] = PXI(4, 14, 0)
        ret["timing"] = {"trigger": Timing(4, 0, 2), "aux timing": Timing(4, 2, 0)}
        ret["position"] = 7.4229  # in [m]
        ret["tcable"] = 105e-9
        ret["cell"] = 25
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "BL3-chargeV":
        ret["devices"] = PXI(5, 4, 2)
        ret["timing"] = {"trigger": Timing(5, 0, 2), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.4229  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 25
        ret["attenuator"] = 10.0
        ret["calibration"] = 1.0
    elif devicename == "BL3-chargeI":
        ret["devices"] = PXI(5, 5, 2)
        ret["timing"] = {"trigger": Timing(5, 0, 2), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.4229  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 25
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
    elif devicename == "BL4":
        ret["devices"] = PXI(4, 14, 1)
        ret["timing"] = {"trigger": Timing(4, 0, 3), "aux timing": Timing(4, 2, 0)}
        ret["position"] = 7.7023  # in [m]
        ret["tcable"] = 107e-9
        ret["cell"] = 26
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "BL4-chargeV":
        ret["devices"] = PXI(5, 4, 3)
        ret["timing"] = {"trigger": Timing(5, 0, 3), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.7023  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 26
        ret["attenuator"] = 10.0
        ret["calibration"] = 1.0
    elif devicename == "BL4-chargeI":
        ret["devices"] = PXI(5, 5, 3)
        ret["timing"] = {"trigger": Timing(5, 0, 3), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.7023  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 26
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
    elif devicename == "BL5":
        ret["devices"] = PXI(4, 15, 0)
        ret["timing"] = {"trigger": Timing(4, 1, 0), "aux timing": Timing(4, 2, 0)}
        ret["position"] = 7.9817  # in [m]
        ret["tcable"] = 104e-9
        ret["cell"] = 27
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "BL5-chargeV":
        ret["devices"] = PXI(5, 4, 4)
        ret["timing"] = {"trigger": Timing(5, 1, 0), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.9817  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 27
        ret["attenuator"] = 10.0
        ret["calibration"] = 1.0
    elif devicename == "BL5-chargeI":
        ret["devices"] = PXI(5, 5, 4)
        ret["timing"] = {"trigger": Timing(5, 1, 0), "aux timing": Timing(5, 0, 0)}
        ret["position"] = 7.9817  # in [m]
        ret["tcable"] = 104e-9  # TODO: need to measure and fix this
        ret["cell"] = 27
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
    elif devicename == "Imon1":
        ret["devices"] = PXI(3, 5, 0)
        ret["timing"] = {"aux timing": Timing(3, 0, 0)}
        ret["position"] = 1.554  # in [m]
        ret["tcable"] = 129e-9
        ret["cell"] = 4
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 543501.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [0.55e-6, 2.35e-6]
            ret["window-uncompressed"] = [0.1e-6, 2.1e-6]
        else:
            ret["window-compressed"] = [0.85e-6, 2.65e-6]
            ret["window-uncompressed"] = [0.4e-6, 2.4e-6]
    elif devicename == "Imon2":
        ret["devices"] = PXI(3, 5, 1)
        ret["timing"] = {"aux timing": Timing(3, 0, 0)}
        ret["position"] = 1.833  # in [m]
        ret["tcable"] = 128e-9
        ret["cell"] = 5
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 614702.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [0.6e-6, 2.45e-6]
            ret["window-uncompressed"] = [0.3e-6, 2.3e-6]
        else:
            ret["window-compressed"] = [0.95e-6, 2.8e-6]
            ret["window-uncompressed"] = [0.6e-6, 2.6e-6]
    elif devicename == "Imon3":
        ret["devices"] = PXI(3, 6, 0)
        ret["timing"] = {"aux timing": Timing(3, 0, 0)}
        ret["position"] = 2.672  # in [m]
        ret["tcable"] = 129e-9
        ret["cell"] = 8
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 487816.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [0.94e-6, 3.0e-6]
            ret["window-uncompressed"] = [0.8e-6, 2.49e-6]
        else:
            ret["window-compressed"] = [1.14e-6, 3.2e-6]
            ret["window-uncompressed"] = [1.0e-6, 2.69e-6]
    elif devicename == "Imon4":
        ret["devices"] = PXI(3, 6, 1)
        ret["timing"] = {"aux timing": Timing(3, 0, 0)}
        ret["position"] = 2.951  # in [m]
        ret["tcable"] = 129e-9
        ret["cell"] = 9
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 509388.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [1.01e-6, 3.4e-6]
            ret["window-uncompressed"] = [0.9e-6, 2.8e-6]
        else:
            ret["window-compressed"] = [1.21e-6, 3.6e-6]
            ret["window-uncompressed"] = [1.1e-6, 3.0e-6]
    elif devicename == "Imon5":
        ret["devices"] = PXI(3, 15, 0)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 3.789  # in [m]
        ret["tcable"] = 131e-9
        ret["cell"] = 12
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 524931.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [1.15e-6, 4.0e-6]
            ret["window-uncompressed"] = [1.3e-6, 3.2e-6]
        else:
            ret["window-compressed"] = [1.35e-6, 4.2e-6]
            ret["window-uncompressed"] = [1.5e-6, 3.4e-6]
    elif devicename == "Imon6":
        ret["devices"] = PXI(3, 15, 1)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 4.348  # in [m]
        ret["tcable"] = 132e-9
        ret["cell"] = 14
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 564513.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [1.3e-6, 4.15e-6]
            ret["window-uncompressed"] = [1.5e-6, 3.3e-6]
        else:
            ret["window-compressed"] = [1.5e-6, 4.35e-6]
            ret["window-uncompressed"] = [1.7e-6, 3.5e-6]
    elif devicename == "Imon7":
        ret["devices"] = PXI(3, 16, 0)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 5.466  # in [m]
        ret["tcable"] = 132e-9  # not measured, used Imon6 value
        ret["cell"] = 18
        ret["attenuator"] = 5.0
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 512635.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [1.5e-6, 4.4e-6]
            ret["window-uncompressed"] = [2.0e-6, 3.3e-6]
        else:
            ret["window-compressed"] = [1.7e-6, 4.6e-6]
            ret["window-uncompressed"] = [2.2e-6, 3.5e-6]
    elif devicename == "Imon8":
        ret["devices"] = PXI(3, 16, 1)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 6.304  # in [m]
        ret["tcable"] = 131e-9
        ret["cell"] = 21
        ret["attenuator"] = 5.0
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 479724.0
        if shotnumber > 3600000000:
            ret["window-compressed"] = [1.66e-6, 5.2e-6]
            ret["window-uncompressed"] = [2.4e-6, 3.8e-6]
        else:
            ret["window-compressed"] = [1.86e-6, 5.4e-6]
            ret["window-uncompressed"] = [2.6e-6, 4.0e-6]
    elif devicename == "Imon9":
        ret["devices"] = PXI(3, 17, 0)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 6.863  # in [m]
        ret["tcable"] = 132e-9  # not measure, used Imon6 value
        ret["cell"] = 23
        ret["attenuator"] = 5.0
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 452829.0
        ret["window-compressed"] = [9.5e-6, 11.5e-6]
        ret["window-uncompressed"] = [9.5e-6, 11.5e-6]
    elif devicename == "Imon10":
        ret["devices"] = PXI(3, 17, 1)
        ret["timing"] = {"aux timing": Timing(3, 2, 0)}
        ret["position"] = 7.701  # in [m]
        ret["tcable"] = 132e-9  # not measure, used Imon6 value
        ret["cell"] = 26
        ret["attenuator"] = 5.0
        ret["zterm"] = 4.97  # Ohm
        ret["LRconstant"] = 1e6 / 550618.0
        ret["window-compressed"] = [10.0e-6, 12.9e-6]
        ret["window-uncompressed"] = [10.0e-6, 12.9e-6]
    elif devicename == "InjImon":
        ret["devices"] = PXI(3, 4, 0)
        ret["timing"] = {"aux timing": Timing(3, 0, 0)}
        ret["position"] = 0.32415  # in [m]
        ret["tcable"] = 175e-9
        ret["length"] = 0.033
        ret["radius"] = 0.066
        ret["cell"] = 0
        ret["calibration"] = 0.1
        ret["window-compressed"] = [0.0e-6, 2.4e-6]
        ret["window-uncompressed"] = [0.0e-6, 2.4e-6]
    elif devicename == "FC":
        ret["devices"] = [PXI(4, 11, 0), PXI(4, 11, 1)]
        ret["position"] = 9.851  # in [m]
        ret["cell"] = 27
        ret["attenuator"] = 10.0
        ret["calibration"] = -35000.0
    elif devicename == "SRK01":
        ret["devices"] = PXI(1, 9, 0)
        ret["timing"] = {"trigger": Timing(1, 0, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 0.5797  # in [m]
        ret["cell"] = 0
    elif devicename == "SRK02":
        ret["devices"] = PXI(1, 9, 1)
        ret["timing"] = {"trigger": Timing(1, 0, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 0.8580  # in [m]
        ret["cell"] = 1
    elif devicename == "SRK03":
        ret["devices"] = PXI(1, 9, 2)
        ret["timing"] = {"trigger": Timing(1, 0, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 1.1374  # in [m]
        ret["cell"] = 2
    elif devicename == "SRK04":
        ret["devices"] = PXI(1, 9, 3)
        ret["timing"] = {"trigger": Timing(1, 0, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 1.4181  # in [m]
        ret["cell"] = 3
    elif devicename == "SRK05":
        ret["devices"] = PXI(1, 9, 4)
        ret["timing"] = {"trigger": Timing(1, 1, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 1.6975  # in [m]
        ret["cell"] = 4
    elif devicename == "SRK06":
        ret["devices"] = PXI(1, 9, 5)
        ret["timing"] = {"trigger": Timing(1, 1, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 1.9769  # in [m]
        ret["cell"] = 5
    elif devicename == "SRK07":
        ret["devices"] = PXI(1, 9, 6)
        ret["timing"] = {"trigger": Timing(1, 1, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 2.2563  # in [m]
        ret["cell"] = 6
    elif devicename == "SRK08":
        ret["devices"] = PXI(1, 9, 7)
        ret["timing"] = {"trigger": Timing(1, 1, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 2.5357  # in [m]
        ret["cell"] = 7
    elif devicename == "SRK09":
        ret["devices"] = PXI(1, 10, 0)
        ret["timing"] = {"trigger": Timing(1, 2, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 2.8151  # in [m]
        ret["cell"] = 8
    elif devicename == "SRK10":
        ret["devices"] = PXI(1, 10, 1)
        ret["timing"] = {"trigger": Timing(1, 2, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 3.0945  # in [m]
        ret["cell"] = 9
    elif devicename == "SRK11":
        ret["devices"] = PXI(1, 10, 2)
        ret["timing"] = {"trigger": Timing(1, 2, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 3.3739  # in [m]
        ret["cell"] = 10
    elif devicename == "SRK12":
        ret["devices"] = PXI(1, 10, 3)
        ret["timing"] = {"trigger": Timing(1, 2, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 3.6533  # in [m]
        ret["cell"] = 11
    elif devicename == "SRK13":
        ret["devices"] = PXI(1, 10, 4)
        ret["timing"] = {"trigger": Timing(1, 3, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 3.9327  # in [m]
        ret["cell"] = 12
    elif devicename == "SRK14":
        ret["devices"] = PXI(1, 10, 5)
        ret["timing"] = {"trigger": Timing(1, 3, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 4.2121  # in [m]
        ret["cell"] = 13
    elif devicename == "SRK15":
        ret["devices"] = PXI(1, 10, 6)
        ret["timing"] = {"trigger": Timing(1, 3, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 4.4915  # in [m]
        ret["cell"] = 14
    elif devicename == "SRK16":
        ret["devices"] = PXI(1, 10, 7)
        ret["timing"] = {"trigger": Timing(1, 3, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 4.7709  # in [m]
        ret["cell"] = 15
    elif devicename == "SRK17":
        ret["devices"] = PXI(1, 11, 0)
        ret["timing"] = {"trigger": Timing(1, 4, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 5.0503  # in [m]
        ret["cell"] = 16
    elif devicename == "SRK18":
        ret["devices"] = PXI(1, 11, 1)
        ret["timing"] = {"trigger": Timing(1, 4, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 5.3297  # in [m]
        ret["cell"] = 17
    elif devicename == "SRK19":
        ret["devices"] = PXI(1, 11, 2)
        ret["timing"] = {"trigger": Timing(1, 4, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 5.6091  # in [m]
        ret["cell"] = 18
    elif devicename == "SRK20":
        ret["devices"] = PXI(1, 11, 3)
        ret["timing"] = {"trigger": Timing(1, 4, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 5.8885  # in [m]
        ret["cell"] = 19
    elif devicename == "SRK21":
        ret["devices"] = PXI(1, 11, 4)
        ret["timing"] = {"trigger": Timing(1, 5, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 6.1679  # in [m]
        ret["cell"] = 20
    elif devicename == "SRK22":
        ret["devices"] = PXI(1, 11, 5)
        ret["timing"] = {"trigger": Timing(1, 5, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 6.4473  # in [m]
        ret["cell"] = 21
    elif devicename == "SRK23":
        ret["devices"] = PXI(1, 11, 6)
        ret["timing"] = {"trigger": Timing(1, 5, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 6.7267  # in [m]
        ret["cell"] = 22
    elif devicename == "SRK24":
        ret["devices"] = PXI(1, 11, 7)
        ret["timing"] = {"trigger": Timing(1, 5, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 7.0061  # in [m]
        ret["cell"] = 23
    elif devicename == "SRK25":
        ret["devices"] = PXI(1, 12, 0)
        ret["timing"] = {"trigger": Timing(1, 6, 0), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 7.2855  # in [m]
        ret["cell"] = 24
    elif devicename == "SRK26":
        ret["devices"] = PXI(1, 12, 1)
        ret["timing"] = {"trigger": Timing(1, 6, 1), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 7.5649  # in [m]
        ret["cell"] = 25
    elif devicename == "SRK27":
        ret["devices"] = PXI(1, 12, 2)
        ret["timing"] = {"trigger": Timing(1, 6, 2), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 7.8443  # in [m]
        ret["cell"] = 26
    elif devicename == "SRK28":
        ret["devices"] = PXI(1, 12, 3)
        ret["timing"] = {"trigger": Timing(1, 6, 3), "aux timing": Timing(1, 0, 0)}
        ret["position"] = 8.1237  # in [m]
        ret["cell"] = 27
    elif devicename == "source-filament-voltage":
        ret["devices"] = Scope("picoscope01", 0)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-filament-current":
        ret["devices"] = Scope("picoscope01", 0)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-grid-voltage":
        ret["devices"] = Scope("picoscope01", 1)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-grid-current":
        ret["devices"] = Scope("picoscope01", 1)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-arc-current":
        ret["devices"] = Scope("picoscope01", 3)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1
        ret["calibration"] = -10  # 1A = 0.1 V
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-arc-voltage":
        ret["devices"] = Scope("picoscope01", 3)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-extraction-voltage":
        ret["devices"] = Scope("picoscope01", 2)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "source-extraction-current":
        ret["devices"] = Scope("picoscope01", 2)
        ret["position"] = 0  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "picoscopeA":
        ret["devices"] = Scope("picoscope01", 0)
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "picoscopeB":
        ret["devices"] = Scope("picoscope01", 1)
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "picoscopeC":
        ret["devices"] = Scope("picoscope01", 2)
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "picoscopeD":
        ret["devices"] = Scope("picoscope01", 3)
        ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "camera-trigger":
        ret["devices"] = Scope("beamlinescope01", 1)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        ret["cell"] = 0
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "scintillator-mesh":
        ret["devices"] = Scope("beamlinescope01", 0)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.8638714  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["cell"] = 0
        ret["zterm"] = 50  # Ohm
    elif devicename in ["FFCup-collector", "LargeFCup-collector"]:
        ret["devices"] = Scope("beamlinescope01", 2)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        if devicename == "FFCup-collector":
            ret["position"] = 9.8658714  # in [m]
        else:
            ret["position"] = 9.94373  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["cell"] = 0
        ret["zterm"] = 50  # Ohm
    elif devicename in ["FFCup-suppressor", "LargeFCup-suppressor"]:
        ret["devices"] = Scope("beamlinescope01", 3)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        if devicename == "FFCup-suppressor":
            ret["position"] = 9.8638714  # in [m]
        else:
            ret["position"] = 9.89838  # in [m]
        ret["cell"] = 0
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["calibration"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "BPM7timing":
        ret["devices"] = Scope("beamlinescope01", 1)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        ret["cell"] = 0
        ret["attenuator"] = 1.995
        ret["calibration"] = 0.78
        ret["zterm"] = 50  # Ohm
    elif devicename == "beamlinescope01A":
        ret["devices"] = Scope("beamlinescope01", 0)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "beamlinescope01B":
        ret["devices"] = Scope("beamlinescope01", 1)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "beamlinescope01C":
        ret["devices"] = Scope("beamlinescope01", 2)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "beamlinescope01D":
        ret["devices"] = Scope("beamlinescope01", 3)
        ret["timing"] = {"trigger": Timing(0, 5, 1)}
        ret["tcable"] = 104.65e-9  # estimate from analysis Time-of-flight 2017-04-21
        ret["position"] = 9.89838  # in [m]
        if shotnumber > 3587133294:
            ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        else:
            ret["attenuator"] = 1.0
        ret["zterm"] = 50  # Ohm
    elif devicename == "Dipoles":
        ret["timing"] = {"trigger": Timing(0, 5, 2)}
    elif devicename == "FEPS":
        ret["timing"] = {"trigger": Timing(3, 0, 0)}
    elif devicename == "CAPSA":
        ret["devices"] = PXI(0, 16, 4)
        ret["timing"] = {"trigger": Timing(3, 0, 1), "aux timing": Timing(0, 4, 0)}
        ret["attenuator"] = 10
        ret["calibration"] = 20
        ret["unit"] = "Current [A]"
    elif devicename == "CAPSB":
        ret["devices"] = PXI(0, 16, 5)
        ret["timing"] = {"trigger": Timing(3, 0, 1), "aux timing": Timing(0, 4, 0)}
        ret["attenuator"] = 10
        ret["calibration"] = 20
        ret["unit"] = "Current [A]"
    elif devicename == "CAPSC":
        ret["devices"] = PXI(0, 16, 6)
        ret["timing"] = {"trigger": Timing(3, 0, 1), "aux timing": Timing(0, 4, 0)}
        ret["attenuator"] = 10
        ret["calibration"] = 20
        ret["unit"] = "Current [A]"
    elif devicename == "CAPSD":
        ret["devices"] = PXI(0, 16, 7)
        ret["timing"] = {"trigger": Timing(3, 0, 1), "aux timing": Timing(0, 4, 0)}
        ret["attenuator"] = 10
        ret["calibration"] = 20
        ret["unit"] = "Current [A]"
    elif devicename == "FFS":
        ret["devices"] = PXI(0, 16, 0)
        ret["timing"] = {"trigger": Timing(3, 0, 2), "aux timing": Timing(0, 4, 0)}
        ret["position"] = 9.69875  # in [m]
        ret["length"] = 0.09296
        ret["radius"] = 0.02  # TODO: double check wall radius, 2.851cm for FFS coils.
        # TODO: check, value taken from shot 3546546988 and 2.857 T/kV charging voltage at 800V
        ret["calibration"] = 4.1108
        ret["unit"] = "B-Field [T]"
        ret["zterm"] = 50  # Ohm TODO: check if needed and if correct
    elif devicename == "Streak":
        ret["timing"] = {"trigger": Timing(3, 2, 0)}
    elif devicename.startswith("PXI"):
        # should be something like PXI4-14-0, we add this, so one can look
        # at raw data. E.g. BPM scopes that don't have a background shot
        name = devicename[3:]
        numbers = [int(n) for n in name.split("-")]
        assert len(numbers) == 3, "wrong number of PXI arguments for devicename"
        ret["devices"] = PXI(numbers[0], numbers[1], numbers[2])
        ret["attenuator"] = get_attenuator_from_DB(shotnumber, ret["devices"])
        ret["calibration"] = get_calibration_from_DB(shotnumber, ret["devices"])
    if devicename.startswith("SRK"):
        ret["attenuator"] = 1.995  # 6dB attenuator behind patch panel in solenoid rack
        ret["calibration"] = 0.78  # coil response: 0.39T/kA, Current Monitor 2kA/V
        ret["zterm"] = 50  # Ohm
        ret["length"] = 0.197406  # in [m]
        ret["radius"] = 0.0473456  # in [m]

    return ret


def get_setting_value_from_DB(name, shotnumber, device, default=1.0):
    """Search for a certain setting in the database"""
    value = default
    setting = get_setting(shotnumber, device.crateslot())
    if setting is not None:
        if name in setting:
            value_list = setting[name]
            # for some, e.g. Scopes and Attenuator settings, these are directly integers
            if isinstance(value_list, int):
                return value_list
            try:
                value = value_list[device.channel]
            except IndexError:
                print(
                    "Error: No setting available for {} in device {} and shot {}".format(
                        name, device, shotnumber
                    )
                )
    return value


def get_attenuator_from_DB(shotnumber, device):
    if isinstance(device, Scope):
        name = "External Gain/Attenuator"
        if device.channel > 0:
            name = f"{name} {device.channel+1}"
        return get_setting_value_from_DB(name, shotnumber, device)
    else:
        return get_setting_value_from_DB("external attenuator/gain", shotnumber, device)
    return out


def get_calibration_from_DB(shotnumber, device):
    return get_setting_value_from_DB("Calibration factor", shotnumber, device)


class Scope:
    """Keep track of the name and channel number"""

    def __init__(self, name, channel):
        self.name = name
        self.channel = channel

    def crateslot(self):
        return str(self.name)

    def __repr__(self):
        return str(self.name) + "-channel" + str(self.channel)


class PXI:
    """Keep track of the crate, slot and channel number"""

    def __init__(self, crate, slot, channel):
        self.crate = crate
        self.slot = slot
        self.channel = channel

    def crateslot(self):
        return "PXI" + str(self.crate) + "Slot" + str(self.slot)

    def __repr__(self):
        return (
            "PXI"
            + str(self.crate)
            + "Slot"
            + str(self.slot)
            + "-channel"
            + str(self.channel)
        )


class Timing:
    """Keep track of the crate, slot and channel number for a timing module.

    This is something like PXI3, timing board 4 (slot 4), channel 2 (0-3).
    """

    def __init__(self, crate, slot, channel):
        self.crate = crate
        self.slot = slot
        self.channel = channel

    def device(self):
        return "PXI" + str(self.crate) + " Timing"

    def __repr__(self):
        return (
            "PXI"
            + str(self.crate)
            + " Timing Board"
            + str(self.slot)
            + "-channel"
            + str(self.channel)
        )


Event = namedtuple("Event", "date, label, tags")
events = [
    Event(datetime(2016, 11, 21), "new filament", ["source"]),
    Event(datetime(2016, 11, 16), "new filament", ["source"]),
    Event(datetime(2016, 9, 29), "building power outage", ["shutdown"]),
    Event(datetime(2015, 10, 23), "new filament", ["source"]),
    Event(datetime(2015, 6, 16), "new He source", ["source"]),
    Event(datetime(2015, 4, 17), "installed YAP", ["target"]),
    Event(datetime(2015, 5, 1), "column failure", ["beamline"]),
    Event(datetime(2014, 12, 5), "new Li source", ["source"]),
    Event(datetime(2014, 11, 10), "changed BPM7 scopes", ["beamline"]),
    Event(datetime(2014, 10, 10), "installed new BPM7", ["beamline"]),
]


def plot_events(ax, filtertags=None):
    for e in events:
        if filtertags is not None:
            use_event = False
            if isinstance(filtertags, str):
                filtertags = [filtertags]
            for t in e.tags:
                if t in filtertags:
                    use_event = True
                    break
        else:
            use_event = True
        if use_event:
            ax.axvline(e.date)
            ymax = ax.get_ylim()[1]
            plt.text(e.date, 0.9 * ymax, e.label, fontsize=10, rotation=45)
