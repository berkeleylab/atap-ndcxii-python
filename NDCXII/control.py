"""remote control NDCXII via python"""

from functools import wraps
import hashlib
import random
import time
from typing import Optional, Callable, Any, Dict

from .db import Shot, Comments, Setting
from . import db as DB
from . import config

try:
    import ujson as json
except ImportError:
    import json

import zmq


# need functions like
#   setSRKvoltages
#   setCellTiming
#   close valve for bkgd shot
#   read settings (from db or rawdata?)
#
#  also:
#    scan voltage in steps of dV
#    do random sampling over dict of parameters
#
#  also:
#    implement wait to adjust for manual changes, e.g
#       SetCellVoltages -- prints out list of voltages that need to be set manual
#                          also saves these to the db
#       wait_for_completion

context = zmq.Context()

need_new_background = False

ZMQ_CONNECTION = {
    "main": "tcp://128.3.58.16:6002",
    "SRKs": "tcp://128.3.58.17:6005",
    "PXI0 Timing": "tcp://128.3.58.50:6000",
    "PXI1 Timing": "tcp://128.3.58.51:6000",
    "PXI3 Timing": "tcp://128.3.58.53:6000",
    "PXI4 Timing": "tcp://128.3.58.54:6000",
    "PXI5 Timing": "tcp://128.3.58.55:6000",
}


def ensure_logged_in(func):
    """Decorator to make sure we can access the database."""

    @wraps(func)
    def wrapper(*args, **kwargs):
        if not DB.init_done:
            DB.init()
        if not DB.init_done:
            print("Warning: can't connect to the database. Skipping")
            return

        return func(*args, **kwargs)

    return wrapper


def send_command(cmd: str, computer: str) -> str:
    """Open a ZMQ connection to the computer and send the command.

    Look up the IP address and send a message to the ZMQ listener on
    that computer.

    Parameters
    ----------
    cmd
       command to send
    computer
       key from ZMQ_CONNECTION dictionary

    Returns
    -------
    str
       message returned by ZMQ

    """
    socket = context.socket(zmq.REQ)
    socket.connect(ZMQ_CONNECTION[computer])

    socket.send(bytearray(cmd, "utf-8"))
    message = socket.recv()
    return message


def trigger() -> None:
    """Trigger NDCX-II."""
    send_command("trigger", computer="main")


def backgroundtrigger() -> None:
    """Trigger NDCX-II in background mode.

    E.g. without the ion source
    """
    global need_new_background
    send_command("backgroundtrigger", computer="main")
    need_new_background = False


def take_shot(timeout: int = 45, testfunc: Optional[Callable] = None) -> None:
    """Take a shot, including backgroundshot if needed

    If testfunc is given, use this to test if the shot was good

    """
    got_data = False
    while not got_data:
        if need_new_background:
            print("taking new background shot")
            backgroundtrigger()
            time.sleep(timeout)
        print("taking new shot")
        trigger()
        triggertime = time.time()
        time.sleep(10)
        if callable(testfunc):
            got_data = testfunc()
            if not got_data:
                print("bad shot...repeating")
        else:
            got_data = True
        wait = time.time() - triggertime
        if wait < timeout:
            time.sleep(timeout - wait)


def last_shot() -> int:
    """Use all shot numbers including background shots."""
    return DB.parse_shotnumber("Alatest")


def load_setting(shotnr: int, devicename: str) -> Any:
    """Loads the json data from the Setting database table."""

    s = Shot.objects(lvtimestamp=shotnr, devicename=devicename).first()
    h = s.settinghash
    setting = Setting.objects(Setting.hash == h).first()
    data = setting.data
    data = json.loads(data)
    return data


def save_setting(data: Any, remove_space: bool = False) -> str:
    """Save the setting back to the database."""
    if remove_space:
        new = json.dumps(data, sort_keys=True).replace(" ", "")
    else:
        new = json.dumps(data, sort_keys=True)
    m = hashlib.md5()
    m.update(bytearray(new, "utf-8"))
    hash = m.hexdigest()

    S = Setting.create(hash=hash, data=new, version=1)
    return hash


@ensure_logged_in
def modify_SRK_voltages(
    newsettings: Dict[str, int], shotnumber: Optional[int] = None,
) -> None:
    """Change the setting of the SRKs.

    Parameters
    ----------
    newsettings
         Dictionary with the settings that should be updated. The keys
         have the form 'SRKNN', e.g. 'SRK01' and the values should be
         integers. All other values are taken from the last shot.
    shotnumber
         If given the other values are based on the shotnumber instead
         of the last shot
    """
    if shotnumber is None:
        shotnumber = last_shot()

    # add check if we got a valid shotnumber
    print(f"Using shot: {shotnumber}")

    data = load_setting(shotnumber, "SRKs")
    for key, value in newsettings.items():
        if key in data:
            data[key] = value
        else:
            print(f"Key name unknown: {key}")

    hash = save_setting(data, remove_space=True)

    # tell labview to load hash
    send_command(hash, computer="SRKs")
    print("Updated timing setting")


def get_timing_device_name(shotnumber, dev):
    configdata = config.config(shotnumber, dev)
    if not "timing" in configdata:
        print(f"No timing information for {dev} available... not changing setting")
        return None, None, None
    if not "trigger" in configdata["timing"]:
        print(
            f"Found timing, but no trigger information for {dev} available... not changing setting"
        )
        return None, None, None

    timing = configdata["timing"]["trigger"]
    timingname = timing.device()
    board = timing.slot
    channel = timing.channel

    return timingname, board, channel


@ensure_logged_in
def modify_timing(
    newsettings: Dict[str, float],
    shotnumber: Optional[int] = None,
    sync_BL_charging: bool = True,
):
    """Change the timing of one or several devices.

    Allowed devices: SRK01-28, Dipoles, FEPS, FFS, FCAPS, Streak, Cell1-7, Injector, BL1-5

    Parameters
    ----------
    newsettings
        Dicitionary of devicename and new timing setting. All other settings will
        taken from the last shot.
    shotnumber
        If given all other settings not specified in newsettings will be taken from
        this shot instead of the last one.
    """
    if shotnumber is None:
        shotnumber = last_shot()

    # a place to store settings
    # Timings for several devices are stored in the same location, so we
    # cannot use last_shot data to overwrite multiple devices and need to store
    # the updated version of the settings and then at the end load them all into the database
    updated_settings = {}
    for dev, value in newsettings.items():
        timingname, board, channel = get_timing_device_name(shotnumber, dev)
        if timingname is None:
            continue

        data = updated_settings.get(
            timingname, load_setting(shotnumber, devicename=timingname)
        )

        # save original time in case we need it later for the BL charing voltages
        time_orig = data["Trigger Modules"][board]["Board Config"]["Channels"][channel][
            "delay (s)"
        ]

        data["Trigger Modules"][board]["Board Config"]["Channels"][channel][
            "delay (s)"
        ] = value

        # updated a single devices, store data in case other devices also need to be changed
        updated_settings[timingname] = data

        # also update BL charging timing if requested (this is the default)
        if sync_BL_charging and dev in ["BL1", "BL2", "BL3", "BL4", "BL5"]:
            timingname, board, channel = get_timing_device_name(
                shotnumber, f"{dev}-chargeV"
            )
            if timingname is None:
                continue
            data = updated_settings.get(
                timingname, load_setting(shotnumber, devicename=timingname)
            )
            t_charging = data["Trigger Modules"][board]["Board Config"]["Channels"][
                channel
            ]["delay (s)"]
            delta = value - time_orig
            new = t_charging + delta
            data["Trigger Modules"][board]["Board Config"]["Channels"][channel][
                "delay (s)"
            ] = new
            updated_settings[timingname] = data

    for timingname, data in updated_settings.items():
        hash = save_setting(data)
        # tell labview to load hash
        send_command(hash, computer=timingname)


@ensure_logged_in
def get_timing(
    devicename: str, shotnumber: Optional[int] = None, verbose: bool = False,
) -> float:
    """Get the trigger timing of a device

    allowed devices: SRK01-28, Dipoles, FEPS, FFS, FCAPS, Streak, Cell1-7, Injector, BL1-5
    """

    if shotnumber is None:
        shotnumber = last_shot()
        if verbose:
            print("Last shot:", shotnumber)

    configdata = config.config(shotnumber, devicename)
    if "timing" not in configdata:
        print(
            f"No timing information for {devicename} available... not changing setting"
        )
        return
    if "trigger" not in configdata["timing"]:
        print(
            f"Found timing, but no trigger information for {devicename} available... "
            f"not changing setting"
        )
        return

    timing = configdata["timing"]["trigger"]
    timingname = timing.device()

    data = load_setting(shotnumber, devicename=timingname)

    board = timing.slot
    channel = timing.channel
    t = data["Trigger Modules"][board]["Board Config"]["Channels"][channel]["delay (s)"]

    if verbose:
        print(f"Timing for {devicename} is {t}")
    return t
