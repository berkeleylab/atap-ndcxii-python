"""NDCXII functions to talk to the cassandra database"""

import cassandra
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model
from cassandra.cqlengine.connection import setup
from cassandra.auth import PlainTextAuthProvider

import appdirs
import sys
from collections import defaultdict, namedtuple
from functools import wraps

try:
    import ujson as json
except ImportError:
    import json
from functools import lru_cache
import logging
import socket
from datetime import datetime, date
import pickle

# log level
log = logging.getLogger(__name__)

comment_list_entry = namedtuple(
    "comment", ["month", "daytime", "species", "nshots", "comment"]
)


class Comments(Model):
    """The comments table in cassandra"""

    month = columns.Integer(primary_key=True)
    daytime = columns.Integer(primary_key=True)
    species = columns.Text(primary_key=True)
    comment = columns.Text()
    sample = columns.Text()
    shots = columns.Set(columns.BigInt)


class Data(Model):
    """The data table in cassandra"""

    devicename = columns.Text(partition_key=True)
    date = columns.Integer(partition_key=True)
    eventtime = columns.DateTime(primary_key=True)
    value = columns.Float(primary_key=True)


class Setting(Model):
    """The setting table in cassandra"""

    hash = columns.Text(primary_key=True)
    data = columns.Text()
    version = columns.Integer()


class Shot(Model):
    """The shot table in cassandra"""

    lvtimestamp = columns.BigInt(primary_key=True)
    devicename = columns.Text(primary_key=True)
    data = columns.Text()
    settinghash = columns.Text()
    version = columns.Integer()


AUTH_PROVIDER = PlainTextAuthProvider(username="ndcxii", password="ndcxii")

from pathlib import Path
from functools import wraps

# set up a disc cache
CACHEDIR = ""
CACHEDIR_MAX_SIZE = 1e9
appname = "NDCXII"
appauthor = "NDCXII"


def enable_disc_cache():
    set_cachedir(Path(appdirs.user_cache_dir(appname, appauthor)) / "data")


def set_cachedir(mydir):
    global CACHEDIR
    old = CACHEDIR
    CACHEDIR = Path(mydir)

    if CACHEDIR.is_file():
        print(f"Error: {mydir} not a directory")
        CACHEDIR = old
        return

    if not CACHEDIR.exists():
        CACHEDIR.mkdir(parents=True, exist_ok=True)


def get_cachedir_size():
    if not CACHEDIR.is_dir():
        print(f"Error {CACHEDIR} not a directory")
        return 0
    size = 0
    for f in CACHEDIR.glob("NDCXII-cache-*"):
        if f.is_file():
            size += f.stat().st_size
    return size


def cache_delete_old_files():
    size = get_cachedir_size()
    if size > CACHEDIR_MAX_SIZE:
        print("CACHE too big...removing old files")
        files = sorted(CACHEDIR.glob("NDCXII-cache-*"), key=lambda x: x.stat().st_atime)
        while size > CACHEDIR_MAX_SIZE and files:
            f = files.pop(0)
            s = f.stat().st_size
            f.unlink()
            size -= s


def _read_from_cache(cache_key):
    cache_file = CACHEDIR / f"NDCXII-cache-{cache_key}"
    if cache_file.is_file():
        with cache_file.open("rb") as f:
            return pickle.load(f)
    return None


def _write_to_cache(cache_key, value):
    cache_file = CACHEDIR / f"NDCXII-cache-{cache_key}"
    with cache_file.open("wb") as f:
        pickle.dump(value, f)


def disk_cache(func):
    """Supply a disk cache of a maximum size

    only use it if CACHEDIR is not empty
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        if CACHEDIR:
            # come up with a unique key, needs to include the function
            # name, since the arguments are not unique to a single
            # function!
            key = (
                func.__name__
                + "-"
                + "-".join(
                    [str(x) for x in args] + [f"{k}-{v}" for k, v in kwargs.items()]
                )
            )
            # check if key already is in cache, if so load from there
            value = _read_from_cache(key)
            if value is not None:
                return value
            # if not, call function
            value = func(*args, **kwargs)
            # and save to disk
            _write_to_cache(key, value)
            return value
        else:
            return func(*args, **kwargs)

    return wrapper


# make a connection on import
init_done = False


def ensure_logged_in(ret=[]):
    "Make sure we are logged into the database or print a warning"

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not init_done:
                init()
            if not init_done:
                print("in offline mode")
                return ret
            return func(*args, **kwargs)

        return wrapper

    return decorator


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(("10.255.255.255", 0))
        IP = s.getsockname()[0]
    except:
        IP = "127.0.0.1"
    finally:
        s.close()
    return IP


def init(timeout=6.0):
    """Connect to the Cassandra database

    First try to connect to the main database. This will only work if
    you are behind the firewall at NDCXII. If this doesn't work, try
    to connect to the public read-only database.

    """

    global init_done
    if init_done:
        return

    # get local IP
    ip = get_ip()

    if ip.startswith("128.3.58"):
        setup(hosts=["128.3.58.20"], default_keyspace="test", protocol_version=3)
        init_done = True
        print("Getting data from 128.3.58.20")
    if not init_done:
        try:
            setup(
                hosts=["rocs.lbl.gov"],
                default_keyspace="test",
                auth_provider=AUTH_PROVIDER,
                control_connection_timeout=timeout,
                protocol_version=3,
            )
            init_done = True
        except:
            print("*" * 36)
            print("*   Error: Can't connect to DB   *")
            print("*" * 36)
            print("traceback:")
            print(sys.exc_info())
            print("*" * 36)
            print("* Will continue, in offline mode *")
            print("*" * 36)
        print("Getting data from rocs.lbl.gov")


@ensure_logged_in()
@disk_cache
def shots_from_comment(month, daytime):
    """Returns all shots in a comment"""

    # also accepts strings, but we really need int
    if isinstance(month, str):
        month = int(month)
    if isinstance(daytime, str):
        daytime = int(daytime)

    # some simple checks
    assert month > 201000, "Month needs to be in the form YYYYMM"
    assert (daytime > 10000) and (
        daytime < 315000
    ), "Daytime needs to be in the form DDHHMM"

    data = Comments.objects(Comments.month == month)
    data = data.filter(Comments.daytime == daytime).limit(1).first()
    if data is None:
        print("Warning: No data in comment {}-{}".format(month, daytime))
        return []
    return sorted(data.shots)


@ensure_logged_in()
@disk_cache
def get_all_shotnumbers_from_month(month):
    shots = []

    for d in list_comments(month):
        # use try function to not run into an error message in case of bad shot (no data)
        try:
            for s in shots_from_comment(month, d.daytime):
                # apply filter, so that only shots with Faraday Cup in get appended
                shots.append(s)
            print("appended " + str(shots[-1]))
        except:
            pass

    return shots


@ensure_logged_in()
def list_comments(months, daytime=None, skip4200=True):
    """Give a list of comments in the given months

    By default we skip the 4200 time, which includes a list of
    all shots for that day.
    """
    log.debug(
        "in list_comments: months:{} daytime:{} skip4200:{}".format(
            months, daytime, skip4200
        )
    )
    assert daytime is None or (
        (int(daytime) > 10000) and (int(daytime) < 315000)
    ), "Daytime needs to be in the form DDHHMM"

    # we want to iterate over the months, so make sure it's a list
    if not isinstance(months, list):
        months = [months]

    for m in months:
        assert int(m) > 201000, "Month needs to be in the form YYYYMM"

        if daytime:
            data = Comments.objects(Comments.month == m)
            data = data.filter(Comments.daytime == daytime).limit(1).first()
            if data is None:
                return []
            data = [data]
        else:
            data = Comments.objects(Comments.month == m).all()

        for d in data:
            if str(d.daytime).endswith("4200"):
                if skip4200:
                    continue
                else:
                    comment = "all shots from this day"
            else:
                comment = d.comment

            nshots = len(d.shots) if d.shots else 0
            yield comment_list_entry(m, d.daytime, d.species, nshots, comment)


@lru_cache(maxsize=1000)
@disk_cache
@ensure_logged_in(ret={})
def get_shot(shotnumber, devicename=None, limit=1):
    """Get all the data from one shot or
       just the data from one device in a shot

    use limit=None and no devicename to get all data for a shot
    """

    log.debug(" shotnumber: {} devicename: {}".format(shotnumber, devicename))

    if devicename is not None:
        dbdata = Shot.objects(Shot.lvtimestamp == shotnumber)
        dbdata = dbdata.filter(Shot.devicename == devicename).limit(1).first()
        if dbdata:
            dbdata = dbdata.data
            if dbdata is not None:
                if devicename != "comment":
                    dbdata = json.loads(dbdata)
        return dbdata
    else:
        dbdata = Shot.objects(Shot.lvtimestamp == shotnumber)
        if limit:
            dbdata = dbdata.all().limit(limit)
        else:
            dbdata = dbdata.all()
        ret = {}
        for dev in dbdata:
            name = dev.devicename
            if dev.data:
                if name == "comment":
                    data = dev.data
                else:
                    data = json.loads(dev.data)
            else:
                data = None
            ret[name] = data
        return ret


@lru_cache(maxsize=1000)
@disk_cache
@ensure_logged_in(ret={})
def get_setting_from_hash(settinghash):
    """Return the Settings for a certain device by hash"""

    if settinghash is None or settinghash.strip() == "":
        return None

    dbdata = Setting.objects(Setting.hash == settinghash).limit(1).first()
    if dbdata:
        dbdata = dbdata.data
        if dbdata is not None:
            dbdata = json.loads(dbdata)
    return dbdata


@lru_cache(maxsize=1000)
@disk_cache
@ensure_logged_in(ret="")
def get_settinghash(shotnumber, devicename):
    """Return the settingshash for a certain device and shot"""

    log.debug(" shotnumber: {} devicename: {}".format(shotnumber, devicename))

    settinghash = Shot.objects(Shot.lvtimestamp == shotnumber)
    settinghash = settinghash.filter(Shot.devicename == devicename).limit(1).first()
    if settinghash:
        settinghash = settinghash.settinghash

    return settinghash


@lru_cache(maxsize=1000)
@disk_cache
@ensure_logged_in(ret={})
def get_setting(shotnumber, devicename):
    """Return the settings for a certain device and shot"""

    log.debug(" shotnumber: {} devicename: {}".format(shotnumber, devicename))

    settinghash = get_settinghash(shotnumber, devicename)
    setting = get_setting_from_hash(settinghash)

    return setting


@lru_cache(maxsize=1000)
@disk_cache
@ensure_logged_in()
def get_data(devicename, date, tstart=None, tstop=None, limit=1):
    """Return cont. data"""

    log.debug("devicename: {}".format(devicename))

    if tstart is not None and tstop is not None:
        data = Data.filter(devicename=devicename, date=date)
        data = data.filter(Data.eventtime > tstart)
        data = data.filter(Data.eventtime < tstop)
        data = data.limit(limit)
        data = data.all()
    return data


@ensure_logged_in()
@disk_cache
def get_data_names():
    """Return list of all available data names and the months for which data is available"""

    log.debug("in get_data_names")

    try:
        a = Data.objects.distinct(["devicename", "date"])
        names = defaultdict(list)
        for i in a:
            names[i.devicename].append(i.date)
        return names
        print("after return")
    except cassandra.OperationTimedOut:
        print("Operation timed out, please try again.")
        return []


def get_comment_from_shot(shotnumber):
    # find comment for a given shot
    try:
        yearmonth, daytime = get_shot(shotnumber, "comment").split("-")
        comment = list(list_comments(yearmonth, daytime))[0]
    except AttributeError:
        comment = None
    except IndexError:
        comment = None
    return comment


def parse_shotnumber(number):
    """Checks to see if we input a shot number

    Allowed values:
    * 10 digit number -> do nothing, just return it as an int
    * less than 10 digits, get the latest shot number and use it to
      autocomplete, e.g. assume that the given digits are the last ones
      and the first ones should be the same as the last taken shot
    * 'latest' figure out the latest shot from the comment
    * any shotnumber or 'latest' with an additional number of ^ to get the
      previous shots, e.g. latest^ is the previous shot, latest^^ is the one
      before that. You can also use latest^3. 1234^3 will also work

    """
    if len(number) == 10 and number.isdigit():
        return int(number)

    # if not we need to get a list of recent shots from this month
    d = date.today()
    daytime = None
    if "@" in number:
        atpos = number.find("@")
        datestring = number[atpos + 2 : -1]  # skip {}
        dates = datestring.split("-")
        month = dates[0].strip()
        if len(dates) == 1:
            pass
        else:
            daytime = dates[1].strip()
        number = number[:atpos]
    else:
        month = "{}{:02d}".format(d.year, d.month)

    # find daytime unless already set
    if number[0] == "A":
        # get last day
        # the 4200 comment list all shots
        if daytime:
            daytime = "{}4200".format(daytime[:-4])
        else:
            day = str(list(list_comments(month))[-1][1])[:-4]
            daytime = "{}4200".format(day)
        number = number[1:]
    else:
        # get last day
        if not daytime:
            daytime = list(list_comments(month))[-1][1]
    shots = list(shots_from_comment(month, daytime))
    if not shots:
        raise SystemExit(f"The comment {month}-{daytime} is empty!")

    last = 0
    # look for ^
    pos = number.find("^")
    if pos > 0:
        for c in number[pos:]:
            if c == "^":
                last += 1
            if c.isdigit():
                last += int(c) - 1
    else:
        pos = len(number)
    assert last < len(shots), f"Not that many shots available for {month}-{daytime}"

    if number.startswith("latest"):
        lastshot = shots[-1 - last]
        return int(lastshot)
    elif number[:pos].isdigit():
        # find location of shot in shotlist
        loc = 0
        for s in reversed(shots):
            if str(s).endswith(number[:pos]):
                break
            loc += 1
        assert last + loc <= len(
            shots
        ), "Not that many shots available for {}-{}".format(month, daytime)
        shot = shots[-1 - last - loc]
        return int(shot)
    else:
        raise SystemExit("Can't parse shotnumber")
