"""Script for creating lists of devices. This is hard-coded to avoid error and
more convenient to keep out of the main data scraping script."""

# Create list of SRK device names, 28 in total
SRK_names = [f"SRK{i:02d}" for i in range(1, 29)]

# Create list of BPM device names, 7 in total
BPM_names = [f"BPM{i:1d}" for i in range(1, 8)]

# Create list of Blumlein names, 5 in total
Blumlein = [f"BL{i:1d}" for i in range(1, 6)]

# Create list of compression cell names, 7 in total
Compr_cell = [f"Cell{i:1d}" for i in range(1, 8)]
