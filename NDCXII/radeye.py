#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 09:22:57 2019

@author: lgeulig
"""

import rawpy
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import glob, os


"""
General file with functions to analyze RadEye data
when working with imshow: Set vmax, so pictures are comparable!!!
"""


# basic function to open a *.raw file and showing the image, if show is set to 'True'
# imput is the path of data file
def openRE(path, show=False):
    npimg = np.fromfile(path, dtype=np.uint16)
    imageSize = (1024, 1024)
    npimg = npimg.reshape(imageSize)
    plt.imshow(npimg, cmap="RdGy", vmax=15000)
    plt.colorbar()
    plt.title(str(path))
    plt.savefig(str(path) + ".pdf")
    if show == True:
        plt.show()
    return npimg


# function to open all the *.raw files in a folder
# imput is the path of the directory
def openfolder(directory):
    os.chdir(directory)
    for file in glob.glob("*.raw"):
        try:
            openRE(file)
        except:
            print("Could not open " + str(file))


# plots the image and a histogram into one picutre
def plotRE(path):
    npimg = np.fromfile(path, dtype=np.uint16)
    imageSize = (1024, 1024)
    npimg = npimg.reshape(imageSize)

    fig = plt.figure(figsize=(6, 6))
    grid = plt.GridSpec(14, 14, wspace=200000000000)
    hist_ax = fig.add_subplot(grid[5:-5, :6])
    image_ax = fig.add_subplot(grid[:, 6:])
    divider = make_axes_locatable(image_ax)

    hist_ax.hist(npimg.flatten(), bins=100, log=True)
    image = image_ax.imshow(npimg, cmap="RdGy", vmax=15000)

    cax_im = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(image, cax=cax_im)
    fig.suptitle(str(path) + "\n max.pixel= " + str(npimg.max()))
    plt.savefig(str(path) + ".pdf")


# this function does the backgroundsubstraction
# Input is the path of the image and the backgroundimage
def normalize(path, background_path, title=None):
    imageSize = (1024, 1024)

    background = np.fromfile(background_path, dtype=np.uint16)
    background = background.reshape(imageSize).astype(np.float)

    image = np.fromfile(str(path), dtype=np.uint16).astype(np.float)
    imageSize = (1024, 1024)
    image = image.reshape(imageSize)
    #
    subtracted = image - background
    np.savetxt("tmp.txt", subtracted)
    mask = subtracted <= 0
    subtracted[mask] = 0
    plt.imshow(subtracted, cmap="RdGy", vmax=15000, extent=[70, 20, -3.3, 45.7])
    plt.colorbar()
    if title is None:
        filename = str(path) + "_normalized.pdf"
    else:
        filename = get_new_filename(title)
    plt.title(str(path) + "_normalized")
    plt.xlabel("magnetic deflection [mm]")
    plt.ylabel("electric deflection [mm]")
    plt.savefig(str(path) + "_normalized_rescaled.pdf")
    plt.show()
    return subtracted


def get_new_filename(title):
    files = glob.glob(f"{title}-*.pdf")
    numbers = [int(x.split("-")[1][:-4]) for x in files]
    new = max(numbers) + 1
    return f"{title}-{new:03d}.pdf"


# this function does the background subtraction for a whole directory
# input is the path of the directory
def normalizeFolder(directory):
    os.chdir(directory)
    for file in glob.glob("*.raw"):
        try:
            normalize(file)
        except:
            print("Could not normalize" + str(file))


def get_max(path):
    image = normalize(path)
    plt.show()

    x = np.sum(image, axis=1)
    x_index = np.where(x == np.amax(x))
    plt.plot(x)
    plt.title("sum over horizontal axis \n max. at " + str(x_index[0]))
    print(x_index[0])

    y = np.sum(image, axis=0)
    y_index = np.where(y == np.amax(y))
    plt.plot(y)
    print(y_index[0])


# calculates the electric field in the TPS depending on the setting on the
# potentiometer knob
def tps_efield(pot):
    return 3.2672 * pot - 0.0803
