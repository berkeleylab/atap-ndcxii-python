These files provide several scripts as well as a python module to
access and control data from the NDCXII experiment at Lawrence
Berkeley National Laboratory.

## INSTALL

To install the module, run `pip install -e .` in the python directory
where you find the setup.py file.

## Contribute

Patched are always welcome. We use 'black' to format the code and
'pre-commit' to automate this. After you downloaded the git repo, please do

    pip install pre-commit

followed by

    pre-commit install

inside the git repo. After that your commits will automatically be
formatted the same way as the rest of the code. It might also help to
set up your editor to use black, but this is not a must.


## Copyright

"NDCX-II Python Module", Copyright (c) 2016, The Regents of the
University of California, through Lawrence Berkeley National
Laboratory (subject to receipt of any required approvals from the
U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this
software, please contact Berkeley Lab's Innovation & Partnerships
Office at IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the
U.S. Department of Energy and the U.S. Government consequently retains
certain rights. As such, the U.S. Government has been granted for
itself and others acting on its behalf a paid-up, nonexclusive,
irrevocable, worldwide license in the Software to reproduce,
distribute copies to the public, prepare derivative works, and perform
publicly and display publicly, and to permit other to do so.
