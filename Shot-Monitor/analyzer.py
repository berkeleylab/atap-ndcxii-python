from __future__ import unicode_literals
from contextlib import redirect_stdout
from contextlib import redirect_stderr
from pathlib import Path
import sys
import time
import traceback
from typing import Optional


import appdirs
import matplotlib

matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt
import numpy as np
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon
from PyQt5.QtWidgets import (
    QApplication,
    QCheckBox,
    QFileDialog,
    QHBoxLayout,
    QInputDialog,
    QListView,
    QListWidget,
    QMainWindow,
    QMenu,
    QMessageBox,
    QPushButton,
    QSizePolicy,
    QTabWidget,
    QVBoxLayout,
    QWidget,
)
from PyQt5.QtCore import Qt, QTimer

from pyqode.core import api
from pyqode.core import modes
from pyqode.core import panels
from pyqode.core import backend

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar

import NDCXII
from NDCXII import data
from NDCXII import db as DB

# remove command line information
NDCXII.plt.figure = NDCXII.oldfigure

import zmq
from io import StringIO

# directory for saving the tabs
appname = "NDCXII-plot"
appauthor = "NDCXII"
CACHEDIR = Path(appdirs.user_cache_dir(appname, appauthor)) / "tabs"
CACHEDIR.mkdir(parents=True, exist_ok=True)

# listen to shot events
c = zmq.Context()
s = c.socket(zmq.SUB)
s.connect("tcp://128.3.58.16:6001")
s.setsockopt_string(zmq.SUBSCRIBE, "")

poller = zmq.Poller()
poller.register(s, zmq.POLLIN)


class SingleFigure(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self, parent=None, width=4, height=4, dpi=100):
        self.fig, self.axes = plt.subplots()

        super().__init__(self.fig)
        self.setParent(parent)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.updateGeometry()

    def plot(self, scope, title=None):
        if scope is None:
            return
        scope.plot(ax=self.axes)
        if title:
            self.axes.set_title(title)
        self.axes.relim()
        self.axes.autoscale_view(True, True, True)
        plt.tight_layout()
        self.draw()


class ListShots(QVBoxLayout):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parent = parent

        hbox1 = QVBoxLayout()

        btn_width = 150
        list_width = 150

        for name, func in [
            ["Add Shot", self.add],
            ["Add Comment", self.add_comment],
            ["Delete selected", self.delete],
            ["Clear all", self.clear_all],
        ]:
            btn = QPushButton(name)
            btn.clicked.connect(func)
            btn.setMaximumWidth(btn_width)
            hbox1.addWidget(btn)

        self.addLayout(hbox1)

        self.list = QListView()
        self.model = QStandardItemModel(self.list)
        self.list.setModel(self.model)
        self.list.setMaximumWidth(list_width)
        self.list.show()
        self.addWidget(self.list)
        self.shotdata = []

    def add_new_shot(self, shotnumber):
        item = QStandardItem(str(shotnumber))
        item.setCheckable(True)
        self.model.appendRow(item)
        existing = [s.shotnumber for s in self.shotdata]
        if shotnumber not in existing:
            A = NDCXII.data.Shotdata(shotnumber)
            self.shotdata.append(A)

    def add(self):
        text, ok = QInputDialog.getText(self.parent, "Add Shot", "Enter a shot number:")
        if text.isdigit() and ok:
            self.add_new_shot(text)

    def add_comment(self):
        text, ok = QInputDialog.getText(
            self.parent, "Add Comment", "Enter a comment (yyyymm-ddhhmm):"
        )
        if ok and len(text) in [12, 13]:
            month, day = text.split("-")
            shots = DB.shots_from_comment(month, day)
            for s in shots:
                self.add_new_shot(s)

    def delete(self):
        i = 0
        while self.model.item(i):
            if self.model.item(i).checkState():
                sn = int(self.model.item(i).text())
                self.shotdata = [s for s in self.shotdata if s.shotnumber != sn]
                self.model.removeRow(self.model.item(i).row())
            else:
                i += 1

    def clear_all(self):
        reply = QMessageBox.question(
            self.parent,
            "Clear all",
            "Clear shot list?",
            QMessageBox.No | QMessageBox.Yes,
            QMessageBox.Yes,
        )
        if reply == QMessageBox.Yes:
            self.model.clear()
            self.shotdata = []


class SingleMonitor(SingleFigure):
    """A canvas that updates itself every second with a new plot."""

    def __init__(self, connect=None, *args, **kwargs):
        self.main = kwargs.pop("main")
        super().__init__(*args, **kwargs)
        self.connect = connect

    def evaluate(self):
        self.Scope.calc_values()
        Scope = self.Scope
        t = self.Scope.t
        x = self.Scope.xprime
        if self.connect == "FFCupcollector":
            x = x * 0.55 / 1.15
            charge = Scope.total * 0.55 / 1.15 * 10 ** 3
            self.axes.set_ylabel("Current [A]")
        else:
            self.axes.set_ylabel("Voltage [V]")
            charge = Scope.total
        self.axes.set_xlabel("Time [$\mu$s]")
        self.axes.plot(t, x)
        self.axes.axvspan(Scope.start, Scope.end, color="b", alpha=0.5)
        self.axes.relim()
        self.axes.autoscale_view(True, True, True)
        plt.sca(self.axes)
        plt.tight_layout()
        self.draw()
        return [
            "Charge:\t{:.3f} nC".format(charge),
            "Peak value:\t{:.3f} A".format(x.max()),
            "FWHM:\t{:.3f} ns".format(Scope.fwhm * 10 ** 3),
        ]


class SinglePlot(QWidget):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.parent = parent

        hbox = QHBoxLayout()
        vbox = QVBoxLayout()

        self.dc = SingleMonitor(parent=parent, connect="FFCupcollector", main=parent)
        self.tb = NavigationToolbar(self.dc, parent=parent)
        vbox.addWidget(self.tb)
        vbox.addWidget(self.dc)
        hbox.addLayout(vbox)

        col = QVBoxLayout()

        save_btn = QPushButton("Save code")
        save_btn.clicked.connect(self.save)
        width = 100
        save_btn.setMaximumWidth(width)
        col.addWidget(save_btn)

        skip_bkgd = QCheckBox("Skip background")
        skip_bkgd.setMaximumWidth(width)
        self.skip_bkgd = skip_bkgd
        col.addWidget(skip_bkgd)

        self.editor = api.CodeEdit()
        col.addWidget(self.editor)

        self.editor.backend.start("server.py")

        self.editor.modes.append(modes.IndenterMode())
        self.editor.modes.append(modes.AutoIndentMode())
        self.editor.modes.append(modes.AutoCompleteMode())
        self.editor.modes.append(modes.SmartBackSpaceMode())
        self.editor.modes.append(
            modes.PygmentsSyntaxHighlighter(self.editor.document())
        )
        self.editor.modes.append(modes.CaretLineHighlighterMode())

        self.listwidget = QListWidget()
        col.addWidget(self.listwidget)

        self.editor.setMinimumWidth(175)
        self.editor.setMaximumWidth(550)
        self.listwidget.setMaximumWidth(550)

        hbox.addLayout(col)

        self.setLayout(hbox)

    def save(self):
        filename = QFileDialog.getSaveFileName(self, "Save Code")
        code = self.editor.toPlainText()
        if filename[0] != "":
            with open(filename[0], "w") as f:
                f.write(code)
            name = filename[0].split(".")[0]
            name = name.split("/")[-1]
            if name.startswith("plugin_"):
                name = name[7:]
            name = name.replace("_", " ")
            self.parent.tabs.setTabText(len(self.parent.tabs) - 1, name)

    def update(self):
        pass


class ApplicationWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.newshot = None
        self.oldshot = None
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowTitle("NDCXII Beam Analyzer")
        self.file_menu = QMenu("&File")
        self.file_menu.addAction("& Load shot", self.loadshot, Qt.CTRL + Qt.Key_S)
        self.file_menu.addAction("&Quit", self.fileQuit, Qt.CTRL + Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.tab_menu = QMenu("&Tabs")
        self.tab_menu.addAction(" & Add tab", self.addTab, Qt.CTRL + Qt.Key_N)
        self.tab_menu.addAction(
            " & Load code in new tab", self.loadCode, Qt.CTRL + Qt.Key_O
        )
        self.tab_menu.addAction(" & replot", self.replot, Qt.CTRL + Qt.Key_R)
        self.tab_menu.addAction(
            " & Rename current tab", self.rename, Qt.CTRL + Qt.Key_Y
        )
        self.menuBar().addMenu(self.tab_menu)

        self.help_menu = QMenu("&Help")
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction("&About", self.about)

        self.main_widget = QWidget(self)

        l = QHBoxLayout()

        self.shotlist = ListShots(self)
        l.addLayout(self.shotlist)

        self.tabs = QTabWidget()
        plot = SinglePlot(self)

        # reload old tabs
        files = list(CACHEDIR.glob("tabs?????.txt"))
        if files:
            for filename in files:
                self.loadCode(filename)
        else:
            self.tabs.addTab(plot, "FFCup")

        oldshots = CACHEDIR / "shots.txt"
        if oldshots.is_file():
            with oldshots.open("r") as f:
                shots = f.readlines()
                shots = [int(s) for s in shots]
            for s in shots:
                self.shotlist.add_new_shot(s)

        l.addWidget(self.tabs)

        self.main_widget.setLayout(l)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        # try to update figures every second
        timer = QTimer(self)
        timer.timeout.connect(self.update_figures)
        timer.start(1000)

    def update_figures(self):
        socks = dict(poller.poll(0))
        if s in socks and socks[s] == zmq.POLLIN:
            self.newshot = int(s.recv_string())
            # need to wait three seconds for data to be written
            # this should be more robust in the future
            time.sleep(3)
        if self.newshot != self.oldshot:
            self.shotlist.add_new_shot(self.newshot)
            self.oldshot = self.newshot
            self.replot()

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QMessageBox.about(
            self, "About", """GUI to monitor and analyze shots live for NDCXII"""
        )

    def loadshot(self):
        text, ok = QInputDialog.getText(self, "Load Shot", "Enter a shot number:")
        if text.isdigit() and ok:
            self.newshot = int(text)

    def addTab(self):
        plot = SinglePlot(self)
        plot.editor.setPlainText(
            "# 'shots' contains a list of Shotdata clases\n# 'axes' is the matplotlib axes to plot on ",
            ".txt",
            "utf-8",
        )
        # self.tabs.addTab(plot, "new {}".format(self.tabs.currentIndex()))
        i = 0
        while self.tabs.isTabEnabled(i) == True:
            i += 1
        self.tabs.addTab(plot, "new {}".format(i + 1))
        return plot

    def replot(self):
        i = 0
        shots = self.shotlist.shotdata
        while self.tabs.widget(i):
            if self.tabs.widget(i).skip_bkgd.isChecked():
                shots = [i for i in shots if i.backgroundshotnumber is not None]
            text = self.tabs.widget(i).editor.toPlainText()
            L = self.tabs.widget(i).dc
            out = self.tabs.widget(i).listwidget
            axes = L.axes
            try:
                axes.clear()
                # capture the output and display it
                new = StringIO()
                with redirect_stderr(sys.stdout):
                    with redirect_stdout(new):
                        exec(text, globals(), locals())
                out.clear()
                out.addItem(new.getvalue())
                out.scrollToBottom()
                L.draw()
            except:
                out.addItem(traceback.format_exc())
                out.scrollToBottom()
                sys.stdout.flush()
            if len(text) == 0:
                tmp = self.tabs.widget(i)
                self.tabs.removeTab(i)
                del tmp
            else:
                with open(CACHEDIR / "tabs{:05d}.txt".format(i), "w") as f:
                    f.write(self.tabs.tabText(i))
                    f.write("\n")
                    f.write(text)
                with open(CACHEDIR / "shots.txt", "w") as f:
                    for s in shots:
                        f.write(str(s.shotnumber))
                        f.write("\n")
                i += 1

    def loadCode(self, filename: Optional[Path] = None) -> None:
        if filename is None:
            filename = QFileDialog.getOpenFileName(self, "Open Code")
            if filename[0] != "":
                return
            filename = Path(filename[0])

        plot = self.addTab()
        with filename.open("r") as f:
            data = f.read()
        if filename.is_relative_to(CACHEDIR):
            data = data.split("\n")
            name = data[0]
            data = "\n".join(data[1:])
        else:
            name = filename.stem
            if name.startswith("plugin_"):
                name = name[7:]
                name = name.replace("_", " ")
        plot.editor.setPlainText(data, ".txt", "utf-8")
        self.tabs.setTabText(len(self.tabs) - 1, name)

    def rename(self):
        text, ok = QInputDialog.getText(self, "Rename Tab", "Enter a new tab name:")
        if ok:
            self.tabs.setTabText(self.tabs.currentIndex(), text)


if __name__ == "__main__":
    qApp = QApplication(sys.argv)

    aw = ApplicationWindow()
    iconpath = Path(".") / "Icon" / "NDCXIILogo.jpeg"

    aw.setWindowIcon(QIcon(str(iconpath)))
    aw.show()
    sys.exit(qApp.exec_())
