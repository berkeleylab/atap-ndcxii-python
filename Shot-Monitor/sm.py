#!/usr/bin/env python3
"""
This file is a modificatoin of NDCXII-shot-monitor, so that it can be integrated with report.py through the all_checks() function.

This file monitors if all scopes record their data in the database for each shot
"""
from NDCXII import shot_warnings
import zmq
import sys
import time

try:
    import ujson as json
except ImportError:
    import json
import select

# import msvcrt
from termcolor import colored
import numpy as np
import signal

from NDCXII.db import Shot, Comments, Data, Setting
import NDCXII.db as DB

c = zmq.Context()
s = c.socket(zmq.SUB)
s.connect("tcp://128.3.58.16:6001")
s.setsockopt_string(zmq.SUBSCRIBE, "")

poller = zmq.Poller()
poller.register(s, zmq.POLLIN)

print("listening for shots")

DB.init()

# this should be really in a text file and be parsed by LabVIEW and python
address = {
    "PXI0Slot5": "tcp://128.3.58.50:6005",
    "PXI0Slot6": "tcp://128.3.58.50:6006",
    "PXI0Slot9": "tcp://128.3.58.50:6009",
    "PXI0Slot10": "tcp://128.3.58.50:6010",
    "PXI0Slot11": "tcp://128.3.58.50:6011",
    "PXI0Slot12": "tcp://128.3.58.50:6012",
    "PXI0Slot15": "tcp://128.3.58.50:6015",
    "PXI0Slot16": "tcp://128.3.58.50:6016",
    "PXI0Slot17": "tcp://128.3.58.50:6017",
    "PXI0Slot18": "tcp://128.3.58.50:6018",
    "PXI1Slot9": "tcp://128.3.58.51:6009",
    "PXI1Slot10": "tcp://128.3.58.51:6010",
    "PXI1Slot11": "tcp://128.3.58.51:6011",
    "PXI1Slot12": "tcp://128.3.58.51:6012",
    "PXI1Slot15": "tcp://128.3.58.51:6015",
    "PXI1Slot16": "tcp://128.3.58.51:6016",
    "PXI1Slot17": "tcp://128.3.58.51:6017",
    "PXI1Slot18": "tcp://128.3.58.51:6018",
    "PXI3Slot4": "tcp://128.3.58.53:6004",
    "PXI3Slot5": "tcp://128.3.58.53:6005",
    "PXI3Slot6": "tcp://128.3.58.53:6006",
    "PXI3Slot8": "tcp://128.3.58.53:6008",
    "PXI3Slot9": "tcp://128.3.58.53:6009",
    "PXI3Slot10": "tcp://128.3.58.53:6010",
    "PXI3Slot11": "tcp://128.3.58.53:6011",
    "PXI3Slot12": "tcp://128.3.58.53:6012",
    "PXI3Slot14": "tcp://128.3.58.53:6014",
    "PXI3Slot15": "tcp://128.3.58.53:6015",
    "PXI3Slot16": "tcp://128.3.58.53:6016",
    "PXI3Slot17": "tcp://128.3.58.53:6017",
    "PXI3Slot18": "tcp://128.3.58.53:6018",
    "PXI4Slot4": "tcp://128.3.58.54:6004",
    "PXI4Slot5": "tcp://128.3.58.54:6005",
    "PXI4Slot6": "tcp://128.3.58.54:6006",
    "PXI4Slot8": "tcp://128.3.58.54:6008",
    "PXI4Slot9": "tcp://128.3.58.54:6009",
    "PXI4Slot10": "tcp://128.3.58.54:6010",
    "PXI4Slot11": "tcp://128.3.58.54:6011",
    "PXI4Slot12": "tcp://128.3.58.54:6012",
    "PXI4Slot14": "tcp://128.3.58.54:6014",
    "PXI4Slot15": "tcp://128.3.58.54:6015",
    "PXI4Slot16": "tcp://128.3.58.54:6016",
    "PXI4Slot17": "tcp://128.3.58.54:6017",
    "PXI4Slot18": "tcp://128.3.58.54:6018",
    "PXI5Slot4": "tcp://128.3.58.55:6004",
    "PXI5Slot5": "tcp://128.3.58.55:6005",
    "PXI0 Timing": "tcp://128.3.58.50:6000",
    "PXI1 Timing": "tcp://128.3.58.51:6000",
    "PXI3 Timing": "tcp://128.3.58.53:6000",
    "PXI4 Timing": "tcp://128.3.58.54:6000",
    "PXI5 Timing": "tcp://128.3.58.55:6000",
    "SRKs": "tcp://128.3.58.17:6005",
    "Dipoles": "tcp://128.3.58.17:6006",
    "Blumleins": "tcp://128.3.58.72:6020",
    "picoscope01": "tcp://128.3.58.31:6000",
}


# device list to check
BPM1 = ["PXI3Slot8", "PXI3Slot9"]
BPM2 = ["PXI3Slot10", "PXI3Slot11"]
BPM3 = ["PXI3Slot12", "PXI3Slot14"]
BPM4 = ["PXI4Slot4", "PXI4Slot5"]
BPM5 = ["PXI4Slot6", "PXI4Slot8"]
BPM6 = ["PXI4Slot9", "PXI4Slot10"]
BPM7 = ["PXI4Slot17", "PXI4Slot18"]
dnBPM = BPM1 + BPM2 + BPM3 + BPM4 + BPM5 + BPM6 + BPM7

# latest scaling factors for the BPM. <1 = gain, >1 attenuator, numbers are atten/gain
BPMscaling = (
    2 * [1 / 2]
    + 2 * [2 / 10]
    + 2 * [2 / 10]
    + 2 * [1 / 2]
    + 2 * [1 / 1]
    + 2 * [2 / 1]
    + 2 * [10 / 1]
)
Cellscaling = [[10, 2], [2, 2], [2, 4], [4, 4]]

dnImon = ["PXI3Slot5", "PXI3Slot6", "PXI3Slot15", "PXI3Slot16"]
dnVmon = ["PXI0Slot9", "PXI0Slot10", "PXI0Slot11", "PXI0Slot12"]
dnIsol = ["PXI1Slot9", "PXI1Slot10", "PXI1Slot11", "PXI1Slot12"]
dnFC = ["PXI4Slot11"]
dnInjImon = ["PXI3Slot4"]
dnBlum = ["PXI4Slot12", "PXI4Slot14", "PXI4Slot15", "PXI5Slot4", "PXI5Slot5"]
dnFFS = ["PXI0Slot16"]
dnVmon = dnVmon + dnBlum

dnlist = dnBPM + dnVmon + dnIsol + dnImon + dnFC + ["picoscope01"] + dnFFS + dnInjImon

# Timing to check
# List all PXI trigger crates. Each crate has 3 buses (slot 3-6, 7-12, 13-18), which can have independt Triggers
# Therefore we sort the cards by the bus they belong to
PXITRIGGERCARDS = [
    [[3, 4], [7, 8], [13, 14]],
    [[3, 4, 5, 6], [7, 8], [13]],
    [[], [], []],
    [[3], [7], [13]],
    [[3], [7], [13]],
    [[3], [7], []],
]


class Timeout:
    """Timeout class using ALARM signal."""

    class Timeout(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.raise_timeout)
        signal.alarm(self.sec)

    def __exit__(self, *args):
        signal.alarm(0)  # disable alarm

    def raise_timeout(self, *args):
        raise Timeout.Timeout()


def check_double_trigger(sn):
    for dev in sorted(dnlist):
        if not dev.startswith("PXI"):
            return
        crate = int(dev[3])
        triggerdevice = dev[:4] + " Timing"
        slot = int(dev[8:])
        tocheck = PXITRIGGERCARDS[crate]
        allcards = tocheck[0] + tocheck[1] + tocheck[2]
        if slot < 7:
            tocheck = tocheck[0]
        elif slot > 12:
            tocheck = tocheck[2]
        else:
            tocheck = tocheck[1]
        active = 0
        delay = 0
        for cards in tocheck:
            # get active channels and delay
            config = DB.get_setting(sn, triggerdevice)
            if config is None:
                out = colored(
                    "Can't get settings for {}".format(triggerdevice),
                    "red",
                    attrs=["reverse"],
                )
            else:
                config = config["Trigger Modules"]
                config = config[allcards.index(cards)]
                config = config["Board Config"]["Aux Channels"]
                for ch in config:
                    if ch["connect to PXI"] and ch["output enable"]:
                        active += 1
                    delay = max(delay, ch["delay (s)"])
        if active == 0:
            out = colored(
                "No Trigger connected to {}".format(dev), "red", attrs=["reverse"]
            )
            print(out)
        elif active > 1:
            out = colored(
                "Too many Trigger connected to {}.".format(dev),
                "red",
                attrs=["reverse"],
            )
            print(out)
            print(
                "Check Timing-raw in LabVIEW. Make sure that for cards in slots 3-6, 7-12, 13-18 only one Aux channel is active."
            )
        else:
            if crate == 1:
                if not np.isclose(float(delay), 50e-9, atol=100e-9):
                    out = colored(
                        "Trigger for {} at {} (should be 50ns)".format(dev, delay),
                        "red",
                        attrs=["reverse"],
                    )
                    print(out)
            else:
                if not np.isclose(float(delay), 1.23e-3, atol=10e-9):
                    out = colored(
                        "Trigger for {} at {} (should be 1.23ms)".format(dev, delay),
                        "red",
                        attrs=["reverse"],
                    )
                    print(out)


def get_shot_data(sn):
    bad = []
    print("    Collecting", len(dnlist), "device channels sn=", sn)
    # ------------------------------------------------------
    # test for existence of pkl file and check contents
    db_read = True
    slot_data = {}
    print("*" * 20 + str(sn) + "*" * 20)
    for dev in sorted(dnlist):
        try:
            query = Shot.objects(Shot.lvtimestamp == sn)
            query = query.filter(Shot.devicename == dev).limit(1).first()
            tmp = json.loads(query.data)
            tmp["length"] = tmp.pop("Actual Record Length")
            tmp["rate"] = tmp.pop("Actual Sample Rate")
            tmp["description"] = ""
            slot_data[dev] = tmp
        except:
            bad.append(dev)
            out = "error at device: {}".format(dev)
            l = len(out)
            if dev == "picoscope01":
                out = colored(out, "yellow", attrs=["reverse"])
            else:
                out = colored(out, "red", attrs=["reverse"])
            print("*" + " " * (47 - l) + out + " *")
    if len(slot_data) == len(dnlist):
        out = "       OK: {}".format(sn)
        l = len(out)
        out = colored(out, "green", attrs=["reverse"])
        print("* " + out + " " * (47 - l) + "*")

    return bad


def fix_dev(dev):
    print(" trying to save missing channel")
    s = c.socket(zmq.REQ)
    s.connect(address[dev])
    s.send(b"save")
    try:
        with Timeout(5):  #   skip if no signal is recieved after 5 seconds
            s.recv()
    except Timeout.Timeout:
        print("Timeout")
    s.close()


def log_bad_dev(dev, sn):
    if dev == "picoscope01":
        return
    with open("SHOT-MONITOR.log", "a") as logfile:
        logfile.write("{}: {}\n".format(sn, dev))


def check_BPM_scaling(sn):
    for dev, scaling in zip(dnBPM, BPMscaling):
        try:
            query = Shot.objects(Shot.lvtimestamp == sn)
            query = query.filter(Shot.devicename == dev).limit(1).first()
            if query:
                hash = query.settinghash
            else:
                hash = None
            dbdata = Setting.objects(Setting.hash == hash).limit(1).first()
            if dbdata:
                dbdata = dbdata.data
                if dbdata is not None:
                    dbdata = json.loads(dbdata)
            realscaling = dbdata["external attenuator/gain"]
            if dev == "PXI3Slot9":
                # broken channel, scaling factor for D is 100x gain
                if scaling != realscaling[0] and 0.01 != realscaling[1]:
                    out = "wrong BPM scaling factor at: {}".format(dev)
                    l = len(out)
                    out = colored(out, "red", attrs=["reverse"])
                    print("*" + " " * (47 - l) + out + " *")
                    print("current setting on p 86")
            else:
                if scaling != realscaling[0] and scaling != realscaling[1]:
                    out = "wrong BPM scaling factor at: {}".format(dev)
                    l = len(out)
                    out = colored(out, "red", attrs=["reverse"])
                    print("*" + " " * (47 - l) + out + " *")
                    print("current setting on p 86")
        except:
            out = "can't get scaling factor for BPM: {}".format(dev)
            l = len(out)
            out = colored(out, "red", attrs=["reverse"])
            print("*" + " " * (47 - l) + out + " *")
            print("current setting on p 86")


def check_Cell_scaling(sn):
    for dev, scaling in zip(dnVmon, Cellscaling):
        try:
            query = Shot.objects(Shot.lvtimestamp == sn)
            query = query.filter(Shot.devicename == dev).limit(1).first()
            if query:
                hash = query.settinghash
            else:
                hash = None
            dbdata = Setting.objects(Setting.hash == hash).limit(1).first()
            if dbdata:
                dbdata = dbdata.data
                if dbdata is not None:
                    dbdata = json.loads(dbdata)
            realscaling = dbdata["external attenuator/gain"]
            if scaling[0] != realscaling[0] and scaling[1] != realscaling[1]:
                out = "wrong BPM scaling factor at: {}".format(dev)
                l = len(out)
                out = colored(out, "red", attrs=["reverse"])
                print("*" + " " * (47 - l) + out + " *")
                print("current setting on p 86")
        except:
            out = "can't get scaling factor for BPM: {}".format(dev)
            l = len(out)
            out = colored(out, "red", attrs=["reverse"])
            print("*" + " " * (47 - l) + out + " *")
            print("current setting on p 86")


def check_trigger_routing(sn):
    A = DB.get_setting(sn, "PXI0 Timing")
    correct = [
        "PXI_Star0",
        "PXI_Star1",
        "PXI_Star4",
        "PXI_Star5",
        "PXI_Star10",
        "PXI_Star11",
        "PFI1",
        "PFI2",
        "PFI3",
        "PFI4",
        "PFI5",
    ]
    if A["Connections"]["Destination"] != correct:
        out = "Incorrect Destination in PXI0 Timing, the first 6 should be Star0,1,4,5,10,11"
        l = len(out)
        out = colored(out, "red", attrs=["reverse"])
        print("*" + " " * (47 - l) + out + " *")
    A = DB.get_setting(sn, "PXI1 Timing")["Connections"]
    if len(A["Destination"]) != len(A["Source"]):
        out = "Incorrect settings in PXI1 Timing: need to have the same amount of sources as destinations"
        l = len(out)
        out = colored(out, "red", attrs=["reverse"])
        print("*" + " " * (47 - l) + out + " *")


def all_checks(sn):
    print("Got shot: ", sn)
    sys.stdout.flush()
    bad = get_shot_data(sn)
    if bad:
        for dev in bad:
            if dev != "picoscope01":
                log_bad_dev(dev, sn)
                fix_dev(dev)
        time.sleep(2)
        bad = get_shot_data(sn)
        if bad:
            print(colored("couldn't fix it", "red", attrs=["reverse"]))
    check_Cell_scaling(sn)
    check_BPM_scaling(sn)
    check_double_trigger(sn)
    check_trigger_routing(sn)
    shot_warnings.check_shot(sn)
    shot_warnings.checktiming(sn)
    sys.stdout.flush()


# Local Variables:
# mode: python
# End:
