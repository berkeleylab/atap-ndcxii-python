"""Script to scrape data from NDCXII run March 5, 2021."""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from matplotlib.backends.backend_pdf import PdfPages
import pdb
import datetime

import NDCXII
import NDCXII.db as db
import NDCXII.device_lists as device_lists
import data_to_ignore as ignore


# db.enable_disc_cache()
db.set_cachedir("/Users/nickvalverde/Library/Caches/NDCXII/data")

# Useful constants
kV = 1e3
ms = 1e-3
us = 1e-6
ns = 1e-9

# Format datetime for saving
today = datetime.datetime.today()
date_string = today.strftime("%m-%d-%Y_%H-%M-%S_")

# ------------------------------------------------------------------------------
#     Utility functions
# Useful functions for performing computations, making plots, etc.
# ------------------------------------------------------------------------------
def get_total_charge(shot):
    try:
        time = shot.LargeFCupcollector.t
        volt = shot.LargeFCupcollector.x
    except AssertionError:
        print("No LargeFCup data.")
        return np.nan, np.nan

    tmax = time[np.argmax(volt)]

    dt = time[1] - time[0]

    mask = (time < tmax + 50e-9) & (time > tmax - 50e-9)
    total_charge = volt[mask].sum() * dt / 50

    return total_charge, tmax


def expt_plot(yearmonth=202103, daytime=51025, show=False):
    SRK = []
    timing = []
    charge = []
    for S in NDCXII.data.ShotsFromComment(yearmonth, daytime):
        # for S in sorted(NDCXII.data.ShotsFromComment(202103, 51130), key=lambda x: x.shotnumber):
        SRK.append([srk.voltagesetting for srk in S.SRK])
        total_charge, _ = get_total_charge(S)
        charge.append(total_charge)
        timing.append([v.timing["trigger"] for v in S.V[1:]])

    SRK = np.array(SRK).T
    timing = np.array(timing).T
    charge = np.array(charge)
    fig, ax = plt.subplots(3, 1, figsize=(6, 5), sharex=True)

    ax[0].scatter([i for i in range(len(charge))], charge / 1e-9, s=5)
    for s in SRK:
        ax[1].plot(s - s[0])
    for s in timing:
        ax[2].plot((s - s[0]) / 1e-6)

    ax[0].set_title("{}-{} Experiment Data".format(int(yearmonth), int(daytime)))
    ax[0].set_ylabel("Charge [nC]")

    ax[1].set_ylabel(r"$\Delta$Volt Setting [V]")
    ax[2].set_ylabel(r"$\Delta$Trigger Setting [$\mu$s]")
    ax[2].set_xlabel("Shot in Run")
    plt.tight_layout()
    if show:
        plt.show()
    else:
        return fig, ax


def make_arcI_plots(yearmonth, daytime=None):
    """Plot all arc current profiles from experiment"""
    if daytime != None:
        rocs_shots = NDCXII.data.ShotsFromComment(yearmonth, daytime)
        fig, ax = plt.subplots()
        ax.set_title(f"ArcI for {yearmonth}-{daytime}")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Voltage [V]")
        for shot in rocs_shots:
            try:
                t = shot.sourcearcI.t
                V = shot.sourcearcI.x
                ax.plot(t / ms, V)
            except (TypeError, AssertionError):
                print("No data to plot")

        save = f"{yearmonth}-{daytime}_arcI"
        plt.savefig(save, dpi=400)

    else:
        data = db.listcomments(yearmonth, skip4200=False)
        daytime = [elem.datetime for elem in data]
        fig, ax = plt.subplots()
        ax.set_title(f"ArcI for {yearmonth}")
        ax.set_xlabel("Time [ms]")
        ax.set_ylabel("Voltage [V]")
        for dt in daytime:
            rocs_shots = NDCXII.data.ShotsFromComment(yearmonth, dt)
            try:
                t = shot.sourcearcI.t
                V = shot.sourcearcI.x
                ax.plot(t / ms, V)
            except (TypeError, AssertionError):
                print("No data to plot")

        save = f"{yearmonth}-{daytime}_arcI"
        plt.savefig(save, dpi=400)

    return print(f"Plots created and saved as {save}.png")


def make_dataframe(yrmonth, comment=None, ignore_shots=True):
    """Collect experimental data into pandas dataframe

    Function is in hopes to make a quicker and more readable way of constructing
    the dataframe.

    ---Still under development
    """

    # Collect all shots numbers from comment. The shot numbers grabbed directly from
    # the rocs database come as a list of objects. The object is what is used
    # to grab the integer shot number and hence the distinct naming scheme
    _shots = []
    if comment != None:
        rocs_shots = NDCXII.data.ShotsFromComment(yrmonth, comment)
    else:
        data = db.list_comments(yrmonth, skip4200=False)  # Generator object
        for i, [m, d, s, n, c] in enumerate(data):
            this_comment = d
            these_rocs_shots = NDCXII.data.ShotsFromComment(yrmonth, this_comment)
            for s in these_rocs_shots:
                shots.append(int(s.shotnumber))

    _shots = np.array(shots)

    # Check if any shots should be ignored. If so, mask out bad shots
    if ignore_shots:
        try:
            bads = np.genfromtxt("badshots.csv", dtype="int")
        except FileNotFoundError:
            print("Provide csv file of bad shots titles badshots.csv")

    shots = _shots[(_shots != bads)]

    # Loop through all shots and construct dataframe row by row

    # Initialize datafame and create column headers
    df = pd.DataFrame()


def pdf_current_hist(
    yrmonth, comments, date=date_string, ignore_comments=None, ignore_shots=None
):
    """Create PDF summary of current histories for experiment"""
    # Number of plots per page
    num_plts_per_page = 4

    for DT in comments:
        if DT in ignore_comments:
            continue

        print("Working: ", DT)
        savestring = "{}{}_{}_CurrentProfiles.pdf".format(date, yrmonth, DT)
        with PdfPages(savestring) as pdf:
            # Make title page
            title = "Current Profiles for LargeFcup"
            shot_month = str(yyyymm) + " " + str(DT)
            plt.figure()
            plt.axis("off")
            plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
            plt.text(
                0.5,
                0.3,
                "Date Made Month-Day-Yr_Hr-Min-Sec:{}".format(date),
                ha="center",
                va="center",
            )
            plt.text(0.5, 0.2, "Shot YYYYMM Comment: {}".format(shot_month))
            pdf.savefig()
            plt.close()

            shots = NDCXII.data.ShotsFromComment(yyyymm, DT)

            for i in range(0, len(shots), num_plts_per_page):
                these_shots = shots[i : i + num_plts_per_page]
                fig, ax = plt.subplots(nrows=2, ncols=2)
                for s, a in zip(these_shots, ax.ravel()):
                    if s.shotnumber in ignore_shots:
                        continue

                    try:
                        s.LargeFCupcollector.plot(ax=a, label="Fcup", lw=1)
                        a.set_title(
                            "{}-{}-{}".format(yyyymm, DT, s.shotnumber), fontsize=8
                        )
                        a.legend(fontsize="x-small")

                    except AssertionError:
                        a.text(0.5, 0.5, "no data", ha="center", va="center")

                plt.tight_layout()
                pdf.savefig()
                plt.close()


def make_current_array(
    yrmonth, comments, date=date_string, ignore_comments=None, ignore_shots=None
):
    """Save current arrays as numpy arrays for future use"""

    # Data arrays are 1000 in length.
    array_length = 1000
    # Create empty list to store voltage and time arrays. Also store shot
    time_list = []
    volt_list = []
    shot_list = []

    shot_index = 0
    for DT in comments:
        if DT in ignore_comments:
            continue

        print("Working: ", DT)
        shots = NDCXII.data.ShotsFromComment(yyyymm, DT)

        for shot in shots:
            if shot.shotnumber in ignore_shots:
                continue

            try:
                volt = shot.LargeFCupcollector.x
                time = shot.LargeFCupcollector.t
                s = shot.shotnumber
                volt_list.append(volt)
                time_list.append(time)
                shot_list.append(s)

                shot_index += 1
            except AssertionError:
                print(f"No data for shot {s}")
                print("Setting values to NaN")

    volt_list = np.array(volt_list)
    time_list = np.array(time_list)
    shot_list = np.array(shot_list)
    np.save(f"./{date}voltages.npy", volt_list)
    np.save(f"./{date}times.npy", time_list)
    np.save(f"./{date}shots.npy", shot_list)


# ------------------------------------------------------------------------------
#     Collecting Functions
# These functions collect data from the various components of the system. Each
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
#     Experiment Summary
# This section will pull the data from the database. The main indices for
# pulling are the shot comment in YearMonth (YYYYMM) format, and then the
# comment number (#####). Using these two numbers can get all the shot
# for that run, i.e. run YYYMM #####. This section's code is analgous to the
# codes that create the output for <NDCXII-rawdata comments 202103>.
# ------------------------------------------------------------------------------

yyyymm = 202103
data = db.list_comments(yyyymm, skip4200=False)  # Generator object

# Load dictionaries holding the comments and shots to ignore.
ignore_comments = ignore.ignore_comments[yyyymm]
ignore_shots = ignore.ignore_shots[yyyymm]

daytime = []
total = 0
# Loop through generator object. This is pulled from line 245 in NDCXII-rawdata.
# the letters are m:month, d:daytime, s:species, n:num shots, c:comment
for i, [m, d, s, n, c] in enumerate(data):
    if str(d).endswith("4200"):
        total += n
    else:
        daytime.append(d)
        print(
            "{:3d}: {} {:6d} species: {:2} shots: {:3d} -- {}".format(
                i + 1, m, d, s, n, c.split("\n")[0]
            )
        )

print("total of {} shots".format(total))

# ------------------------------------------------------------------------------
#     Experimental Plots
# Here the experimental plots are made. These plots are created from the all
# the shots in a given run. These plots are saved and hand-copied into the
# logbook.
# In addition, the current histories are collected in pdf files named with
# naming schem 202103_#comment_CurrentProfiles.
# ------------------------------------------------------------------------------
make_expt_plots = False
if make_expt_plots:
    for DT in daytime:
        print("Making plots for {}-{}".format(yyyymm, DT))
        fig, ax = expt_plot(yyyymm, DT, show=False)
        save_str = "{}_{}plot".format(yyyymm, DT)
        plt.savefig(save_str, dpi=400)
        plt.close()

pdf_current_hist(
    yyyymm,
    daytime,
    date_string,
    ignore_comments=ignore_comments,
    ignore_shots=ignore_shots,
)
make_current_array(
    yyyymm,
    daytime,
    date_string,
    ignore_comments=ignore_comments,
    ignore_shots=ignore_shots,
)

# ------------------------------------------------------------------------------
#     Dataframe Construction
# Dataframes are not simple to make from scratch and are best created from
# using previously collected data. To create it, the data is collected first
# into multidimensional arrays. The first index of the arrays are the number of
# devices. For example, SRK_data is (28 by total, by 2) dimensional. There are
# 28 SRK's have a 'total' amount of values to collect and 2 values are being
# collected, the voltage peak and time of voltage peak.
# After looping through each shot number and appending the data, the arrays are
# then turned into dictionaries which are then used to populate the dataframe.
# ------------------------------------------------------------------------------

# Create SRK tensor along with the peak voltage and time of peak volt arrays.
SRK_data = np.zeros(shape=(28, total, 2))

# Create Compression cell tensor holding the peak voltage and time of peak volt
Cell_data = np.zeros(shape=(7, total, 2))

# Create Blumlein tensor holding the peak voltage and time of peak voltage
Blumlein_data = np.zeros(shape=(5, total, 2))

# Create Large Faraday Cup (target) tensor holding  total charge, peak voltage,
# and time of peak voltage
LargeFcup_data = np.zeros(shape=(total, 3))

# Create array holding shots
shot_number = np.zeros(total)

# Create array for holding arc current. 0.2ms is good spot for taking Vpeak
arcI_data = np.zeros(total)
arcI_tselect = 0.2 * ms

# Loop through shots and populate data lists with information. The top loop
# will loop through the shot comments. The subsequent loop will go through
# each run for that comment
shot_index = 0

for comment_num in daytime:
    if comment_num in ignore_comments:
        continue

    print("Working comment: ", comment_num)
    for shot in NDCXII.data.ShotsFromComment(yyyymm, comment_num):
        if shot in ignore_shots:
            continue

        # Calculate the peak current for this run and the time at which the
        # current peaks.
        try:
            Qtotal, tpeak = get_total_charge(shot)
            Vpeak = np.max(shot.LargeFCupcollector.x)
            LargeFcup_data[shot_index, :] = Qtotal, tpeak, Vpeak
        except AssertionError:
            print("--No data for LargeFCup. Setting Values to nan")
            Qtotal, tpeak, Vpeak = np.nan, np.nan, np.nan
            LargeFcup_data[shot_index, :] = Qtotal, tpeak, Vpeak

        # Grab voltage output at tselect for the arc source
        this_arcI = shot.sourcearcI
        try:
            this_arcI_dt = this_arcI.t[1] - this_arcI.t[0]
            this_arcI_tind = np.where(
                (this_arcI.t > 0.2 * ms - this_arcI_dt)
                & (this_arcI.t < 0.2 * ms + this_arcI_dt)
            )[0][0]
            this_arcI_V = this_arcI.x[this_arcI_tind]
            arcI_data[shot_index] = this_arcI_V
        except (AssertionError, TypeError):
            print(f"--No data for ArcI. Setting to NaN")
            arcI_data[shot_index] = np.nan

        # Instantiate SRK and grab characteristics
        this_SRK = shot.SRK
        Vpeaks = np.zeros(len(this_SRK))
        tpeaks = Vpeaks.copy()

        for i, srk in enumerate(this_SRK):
            try:
                ind_max = np.argmax(srk.x)
                Vpeaks[i] = srk.x[ind_max]
                tpeaks[i] = srk.t[ind_max]
            except AssertionError:
                print("--No data found for ", srk.name)
                print("--Setting Vpeak and tpeak to NaN")
                Vpeaks[i] = np.nan
                tpeaks[i] = np.nan

        SRK_data[:, shot_index, 0] = Vpeaks
        SRK_data[:, shot_index, 1] = tpeaks

        # Instantiate Blumelein and grab characteristics
        this_BL = shot.BL
        Vpeaks = np.zeros(len(this_BL))
        tpeaks = Vpeaks.copy()

        for i, bl in enumerate(this_BL):
            try:
                ind_max = np.argmax(bl.x)
                Vpeaks[i] = bl.x[ind_max]
                tpeaks[i] = bl.t[ind_max]
            except (AssertionError, AttributeError):
                print("--No data found for ", bl.name)
                print("--Setting Vpeak and tpeak to NaN")
                Vpeaks[i] = np.nan
                tpeaks[i] = np.nan

        Blumlein_data[:, shot_index, 0] = Vpeaks
        Blumlein_data[:, shot_index, 1] = tpeaks

        # Instantiate Compression cells and grab characteristics
        this_Cell = shot.Cells
        Vpeaks = np.zeros(len(this_Cell))
        tpeaks = Vpeaks.copy()

        for i, cell in enumerate(this_Cell):
            try:
                ind_max = np.argmax(cell.x)
                Vpeaks[i] = cell.x[ind_max]
                tpeaks[i] = cell.t[ind_max]
            except AssertionError:
                print("--No data found for ", cell.name)
                print("--Setting Vpeak and tpeak to NaN")
                Vpeaks[i] = np.nan
                tpeaks[i] = np.nan

        Cell_data[:, shot_index, 0] = Vpeaks
        Cell_data[:, shot_index, 1] = tpeaks

        shot_number[shot_index] = shot.shotnumber
        shot_index += 1

# Initialize the dataframe with the shot number, and LargeFcup data (Q, tpeak, and
# vpeak)
seed_data = {
    "Shot": shot_number,
    "Qtot": LargeFcup_data[:, 0],
    "tpeak": LargeFcup_data[:, 1],
    "Vpeak": LargeFcup_data[:, 2],
    "ArcI V": arcI_data,
}
df = pd.DataFrame(seed_data)
# Convert shot to integer number
df["Shot"] = df["Shot"].astype(int)

# Dataframes are much more maleable with dictionaries. This loop will grab the
# SRK characteristics and turn them into dictionaries with SRK## keys and
# voltage setting and trigger as values.
SRK_dict = {}
for i, srk in enumerate(device_lists.SRK_names):
    vpeak_name = srk + " " + "Vpeak"
    tpeak_name = srk + " " + "tpeak"

    # Grab data and populate dictionary
    SRK_dict[vpeak_name] = SRK_data[i, :, 0]
    SRK_dict[tpeak_name] = SRK_data[i, :, 1]

BL_dict = {}
for i, bl in enumerate(device_lists.Blumlein):
    vset_name = bl + " " + "Vpeak"
    tmax_name = bl + " " + "tpeak"

    # Grab data and populate dictionary
    BL_dict[vset_name] = Blumlein_data[i, :, 0]
    BL_dict[tmax_name] = Blumlein_data[i, :, 1]

Cell_dict = {}
for i, cell in enumerate(device_lists.Compr_cell):
    vset_name = cell + " " + "Vpeak"
    tmax_name = cell + " " + "tpeak"

    # Grab data and populate dictionary
    Cell_dict[vset_name] = Cell_data[i, :, 0]
    Cell_dict[tmax_name] = Cell_data[i, :, 1]

# Create column names for the dataframe and add columns with information
for column_name in list(SRK_dict.keys()):
    df[column_name] = SRK_dict[column_name]

for column_name in list(BL_dict.keys()):
    df[column_name] = BL_dict[column_name]

for column_name in list(Cell_dict.keys()):
    df[column_name] = Cell_dict[column_name]

df.to_csv(f"{date_string}{yyyymm}-dataframe.csv")
