# Short script to look at the relationship between the total charge delivered on
# target and the arc source current. In experiments, it was noticed that the
# total charge would have large fluctuations when running an optimization routine
# even when it was clear that the program had found some sort of maxima. This
# was because the arc source also had fluctations and not as much current was
# able to be extracted.

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pdb
import datetime
import os
from matplotlib.backends.backend_pdf import PdfPages

import NDCXII
import NDCXII.data
import NDCXII.db as db
import NDCXII.device_lists as device_lists

# Set cache directory. This only works if user has already collected and stored
# the data from Rocs through db.enable_disc_cache()
db.set_cachedir("/Users/nickvalverde/Library/Caches/NDCXII/data")
ms = 1e-3
nC = 1e-9
us = 1e-6


def get_total_charge(shot):
    try:
        time = shot.LargeFCupcollector.t
        volt = shot.LargeFCupcollector.x
    except AssertionError:
        print("No LargeFCup data.")
        return np.nan, np.nan

    tmax = time[np.argmax(volt)]

    dt = time[1] - time[0]

    mask = (time < tmax + 50e-9) & (time > tmax - 50e-9)
    total_charge = volt[mask].sum() * dt / 50

    return total_charge, tmax


# ------------------------------------------------------------------------------
#                   Initial Data and Visualization
# Read in the dataframe from a filtered dataset. This filtering is done in a
# previous scripts. The filtering mainly removes any clearly bad shots such as
# unrealistic values or shots that had incomplete data.
# Nan values here turned into zeroes
# The visualization shows the both the total charge collected and the arc current
# voltage just before the beam is extracted. Both are plotted along with the
# the an arbitrary index value that the order of the shot (first to last).
# ------------------------------------------------------------------------------
data = pd.read_csv("202109-filtered_dataframe.csv")
shots = data["Shot"].to_numpy()


arcI_tpeak = np.loadtxt("arcI_tpeak")
arcI_tpeak = np.nan_to_num(arcI_tpeak)
arcI_volt = np.loadtxt("arcI_volt")
arcI_volt = np.nan_to_num(arcI_volt)
qtot = np.loadtxt("qtot")
norm_qtot = qtot / arcI_volt
norm_qtot = np.nan_to_num(norm_qtot)

Fcup_tpeak = np.loadtxt("Fcup_tpeak")
arb_ind = np.array([(i + 1) for i in range(len(qtot))])

fig, ax = plt.subplots()
ax.scatter(arb_ind, qtot / nC, s=8)
ax.set_title("Total Integrated Charge (Filtered Data)")
ax.set_xlabel("Arbitrary Index")
ax.set_ylabel("Integrated Charge [nC]")
plt.show()

mask_q = norm_qtot >= 0.0
fig, ax = plt.subplots()
ax.scatter(arb_ind[mask_q], norm_qtot[mask_q] / nC, s=8)
ax.set_title("Total Integrated Charge Normalized by Arccurrent (Filtered Data)")
ax.set_xlabel("Arbitrary Index")
ax.set_ylabel("Integrated Charge Dividing by Arc Voltage [nC/V]")
plt.show()

fig, ax = plt.subplots()
mask = arcI_tpeak > 1e-7 / ms
ax.scatter(Fcup_tpeak[mask] / us, arcI_tpeak[mask] / ms, s=10)
ax.set_xlabel(r"Time of Peak Voltage in Fcup [$\mu s$]")
ax.set_ylabel(r"Time of Peak Voltage in Arc source [ms]")
plt.show()

arcI_tpeak = []
arcI_volt = []
qtot = []
Fcup_tpeak = []
no_data_shots = []

for i, shot in enumerate(shots):
    print(f"Working shot {i+1}/{len(shots)}")
    s = NDCXII.data.Shotdata(shot)
    arcI = s.sourcearcI
    fcup = s.LargeFCupcollector

    try:
        arcI_ind = np.argmin(arcI.x)
        arcI_dt = arcI.t[1] - arcI.t[0]
        arcI_tind = np.where(
            (arcI.t > 0.2 * ms - arcI_dt) & (arcI.t < 0.2 * ms + arcI_dt)
        )[0][0]
        arcI_V = arcI.x[arcI_tind]
        arcI_tpeak.append(arcI.t[arcI_ind])
        arcI_volt.append(arcI_V)

    except AssertionError:
        print(f"no data for shot {shot}")
        arcI_tpeak.append(np.nan)
        arcI_volt.append(np.nan)
        no_data_shots.append(shot)

    q, fcup_tmax = get_total_charge(s)
    qtot.append(q)
    Fcup_tpeak.append(fcup_tmax)

np.savetxt("arcI_tpeak", arcI_tpeak)
np.savetxt("arcI_volt", arcI_volt)
np.savetxt("qtot", qtot)
np.savetxt("Fcup_tpeak", Fcup_tpeak)
np.savetxt("no_data_shots", no_data_shots)
