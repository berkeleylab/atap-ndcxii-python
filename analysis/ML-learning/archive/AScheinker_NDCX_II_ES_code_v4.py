#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 12:14:10 2021

@author: Alexander Scheinker
"""

import time

import numpy as np
import matplotlib.pyplot as plt


import NDCXII

font_size = 12
plt.rcParams.update({"font.size": font_size})

# Useful Constants
ms = 1e-3
ns = 1e-9
kV = 1e3
nC = 1e-9

# Total number of steps to take
nES_steps = 600

a_scale = 1.0

# Keep track of the total cost function to be maximized
# Which is the total charge in a 3 ns window
# around the current peak
cES_Q = np.zeros(nES_steps)

# Set up ES parameters for gaps
nES_params = 12
gap_ES = np.zeros([nES_steps, nES_params])
wES = np.linspace(100, 175.149, nES_params // 2)
dtES = 2 * np.pi / (10.0 * np.max(wES))

# aES = np.max(wES)*(0.01e-6)**2
aES = np.zeros(nES_params)
aES[:7] = 0.25 * np.max(wES) * (100e-9) ** 2
aES[7:] = 0.25 * np.max(wES) * (10e-9) ** 2

# Use for cES_ns maximization
# kES = 0.5*0.5*(1.0/20.0)*1e-2
kES = 0.25
kES = 0.025
kES = 0.0015
kES = 0.003

# Set up parameter limits
pES_max = 0.00124
pES_min = 0.00121


# Set up ES parameters for solenoids
nES_params_sol = 28
sol_ES = np.zeros([nES_steps, nES_params_sol])
wES_sol = np.linspace(100, 175.149, nES_params_sol // 2)
dtES_sol = 2 * np.pi / (21.0 * np.max(wES))

rand_phase = np.pi * np.random.rand(nES_params) - np.pi / 2.0
rand_phase_sol = np.pi * np.random.rand(nES_params_sol) - np.pi / 2.0

aES_sol = np.zeros(nES_params_sol) + 0.05 * np.max(wES_sol) * (1) ** 2
# aES_sol = np.zeros(nES_params_sol)
# Use for cES_ns maximization
# kES_sol = 0.5*2.0*0.5*(1.0/20.0)*1e-2
kES_sol = 0.01
kES_sol = 0.0075

# Set up parameter limits
pES_max_sol = 3000
pES_min_sol = 0


# kES = 0
# kES_sol = 0


def get_last_shot():
    """return the last shotnumber.

    Default to a working shotnumber for offline testing or if we
    haven't taken a shot yet today.

    """
    try:
        shotnumber = NDCXII.control.last_shot()
    except IndexError:
        shotnumber = 3683645181

    return shotnumber


start_number = 3683645181
start_number = 3697819139
start_number = 3697826250
start_number = 3697827853
start_number = 3698421898
start_number = 3698426772
shotnumber = get_last_shot()
SRKs = NDCXII.control.load_setting(start_number, "SRKs")
timing_names = [f"Cell{i}" for i in range(1, 8)] + [f"BL{i}" for i in range(1, 6)]

# get initial values for timing and solenoids
gap_ES[0] = np.array(
    [NDCXII.control.get_timing(dev, shotnumber) for dev in timing_names]
)

# Good solenoid values to start with
sol_ES[0] = np.array(list(SRKs.values()))

# Function that loads in the current profile from the Faraday cup
# and sums up the total charge in a 3 ns window around the peak


def charge_near_peak(shotdata):
    # get time and current of last shotdata in the FCup
    current_t = shotdata.LargeFCupcollector.t
    current_I = shotdata.LargeFCupcollector.x

    # Sampling time of current profile
    dt_I = current_t[1] - current_t[0]

    # Find the peak of the current profile
    t_max_index = np.argmax(current_I)
    t_max_time = current_t[t_max_index]

    # Find all points within a 15 ns range around the peak
    dt_keep = 15 * ns
    t_good = (current_t >= t_max_time - dt_keep) & (current_t <= t_max_time + dt_keep)

    # Integrate the total charge in that window in nC
    beam_charge = dt_I * current_I[t_good].sum() / nC
    # fig, ax =plt.subplots()
    # ax.plot(current_t, current_I, color='black')
    # ax.plot(current_t[t_good], current_I[t_good], color='red')
    # plt.show(block=False)
    print(f"beam charge now = {beam_charge:.2f} [nC] {shotdata.shotnumber}")

    return beam_charge


# Import the Faraday cup current profile and calculate charge in 3 ns window
cES_Q[0] = charge_near_peak(NDCXII.data.Shotdata(start_number))


# Save everything
np.save("cES_Q.npy", cES_Q)
np.save("gap_ES.npy", gap_ES)
np.save("sol_ES.npy", sol_ES)


def update_value(last, step, idx, dt, w, k, c_Q, a, rp, clip):
    if (-1) ** idx > 0:
        func = np.sin
    else:
        func = np.cos

    value = (
        last[idx]
        + a_scale
        * dt
        * func(step * dt * w[idx // 2] + rp[idx] - k * c_Q[step])
        * (a[idx] * w[idx // 2]) ** 0.5
    )
    value = np.clip(value, clip[0], clip[1])

    return value


# Now we enter the iterative loop
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 12:14:10 2021

@author: Alexander Scheinker
"""

import time

import numpy as np
import matplotlib.pyplot as plt


import NDCXII

font_size = 12
plt.rcParams.update({"font.size": font_size})


# Total number of steps to take
nES_steps = 600

a_scale = 1.0

# Keep track of the total cost function to be maximized
# Which is the total charge in a 3 ns window
# around the current peak
cES_Q = np.zeros(nES_steps)

# Set up ES parameters for gaps
nES_params = 12
gap_ES = np.zeros([nES_steps, nES_params])
wES = np.linspace(100, 175.149, nES_params // 2)
dtES = 2 * np.pi / (10.0 * np.max(wES))

# aES = np.max(wES)*(0.01e-6)**2
aES = np.zeros(nES_params)
aES[:7] = 0.25 * np.max(wES) * (50e-9) ** 2
aES[7:] = 0.25 * np.max(wES) * (10e-9) ** 2

# Use for cES_ns maximization
# kES = 0.5*0.5*(1.0/20.0)*1e-2
kES = 0.25
kES = 0.025
kES = 0.0015
kES = 0.003

# Set up parameter limits
pES_max = 0.00124
pES_min = 0.00121


# Set up ES parameters for solenoids
nES_params_sol = 28
sol_ES = np.zeros([nES_steps, nES_params_sol])
wES_sol = np.linspace(100, 175.149, nES_params_sol // 2)
dtES_sol = 2 * np.pi / (21.0 * np.max(wES))

rand_phase = np.pi * np.random.rand(nES_params) - np.pi / 2.0
rand_phase_sol = np.pi * np.random.rand(nES_params_sol) - np.pi / 2.0

aES_sol = np.zeros(nES_params_sol) + 0.05 * np.max(wES_sol) * (1) ** 2
# aES_sol = np.zeros(nES_params_sol)
# Use for cES_ns maximization
# kES_sol = 0.5*2.0*0.5*(1.0/20.0)*1e-2
kES_sol = 0.01
kES_sol = 0.0075

# Set up parameter limits
pES_max_sol = 3000
pES_min_sol = 0


# kES = 0
# kES_sol = 0


def get_last_shot():
    """return the last shotnumber.

    Default to a working shotnumber for offline testing or if we
    haven't taken a shot yet today.

    """
    try:
        shotnumber = NDCXII.control.last_shot()
    except IndexError:
        shotnumber = 3683645181

    return shotnumber


start_number = 3683645181
start_number = 3697819139
start_number = 3697826250
start_number = 3697827853
start_number = 3698421898
start_number = 3698426772
shotnumber = get_last_shot()
SRKs = NDCXII.control.load_setting(start_number, "SRKs")
timing_names = [f"Cell{i}" for i in range(1, 8)] + [f"BL{i}" for i in range(1, 6)]

# get initial values for timing and solenoids
gap_ES[0] = np.array(
    [NDCXII.control.get_timing(dev, start_number) for dev in timing_names]
)

# Good solenoid values to start with
sol_ES[0] = np.array(list(SRKs.values()))

# Function that loads in the current profile from the Faraday cup
# and sums up the total charge in a 3 ns window around the peak


def charge_near_peak(shotdata):
    # get time and current of last shotdata in the FCup
    current_t = shotdata.LargeFCupcollector.t
    current_I = shotdata.LargeFCupcollector.x

    # Sampling time of current profile
    dt_I = current_t[1] - current_t[0]

    # Find the peak of the current profile
    t_max_index = np.argmax(current_I)
    t_max_time = current_t[t_max_index]

    # Find all points within a 5 ns range around the peak
    dt_keep = 15e-9
    t_good = (current_t >= t_max_time - dt_keep) & (current_t <= t_max_time + dt_keep)

    # Integrate the total charge in that window in nC
    beam_charge = dt_I * current_I[t_good].sum() / 1e-9
    # fig, ax =plt.subplots()
    # ax.plot(current_t, current_I, color='black')
    # ax.plot(current_t[t_good], current_I[t_good], color='red')
    # plt.show(block=False)
    print(f"beam charge now = {beam_charge:.2f} [nC] {shotdata.shotnumber}")

    return beam_charge


# Import the Faraday cup current profile and calculate charge in 3 ns window
cES_Q[0] = charge_near_peak(NDCXII.data.Shotdata(start_number))


# Save everything
np.save("cES_Q.npy", cES_Q)
np.save("gap_ES.npy", gap_ES)
np.save("sol_ES.npy", sol_ES)


def update_value(last, step, idx, dt, w, k, c_Q, a, rp, clip):
    if (-1) ** idx > 0:
        func = np.sin
    else:
        func = np.cos

    value = (
        last[idx]
        + a_scale
        * dt
        * func(step * dt * w[idx // 2] + rp[idx] - k * c_Q[step])
        * (a[idx] * w[idx // 2]) ** 0.5
    )
    value = np.clip(value, clip[0], clip[1])

    return value


# Now we enter the iterative loop
fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(8, 12))

for jES in np.arange(nES_steps - 1):

    # Show progress
    print(f"starting ES step {jES+2}/{nES_steps}")

    # Update the gap parameters based on the Faraday cup
    for i in range(12):
        gap_ES[jES + 1, i] = update_value(
            gap_ES[jES],
            jES,
            i,
            dtES,
            wES,
            kES,
            cES_Q,
            aES,
            rand_phase,
            clip=[pES_min, pES_max],
        )

    # Update the solenoid parameters based on the Faraday cup
    for i in range(28):
        sol_ES[jES + 1, i] = update_value(
            sol_ES[jES],
            jES,
            i,
            dtES_sol,
            wES_sol,
            kES_sol,
            cES_Q,
            aES_sol,
            rand_phase_sol,
            clip=[pES_min_sol, pES_max_sol],
        )

    # Write the gap and solenoid values to NDCX-II
    new_SRKs = {f"SRK{i+1:02d}": sol_ES[jES + 1, i] for i in range(28)}
    NDCXII.control.modify_SRK_voltages(new_SRKs)
    print(new_SRKs)

    for i, t in enumerate(timing_names):
        NDCXII.control.modify_timing(t, gap_ES[jES + 1, i])
        print(t, gap_ES[jES + 1, i])

    # Run NDCX-II
    print("Trigger NDCXII in 5 seconds", flush=True)
    time.sleep(5)
    NDCXII.control.trigger()
    print("Trigger!!!", flush=True)
    time.sleep(25)

    # Record data

    # Import the Faraday cup current profile and calculate charge in 3 ns window
    shotnumber = get_last_shot()
    shotdata = NDCXII.data.Shotdata(shotnumber)

    cES_Q[jES + 1] = charge_near_peak(shotdata)

    # Save everything
    np.save("cES_Q.npy", cES_Q)
    np.save("gap_ES.npy", gap_ES)
    np.save("sol_ES.npy", sol_ES)

    a_scale *= 0.995

    # Plot the progress to see how things are going
    fig.clf

    ax[0, 0].plot(cES_Q[0 : jES + 1])
    ax[0, 0].plot(cES_Q[0 : jES + 1], "k.")
    ax[0, 0].set_ylabel(r"$Q_{peak,5ns}$ [nC]")
    ax[0, 0].set_xticks([])

    ax[1, 0].plot(gap_ES[0 : jES + 1] / 1e-9 - gap_ES[0] / 1e-9)
    ax[1, 0].set_ylabel("$\Delta$g [ns]")
    ax[1, 0].set_xticks([])

    ax[2, 0].plot(sol_ES[0 : jES + 1] - sol_ES[0])
    ax[2, 0].set_ylabel("Solenoid Voltage [V]")
    ax[2, 0].set_xlabel("Step Number")

    plt.pause(0.25)

    shotdata.LargeFCupcollector.plot(ax=ax[0, 1])

    plt.tight_layout()
    plt.pause(0.25)

    print("Waiting 20s befor next shot", flush=True)
    time.sleep(20)
