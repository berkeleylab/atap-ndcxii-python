#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 10:22:24 2021

@author: ascheink
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from sklearn.preprocessing import StandardScaler, QuantileTransformer

font_size = 12
plt.rcParams.update({"font.size": font_size})

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"
PATH_TO_DATA = "/Users/nickvalverde/Desktop/"

# Initial data loading
# Load the voltage data
yyyymm = 202103
volt_profiles = np.load(PATH_TO_DATA + f"{yyyymm}_Fcup_volt_profile.npy")
# Load the time data
t_profiles = np.load(PATH_TO_DATA + f"{yyyymm}_Fcup_time_profile.npy")
# Load the data frame with solenoids, cells, and gaps
df = pd.read_csv(PATH_TO_DATA + f"{yyyymm}-filtered_dataframe.csv")
if yyyymm == 202103:
    # Drop negative Q shots for 202103. Quick fix for now.
    df.drop(index=[3, 4, 6], axis=0, inplace=True)
    volt_profiles = np.delete(volt_profiles, [3, 4, 6], axis=0)
    t_profiles = np.delete(t_profiles, [3, 4, 6], axis=0)
# ------------------------------------------------------------------------------
#     Constants and Parameters
# Useful constants for the script and general parameters are defined here.
# ------------------------------------------------------------------------------
ns = 1e-9
us = 1e-6
nC = 1e-9

# Define number of compression cells and blumleins
N_compr_cells = 7
N_bls = 5

N_shots = len(volt_profiles)

# ------------------------------------------------------------------------------
#     Functions
# Utility functions such as plotting and analysis are defined here. Other
# functions that are required by the script should be put here.
# ------------------------------------------------------------------------------
def plot_bl_hist(data, title="Blumlein Data", nbins=50, xlabel="", ylabel="", save=""):
    """Make a Histogram of the data given for each blumlein

    The primary data for the blumleins are the peak voltage and the time when
    the voltage peaked. Either one of these array scan be input as data and this
    array is assumed to be a 1D array containg the float value.
    """
    # There are 5 blumleins so the data will be put on 5-panel plot
    fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(15, 7))
    for ax, bl in zip(axes.ravel(), data):
        ax.hist(bl, bins=nbins, edgecolor="k", lw=0.5)

    # Check labels. Only put labels on first plot since the other plots will
    # be the same.
    if len(xlabel) > 0:
        ax.set_xlabel(xlabel)
    if len(ylabel) > 0:
        ax.set_ylabel(ylabel)

    fig.suptitle(title)
    plt.tight_layout()
    if len(save) > 0:
        plt.savefig(save, dpi=400)


# ------------------------------------------------------------------------------
#     Data Shaping/Handling
# Here, the data is extracted form the data set and the data arrays for various
# objects (solenoids, cells, blumleins, etc) are defined. Any other data
# manipulations should be performed here, such as dropping data points, shifting
# values, normalization schemes, etc.
# ------------------------------------------------------------------------------
# Load all the solenoids
S_all = np.zeros([N_shots, 28])
for n in np.arange(28):
    if n < 9:
        S_all[:, n] = np.array(df[f"SRK0{n+1} Vpeak"])
    else:
        S_all[:, n] = np.array(df[f"SRK{n+1} Vpeak"])

# Load all the cells. Create empty array then populate with compression cell and
# blumlein data
C_all = np.zeros([N_shots, 12])
for i in range(N_compr_cells):
    this_cell = np.array(df[f"Cell{i+1:1d} tpeak"])
    C_all[:, i] = this_cell

for i in range(N_bls):
    this_cell = np.array(df[f"BL{i+1:1d} tpeak"])
    C_all[:, i + N_compr_cells] = this_cell

# Load total charge in each shot
Q_total = np.array(df.Qtot)

# Find time at which the voltage waveform is maximum for each shot
t_max_index = np.argmax(volt_profiles, 1)
t_max = np.zeros(N_shots)

for i, index in enumerate(t_max_index):
    this_tprofile = t_profiles[i, :]
    t_max[i] = this_tprofile[index]

# Plot peak time vs total charge
plt.figure(1)
plt.plot(t_max / us, Q_total / nC, ".")
plt.xlabel("Time of peak [$\mu$s]")
plt.ylabel("Q [nC]")
plt.ylim([-1, 8])

# Keep index of relatively stable times only
indx_use_1 = np.where(t_max >= 3.54 * us)[0]
indx_use_2 = np.where(t_max <= 3.56 * us)[0]
indx_use = np.intersect1d(indx_use_1, indx_use_2)

# Keep index only for shots where cells were on
indx_cell = np.where(np.abs(C_all[:, 0]) > 0)[0]
for n in np.arange(12):
    if n != 5:
        indx_cell_temp = np.where(np.abs(C_all[:, n]) > 0)[0]
        indx_cell = np.intersect1d(indx_cell, indx_cell_temp)

indx_use = np.intersect1d(indx_use, indx_cell)

# Keep only shots with a decent amount of charge
indx_v = np.where(Q_total > 0.2 * nC)[0]
indx_use = np.intersect1d(indx_use, indx_v)

# Check the sampling times of each shot
dt_all = np.zeros(N_shots)
for n in np.arange(N_shots):
    dt_all[n] = t_profiles[n, 1] - t_profiles[n, 0]

plt.plot(dt_all, ".")
plt.show()

# Keep only those with fast enough sampling
indx_t = np.where(np.abs(dt_all - 0.2 * ns) < 0.1 * ns)[0]
# This is the index
indx_use = np.intersect1d(indx_use, indx_t)

# Plot peak time vs total charge
plt.figure(1)
plt.scatter(t_max / us, Q_total / nC, s=30, c="k", label="all data")
plt.scatter(
    t_max[indx_use] / us,
    Q_total[indx_use] / nC,
    s=5,
    c=t_max[indx_use] / us,
    label="data being used",
)
plt.xlabel("Time of peak [$\mu$s]")
plt.ylabel("Q [nC]")
plt.legend(frameon=False)
plt.colorbar()
plt.tight_layout()
plt.show()

# Total number of shots being used
n_data = len(indx_use)

# Randomize the indices
indx_rand = np.arange(n_data)
np.random.shuffle(indx_rand)

# Number of samples to keep for validation
n_val = 30

# Index of training data
indx_train = indx_use[indx_rand[0 : (n_data - n_val)]]

# Index of validation data
indx_val = indx_use[indx_rand[(n_data - n_val) :]]

# Normalize the data using standard scaler
# Training gaps and solenoids
x_gap_t = C_all[indx_train]
x_sol_t = S_all[indx_train]

# Validation gaps and solenoids
x_gap_v = C_all[indx_val]
x_sol_v = S_all[indx_val]

# Define standard scaler objects, these subtract mean and divide by standard deviation
gap_ss = StandardScaler()
sol_ss = StandardScaler()

# Fit the scalers with the training data
gap_ss.fit(x_gap_t)
sol_ss.fit(x_sol_t)

# Normalize the training data
x_gap_t_n = gap_ss.transform(x_gap_t)
x_sol_t_n = sol_ss.transform(x_sol_t)

# Normalize the validation data too
x_gap_v_n = gap_ss.transform(x_gap_v)
x_sol_v_n = sol_ss.transform(x_sol_v)

garbage
# Compare normalized and not-normalized
plt.figure(1, figsize=(15, 7))
for n in np.arange(12):
    plt.subplot(3, 4, n + 1)
    plt.hist(x_gap_t[:, n], 100)
    plt.xlabel(f"$g_{{{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()


plt.figure(2, figsize=(15, 7))
for n in np.arange(12):
    plt.subplot(3, 4, n + 1)
    plt.hist(x_gap_t_n[:, n], 100, color="r")
    plt.xlabel(f"$g_{{n,{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()

plt.figure(3, figsize=(15, 7))
for n in np.arange(28):
    plt.subplot(4, 7, n + 1)
    plt.hist(x_sol_t[:, n], 100)
    plt.xlabel(f"$s_{{{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()


plt.figure(4, figsize=(15, 7))
for n in np.arange(28):
    plt.subplot(4, 7, n + 1)
    plt.hist(x_sol_t_n[:, n], 100, color="r")
    plt.xlabel(f"$s_{{n,{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()
plt.show()

# Normalize the data using QuantileTransformer
gap_qt = QuantileTransformer(n_quantiles=700, output_distribution="uniform")
sol_qt = QuantileTransformer(n_quantiles=700, output_distribution="normal")

# Fit the scalers with the training data
gap_qt.fit(x_gap_t)
sol_qt.fit(x_sol_t)

# Normalize the training data
x_gap_t_n = gap_qt.transform(x_gap_t)
x_sol_t_n = sol_qt.transform(x_sol_t)

# Normalize the validation data too
x_gap_v_n = gap_qt.transform(x_gap_v)
x_sol_v_n = sol_qt.transform(x_sol_v)

# Compare normalized and not-normalized
plt.figure(1, figsize=(15, 7))
for n in np.arange(12):
    plt.subplot(3, 4, n + 1)
    plt.hist(x_gap_t[:, n], 100)
    plt.xlabel(f"$g_{{{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()


plt.figure(2, figsize=(15, 7))
for n in np.arange(12):
    plt.subplot(3, 4, n + 1)
    plt.hist(x_gap_t_n[:, n], 100, color="r")
    plt.xlabel(f"$g_{{n,{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()

plt.figure(3, figsize=(15, 7))
for n in np.arange(28):
    plt.subplot(4, 7, n + 1)
    plt.hist(x_sol_t[:, n], 100)
    plt.xlabel(f"$s_{{{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()


plt.figure(4, figsize=(15, 7))
for n in np.arange(28):
    plt.subplot(4, 7, n + 1)
    plt.hist(x_sol_t_n[:, n], 100, color="r")
    plt.xlabel(f"$s_{{n,{n+1}}}$")
    plt.ylabel("count")
plt.tight_layout()
plt.show()

# For voltage waveforms keep only data near the peak
t_start = 300
t_stop = 800

# Training and validation voltages
v_t = volt_profiles[indx_train, t_start:t_stop]
v_v = volt_profiles[indx_val, t_start:t_stop]

# Times
t_t = t_profiles[indx_train, t_start:t_stop]
t_v = t_profiles[indx_val, t_start:t_stop]

plt.figure(1)
for n in np.arange(10):
    plt.plot(v_t[n])
plt.tight_layout()

plt.figure(1, figsize=(15, 7))
for n in np.arange(50):
    plt.subplot(5, 10, n + 1)
    plt.hist(v_t[:, n + 100], 100)
plt.tight_layout()


plt.figure(2, figsize=(15, 7))
for n in np.arange(50):
    plt.subplot(5, 10, n + 1)
    plt.hist(v_t[:, n + 100], 100)
    plt.xlim([-5, 32])
plt.tight_layout()
plt.show()

# Define standard scaler for voltage
v_qt = QuantileTransformer(n_quantiles=700, output_distribution="normal")
v_qt.fit(v_t)
v_t_n = v_qt.transform(v_t)
v_v_n = v_qt.transform(v_v)

plt.figure(1, figsize=(15, 7))
for n in np.arange(50):
    plt.subplot(5, 10, n + 1)
    plt.hist(v_t_n[:, n + 100], 100)
    plt.xlim([-5, 5])
plt.tight_layout()
plt.show()

# Make a simple dense neural network
# Length of voltage vector
len_out = t_stop - t_start

# Solenoid value inputs
in_sol = keras.Input(shape=(28,))

# Gap value inputs
in_gap = keras.Input(shape=(12,))

# Mass input
# in_mass = keras.Input(shape=(1,))

# Regularizer weight
wL2 = 1e-4

# Solenoid inputs
dense_S1 = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(in_sol)
dense_S2 = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S1)
dense_S3 = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S2)

# Gap inputs
dense_G1 = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(in_gap)
dense_G2 = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_G1)
dense_G3 = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_G2)

# Mass inputs
# dense_M1 = layers.Dense(200, activation="selu", kernel_regularizer = tf.keras.regularizers.L2(l2=wL2))(in_mass)
# dense_M2 = layers.Dense(100, activation="selu", kernel_regularizer = tf.keras.regularizers.L2(l2=wL2))(dense_M1)
# dense_M3 = layers.Dense(50, activation="selu", kernel_regularizer = tf.keras.regularizers.L2(l2=wL2))(dense_M2)

# Combine inputs
dense_SG = tf.concat([dense_S3, dense_G3], 1)

# Generate waveforms
dense_SG1 = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_SG)
dense_SG2 = layers.Dense(
    250, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_SG1)
dense_SG_out = layers.Dense(len_out, activation="linear")(dense_SG2)

model_NDCX_data = keras.Model(
    inputs=[in_sol, in_gap], outputs=[dense_SG3], name="NDCX_II_data"
)

model_NDCX_data.summary()


# Train the neural network
n_epochs = 100
b_size = 16

opt = keras.optimizers.Adam(learning_rate=1e-4)

model_NDCX_data.compile(
    loss="mse", optimizer=opt, metrics=[tf.keras.metrics.RootMeanSquaredError()]
)

history = model_NDCX_data.fit(
    x=[x_sol_t_n, x_gap_t_n],
    y=[v_t_n],
    batch_size=b_size,
    epochs=n_epochs,
    validation_data=([x_sol_v_n, x_gap_v_n], [v_v_n]),
)

print("*******************************")
print("*******************************")
print("*******************************")
print("Finished step 1 of training")
print("*******************************")
print("*******************************")
print("*******************************")

plt.figure(9)
plt.plot(history.history["loss"], label="$train$")
plt.plot(history.history["val_loss"], label="$val$")
# plt.plot(history2.history['loss'],label='$train$')
# plt.plot(history2.history['val_loss'],label='$val$')
# plt.plot(history3.history['loss'],label='$train$')
# plt.plot(history3.history['val_loss'],label='$val$')
plt.legend(frameon=False)
plt.xlabel("step")
plt.ylabel("loss")
plt.tight_layout()
plt.show()

# Check predictions
pred_v_v_n = model_NDCX_data.predict([x_sol_v_n, x_gap_v_n])
pred_v_t_n = model_NDCX_data.predict([x_sol_t_n, x_gap_t_n])

# Un-normalize prediction
pred_v_v = v_qt.inverse_transform(pred_v_v_n)
pred_v_t = v_qt.inverse_transform(pred_v_t_n)

# Plot some results
n_offset = 0

plt.figure(1, figsize=(18, 8))
for n in np.arange(30):
    plt.subplot(3, 10, n + 1)
    plt.plot(t_v[n + n_offset], v_v[n + n_offset], "r", label="true")
    plt.plot(t_v[n + n_offset], pred_v_v[n + n_offset], "k", label="NN")
    if n == 0:
        plt.legend(frameon=False)
        plt.title("val")
    plt.ylim([-1, 41])
    if n > 0:
        if n != 10:
            if n != 20:
                plt.yticks([])
    if n < 20:
        plt.xticks([])

plt.tight_layout()

plt.figure(2, figsize=(18, 8))
for n in np.arange(30):
    plt.subplot(3, 10, n + 1)
    plt.plot(t_t[n + n_offset], v_t[n + n_offset], "r", label="true")
    plt.plot(t_t[n + n_offset], pred_v_t[n + n_offset], "b", label="NN")
    if n == 0:
        plt.legend(frameon=False)
        plt.title("train")
    plt.ylim([-1, 41])
    if n > 0:
        if n != 10:
            if n != 20:
                plt.yticks([])
    if n < 20:
        plt.xticks([])

plt.tight_layout()
plt.show()
