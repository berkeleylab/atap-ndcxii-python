"""This script will serve as a temporary holding place for doing more analysis
on the dataframe created from the scraper. The tools and procedures here will
eventually be moved (or put under a different name). But, in the meantime, this
will act as a test bed and isolated analysis script."""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pdb
import datetime
import os
from matplotlib.backends.backend_pdf import PdfPages

import NDCXII
import NDCXII.data
import NDCXII.db as db
import NDCXII.device_lists as device_lists
import data_to_ignore as ignore

yyyymm = 202111
db.enable_disc_cache()
db.set_cachedir("/Users/nickvalverde/Library/Caches/NDCXII/data")

# Load dictionaries holding the comments and shots to ignore.
ignore_comments = ignore.ignore_comments[yyyymm]
ignore_shots = ignore.ignore_shots[yyyymm]

# Create useful unit definitions
kV = 1.0e3
ms = 1.0e-3
us = 1.0e-6
ns = 1.0e-9
nC = 1.0e-9

# Create formatting for datetime saving. The format will be YearMonDay-HHMM
# where the hour is on 24 hour clock.
today = datetime.datetime.today()
date_stamp = today.strftime("%m-%d-%Y_%H-%M-%S_")

# ------------------------------------------------------------------------------
#     Utility Functions
# Functions used for various data scraping and plotting. Also a place to put
# functions that make the experimental plots used during the runs.
# ------------------------------------------------------------------------------
def plot_expt(
    yyyymm,
    comment,
    dt_keep=50 * ns,
    target_time=2.21 * us,
    ignore_shots=None,
    init_shot=3714773511,
):
    """Make plot used during experiment.

    The experimental plots made are 3 panels in total. The first panel has
    3 rows plotting the integrated charge, the voltatge reading from the
    arcsource at time near when the beam is fully extracted and the cost
    function.

    The second panel has three rows. Row 1 is the variation in time settngs
    for the blumleins and compression cells. The second rows it he various
    solenoid voltages for all 28 solenoids. Lastly, row 3 shows the time of
    peak voltage observed in the Fcup along with the target time.

    The last panel is the Fcup voltage profiles for each shot."""

    # Collect shots to ignore
    ignore = ignore_shots
    # Collect shots from indicated comment and avoid ignoreable shots
    shots = []
    data = NDCXII.data.ShotsFromComment(yyyymm, comment)
    for s in data:
        if s.shotnumber in ignore:
            continue
        shots.append(s.shotnumber)

    shots = np.array(shots)

    # Create the plots. Three figures are needed. The cond fig holds the
    # conductor characteristics including the target time.
    cost_fig, cost_ax = plt.subplots(nrows=3, figsize=(8, 12), sharex=True)
    cond_fig, cond_ax = plt.subplots(nrows=3, figsize=(8, 12), sharex=True)
    Fcup_fig, Fcup_ax = plt.subplots(figsize=(8, 12))

    # Create empty arrays for collecting the data values for each shot.
    bl_data = np.zeros(shape=(len(shots), 5))
    cell_data = np.zeros(shape=(len(shots), 6))
    srk_data = np.zeros(shape=(len(shots), 28))

    arcI_data = np.zeros(shape=(len(shots)))
    accum_Q = np.zeros(len(shots))
    peak_times = np.zeros(len(shots))

    # Main loop. For each shot collect the conductor objects and plot
    # characteristics. For the target time plots, some manipulation needs to be
    # done to find the average arrivate time of the beam and compare to the
    # target.
    total_shots = len(shots)
    for i, shot in enumerate(shots):
        s = NDCXII.data.Shotdata(shot)
        print(f"Working {i+1}/{total_shots}: shot {s.shotnumber}")
        # Collect all interest conductors. Cell1 is ignored
        srks = s.SRK
        bls = s.BL
        cells = s.Cells[1:]
        fcup = s.LargeFCupcollector
        arcI = s.sourcearcI

        # Collect voltage setting for each SRK
        for j, srk in enumerate(srks):
            volt_set = srk.voltagesetting
            srk_data[i, j] = volt_set

        # Collect time setting for each BL
        for j, bl in enumerate(bls):
            bl_time = bl.timing["trigger"]
            bl_data[i, j] = bl_time

        # Collect time setting for each cell
        for j, cell in enumerate(cells):
            cell_time = cell.timing["trigger"]
            cell_data[i, j] = cell_time

        # Collect arcI voltage reading
        dt = arcI.t[1] - arcI.t[0]
        tindex = np.where((arcI.t > 0.2 * ms - dt) & (arcI.t < 0.2 * ms + dt))[0][0]
        arcI_volt = arcI.x[tindex]
        arcI_data[i] = arcI_volt * 10

        # Handle Fcup data
        current_t = fcup.t
        current_I = fcup.x
        dt_I = current_t[1] - current_t[0]

        # Find the peak of the current profile
        t_max_index = np.argmax(current_I)
        t_max_time = current_t[t_max_index]

        # Find all points within a dt ns range around the peak
        t_mask = (current_t >= t_max_time - dt_keep) & (
            current_t <= t_max_time + dt_keep
        )
        time_good = current_t[t_mask]
        peak_time_now = np.mean(time_good)

        # Integrate the total charge in that window in nC
        beam_charge = dt_I * current_I[t_mask].sum()

        accum_Q[i] = beam_charge
        peak_times[i] = peak_time_now

        # Plot Fcup profile
        fcup.plot(ax=Fcup_ax)

    # Plot acuumulated charge, arcsource voltage and cost.
    cES_Q = accum_Q / arcI_data
    cost_ax[0].plot(accum_Q / nC)
    cost_ax[1].plot(arcI_data)
    cost_ax[2].plot(cES_Q / nC)
    cost_ax[2].set_xlabel("Step Number")

    cost_ax[0].set_ylabel("Q [nC]")
    cost_ax[1].set_ylabel("ArcI Voltage [V]")
    cost_ax[2].set_ylabel("Cost of Q/arcI_Volt [nC/V]")

    # Plot gap differences and solenoid voltages.
    for bl_ind in range(bl_data.shape[-1]):
        diff = bl_data[1:, bl_ind] - bl_data[0, bl_ind]
        cond_ax[0].plot(diff / ns)

    for cell_ind in range(cell_data.shape[-1]):
        diff = cell_data[1:, cell_ind] - cell_data[0, cell_ind]
        cond_ax[0].plot(diff / ns)

    for srk_ind in range(srk_data.shape[-1]):
        diff = srk_data[1:, srk_ind] - srk_data[0, srk_ind]
        cond_ax[1].plot(diff)

    cond_ax[2].plot(peak_times / us, alpha=0.3, lw=0.6, c="k")
    cond_ax[2].scatter([i for i in range(len(peak_times))], peak_times / us, c="k")
    cond_ax[2].axhline(
        y=target_time / us, c="r", label=f"Target Time {target_time / us:.3f}[us]"
    )

    cond_ax[2].set_ylabel("Arrival time [$\mu$s]")
    cond_ax[2].set_xlabel("Step Number")
    cond_ax[2].legend()
    cond_ax[0].set_ylabel(r"Gad Diff $\Delta$g [ns]")
    cond_ax[1].set_ylabel("Solenoid Voltage [V]")

    # Plot current profile of the Fcup
    Fcup_ax.set_title("Fcup Current Profile")
    fcup.plot(ax=Fcup_ax)

    plt.tight_layout()
    cost_fig.savefig(f"{yyyymm}-{comment}_expt-cost", dpi=400)
    cond_fig.savefig(f"{yyyymm}-{comment}_expt-conds", dpi=400)
    Fcup_fig.savefig(f"{yyyymm}-{comment}_expt-fcup", dpi=400)
    plt.show()


# ------------------------------------------------------------------------------
# This section will plot each column in the dataframe on the same plot in the
# hopes to spot ~unusual~ behavior. The plots will be saved to respective PDFs
# with the devices being in their own pdf file and the device information have
# their own page for the plot. For example, all SRK plots will go into one SRK
# PDF and the voltage, timing, etc, will be plotted on their own page.
# The bad shots are hand coded into a spreadsheet and then read in here.
# ------------------------------------------------------------------------------
# Read in dataframe created from scraper
data = pd.read_csv("12-01-2021_15-11-56_202111-dataframe.csv")
df = data.copy()

# Cycle through bad shots and drop from df
for shot in ignore_shots:
    df = df[df["Shot"] != shot]

plot_expt(202111, 301415, target_time=2.21 * us, ignore_shots=ignore_shots)

# Remove rows with zero shot number
df = df[df["Shot"] != 0]
df.to_csv(f"{yyyymm}-filtered_dataframe.csv")

# Switches for routines of various plot creations found below.
collect_current_profiles = False
plot_srks = False
plot_bls = False
plot_cells = False
plot_qtot_tmax = False
plot_normed_qtot = False
plot_fcup_waveforms = False

shots = df["Shot"].to_numpy()

# Uncomment section to save voltage and time arrays for LargeFCup
if collect_current_profiles:
    time_profile = []
    volt_profile = []

    for i, s in enumerate(shots):
        if s in ignore_shots:
            continue

        try:
            data = NDCXII.data.Shotdata(s)
            obj = data.LargeFCupcollector
            this_tprofile = obj.t
            this_Vprofile = obj.x

            time_profile.append(this_tprofile)
            volt_profile.append(this_Vprofile)
        except AssertionError:
            print(f"No Fcup data for shot {s}")

    time_profile = np.array(time_profile)
    volt_profile = np.array(volt_profile)
    np.save(f"{yyyymm}_Fcup_time_profile.npy", time_profile)
    np.save(f"{yyyymm}_Fcup_volt_profile.npy", volt_profile)

if plot_srks:
    srk_save = "SRKs_" + date_stamp + ".pdf"
    with PdfPages(srk_save) as pdf:
        # Create cover page
        # Make title page
        title = "SRK Peak Voltage and Time of Peak Voltage Plots"
        shot_month = str(yyyymm)
        date = datetime.datetime.today()
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
        plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
        plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
        pdf.savefig()
        plt.close()

        for srk_label in [f"SRK{i:02d}" for i in range(1, 29)]:
            srk_vset = df[srk_label + " " + "Vpeak"]
            srk_trigger = df[srk_label + " " + "tpeak"]
            row_index = [i for i in range(len(srk_trigger))]

            fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True)
            ax[0].scatter(row_index, srk_vset / kV, s=1)
            ax[1].scatter(row_index, srk_trigger / us, s=1)
            ax[0].plot(row_index, srk_vset / kV, alpha=0.3, lw=0.6, c="k")
            ax[1].plot(row_index, srk_trigger / us, alpha=0.3, lw=0.6, c="k")

            ax[0].set_title(srk_label + " Peak Voltage")
            ax[0].set_ylabel("Voltage [kV]")
            ax[1].set_title(srk_label + " Time of Peak Voltage")
            ax[1].set_ylabel(r"Time [$\mu s$]")
            ax[1].set_xlabel("Row Index in DataFrame")

            plt.tight_layout()
            pdf.savefig()
            plt.close()

# Repeat Process for Blumleins
if plot_bls:
    bl_save = "BLs_" + date_stamp + ".pdf"
    with PdfPages(bl_save) as pdf:
        # Create cover page
        # Make title page
        title = "Blumlein Peak Voltage and Time of Peak Voltage Plots"
        shot_month = str(yyyymm)
        date = datetime.datetime.today()
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
        plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
        plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
        pdf.savefig()
        plt.close()

        for bl_label in [f"BL{i:1d}" for i in range(1, 6)]:
            bl_vset = df[bl_label + " " + "Vpeak"]
            bl_trigger = df[bl_label + " " + "tpeak"]
            row_index = [i for i in range(len(srk_trigger))]

            fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True)
            ax[0].scatter(row_index, bl_vset / kV, s=1)
            ax[1].scatter(row_index, bl_trigger / us, s=1)
            ax[0].plot(row_index, bl_vset / kV, alpha=0.3, lw=0.6, c="k")
            ax[1].plot(row_index, bl_trigger / us, alpha=0.3, lw=0.6, c="k")

            ax[0].set_title(bl_label + " Peak Voltage")
            ax[0].set_ylabel("Voltage [kV]")
            ax[1].set_title(bl_label + " Time of Peak Voltage")
            ax[1].set_ylabel(r"Time [$\mu s$]")

            plt.tight_layout()
            pdf.savefig()
            plt.close()

# Repeat for cells
if plot_cells:
    cell_save = "Cells_" + date_stamp + ".pdf"
    with PdfPages(cell_save) as pdf:
        # Create cover page
        # Make title page
        title = "Cell Peak Voltage and Time of Peak Voltage Plots"
        shot_month = str(yyyymm)
        date = datetime.datetime.today()
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
        plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
        plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
        pdf.savefig()
        plt.close()

        for cell_label in [f"Cell{i:1d}" for i in range(1, 8)]:
            cell_vset = df[cell_label + " " + "Vpeak"].to_numpy()
            cell_trigger = df[cell_label + " " + "tpeak"].to_numpy()
            row_index = np.array([i for i in range(len(cell_trigger))])

            # Mask values where cells were set to 0 voltage. The scale can be
            # seen from the plot in the notes, where scale is order 10,000
            mask = cell_vset > 10

            fig, ax = plt.subplots(nrows=2, ncols=1, sharex=True)
            ax[0].scatter(row_index[mask], cell_vset[mask] / kV, s=1)
            ax[1].scatter(row_index[mask], cell_trigger[mask] / us, s=1)
            ax[0].plot(row_index[mask], cell_vset[mask] / kV, alpha=0.3, lw=0.6, c="k")
            ax[1].plot(
                row_index[mask], cell_trigger[mask] / us, alpha=0.3, lw=0.6, c="k"
            )

            ax[0].set_title(cell_label + " Peak Voltage")
            ax[0].set_ylabel("Voltage [kV]")
            ax[1].set_title(cell_label + " Time of Peak Voltage")
            ax[1].set_ylabel(r"Time [$\mu s$]")

            plt.tight_layout()
            pdf.savefig()
            plt.close()

# Repeat for total Q and time of peak current
if plot_qtot_tmax:
    q_save = "QandTmax_" + date_stamp + ".pdf"
    with PdfPages(q_save) as pdf:
        # Create cover page
        # Make title page
        title = "Large FCup Total Charge, Peak Voltage and Time of Peak Voltage"
        shot_month = str(yyyymm)
        date = datetime.datetime.today()
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
        plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
        plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
        pdf.savefig()
        plt.close()

        charge = df["Qtot"]
        tmax = df["tpeak"]
        Vpeak = df["Vpeak"]
        row_index = [i for i in range(len(charge))]

        fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)
        ax[0].scatter(row_index, charge / nC, s=1)
        ax[1].scatter(row_index, Vpeak / kV, s=1)
        ax[2].scatter(row_index, tmax / us, s=1)
        ax[0].plot(row_index, charge / nC, alpha=0.3, lw=0.6, c="k")
        ax[1].plot(row_index, Vpeak / kV, alpha=0.3, lw=0.6, c="k")
        ax[2].plot(row_index, tmax / us, alpha=0.3, lw=0.6, c="k")

        ax[0].set_title("Calculated Total Charge for Each Shot")
        ax[0].set_ylabel("Charge [nC]")
        ax[1].set_title("Peak Voltage for Each Shot")
        ax[1].set_ylabel("Voltage [kV]")
        ax[2].set_title("Time of Peak Voltage for Each Shot")
        ax[2].set_ylabel(r"Time [$\mu s$]")
        ax[2].set_xlabel("Row Index in DataFrame")

        plt.tight_layout()
        pdf.savefig()
        plt.close()

# Plot qtot normalized by arc source
if plot_normed_qtot:
    q_save = "NormedQ_" + date_stamp + ".pdf"
    with PdfPages(q_save) as pdf:
        # Create cover page
        # Make title page
        title = "Charge On Target Normalized by Arcsource"
        shot_month = str(yyyymm)
        date = datetime.datetime.today()
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
        plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
        plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
        pdf.savefig()
        plt.close()

        charge = df["Qtot"]
        arcI_V = df["ArcI V"]
        normedQ = charge / arcI_V
        row_index = np.array([i for i in range(len(charge))])

        # Sort out negative and zero values.
        mask = normedQ >= 0.0

        fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)
        ax[0].scatter(row_index[mask], charge[mask] / nC, s=1)
        ax[1].scatter(row_index[mask], arcI_V[mask], s=1)
        ax[2].scatter(row_index[mask], normedQ[mask] / nC, s=1)
        ax[0].plot(row_index[mask], charge[mask] / nC, alpha=0.3, lw=0.6, c="k")
        ax[1].plot(row_index[mask], arcI_V[mask], alpha=0.3, lw=0.6, c="k")
        ax[2].plot(row_index[mask], normedQ[mask] / nC, alpha=0.3, lw=0.6, c="k")

        ax[0].set_title("Calculated Total Charge for Each Shot")
        ax[0].set_ylabel("Charge [nC]")
        ax[1].set_title("Voltage Reading on Arcsource")
        ax[1].set_ylabel("Voltage [V]")
        ax[2].set_title("Charge Divided by Arcsource Voltage ")
        ax[2].set_ylabel(r"Charge/Voltage [nC/V]")
        ax[2].set_xlabel("Row Index in DataFrame")

        plt.tight_layout()
        pdf.savefig()
        plt.close()

# Plot all FCup waveforms on one plot excluding bad shots
if plot_fcup_waveforms:
    savename = date_stamp + "FCup_waveforms" + ".pdf"
    shots = df["Shot"].to_numpy()
    fig, ax = plt.subplots()
    ax.set_title("Large FCup Waveforms")
    for i, shot in enumerate(shots):
        print(f"Working Shot: {i+1}/{len(shots)}")
        shot_data = NDCXII.data.Shotdata(shot)
        object = shot_data.LargeFCupcollector
        object.plot(ax=ax, lw=0.5)
    plt.savefig(savename, dpi=400)

# xlim and ylim on plots
# xlim = (3.3954499999998973e-06, 3.835009999999996e-06)
# ylim = (-4.717031250000001, 42.44203125000001)
#
# waveforms_v = np.load("waveforms_v.npy")
# waveforms_t = np.load("waveforms_t.npy")
#
# this_df = pd.read_csv("this.csv")
# do_this = False
# if do_this:
#     this_save = "bad_interest_shots_LargeFCupcollector" + date_stamp + ".pdf"
#     with PdfPages(this_save) as pdf:
#         # Create cover page
#         # Make title page
#         title = "Bad/Interest Waveforms From LargeFCupcollect"
#         shot_month = str(yyyymm)
#         date = datetime.datetime.today()
#         plt.figure()
#         plt.axis("off")
#         plt.text(0.5, 0.5, "{}".format(title), ha="center", va="center")
#         plt.text(0.5, 0.3, "Date Made:{}".format(date), ha="center", va="center")
#         plt.text(0.5, 0.2, "For Runs YYYYMM : {}".format(shot_month))
#         pdf.savefig()
#         plt.close()
#
#         shot_inds = this_df["Index in Filtered DF"].to_numpy()
#
#         for i in shot_inds:
#             fig, ax = plt.subplots()
#             ax.set_xlabel("Times [s]")
#             t = waveforms_t[i]
#             v = waveforms_v[i]
#             ax.plot(t, v)
#             ax.set_title(f"Shot {shot_list[i]}, Index {i:3d}")
#             ax.axhline(y=10, c="k", ls="--", lw=0.1)
#             ax.axhline(y=20, c="k", ls="--", lw=0.1)
#             ax.axhline(y=30, c="k", ls="--", lw=0.1)
#             ax.axhline(y=40, c="k", ls="--", lw=0.1)
#             ax.set_ylabel("Voltage [V]")
#             ax.set_xlim(xlim)
#             ax.set_ylim(ylim)
#
#             plt.tight_layout()
#             pdf.savefig()
#             plt.close()
