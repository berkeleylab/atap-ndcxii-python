# Script to hold ignored shot numbers and comments. Originally, the shots
# were collected in the original data scraping and then parsed to find
# ignorable shots. These were collected in a spreadsheet and then later loaded
# in for ignoring. However, given that there are whole comments that can be
# ignored (script testing, experiment checks, etc.) this became inefficient and
# error prone.
# Instead, it is more useful to collect the ignored comments and shots in a
# seperate script such as this and the variables dictionaries can be called in.
# The comments can be created by manually entering comment numbers into the
# array.
# For shots, I figure the best organizational scheme would be to create a
# dictionary whose keys are the eperiment year-month, e.g. 202109 for the
# experiment ran in the year 2021 on September. This way, all shots are
# collectively stored and can be grabbed best on the experiment year-month.

import numpy as np

# ------------------------------------------------------------------------------
#     Ignore Comments
# The first comments typically represent testing the experiment and/or scripts
# before actually running. Some of these comments (like 202111-51018) look to
# have a decent amount of shots. However, for this particular comment the high
# voltage on the BLs was turned off and the arcsource and plotting routine were
# being tested which required numerous shots.
# ------------------------------------------------------------------------------
ignore_comments = {
    202112: np.array([30912, 31608], dtype=int,),
    202111: np.array(
        [
            21124,
            21143,
            50927,
            51018,
            51355,
            51709,
            291401,
            110950,
            110954,
            111103,
            111131,
            111427,
            111732,
            111452,
            111512,
            111519,
            111540,
            111745,
            111749,
            111756,
            111822,
            111823,
            111833,
            111842,
            111854,
            111908,
            111919,
            111926,
            111938,
            111959,
            112005,
            112015,
            112039,
            112053,
            112106,
            290959,
            291401,
            300957,
        ],
        dtype=int,
    ),
    202109: np.array(
        [
            161448,
            170930,
            170958,
            171038,
            171225,
            171228,
            171331,
            171416,
            171526,
            171648,
            171917,
        ],
        dtype=int,
    ),
    202103: np.array(
        [
            50936,
            51002,
            51304,
            51423,
            51456,
            111635,
            121003,
            121703,
            121707,
            121713,
            121749,
        ],
        dtype=int,
    ),
}


# ------------------------------------------------------------------------------
#     Ignore Shots
# The majority of shots correspond to data with unphysical results such as
# negative charge. There are also data points for which, for whatever reason,
# the data wasn't recorded and no values could be pulled from the data base.
# This is the main reason for the try and except blocks in the data scraping
# and analysis scripts.
# ------------------------------------------------------------------------------
ignore_shots = {
    202112: np.array(
        [
            3721407436,
            3721410314,
            3721399954,
            3721403249,
            3721403255,
            3721406508,
            3721407430,
            3721407717,
            3721409682,
            3721410308,
            3721410338,
            3721415055,
            3721420400,
            3721405081,
        ],
        dtype=int,
    ),
    202111: np.array([3721143674, 3721177922, 3721156525], dtype=int),
    202109: np.array(
        [
            3714744778,
            3714746243,
            3714746501,
            3714747431,
            3714753630,
            3714754353,
            3714758627,
            3714758799,
            3714758954,
            3714763184,
            3714764064,
            3714766823,
            3714770593,
            3714770644,
            3714773252,
            3714773459,
            3714773614,
            3714774978,
            3714775755,
            3714676505,
            3714743444,
            3714745115,
            3714745180,
            3714746707,
            3714749681,
            3714752354,
            3714752406,
            3714752530,
            3714755129,
            3714755324,
            3714758303,
            3714758470,
            3714758562,
            3714758960,
            3714758974,
            3714759030,
            3714759184,
            3714759746,
            3714760845,
            3714760857,
            3714761352,
            3714762431,
            3714767466,
            3714767517,
            3714767569,
            3714768344,
            3714768707,
            3714769799,
            3714769851,
            3714770039,
            3714770249,
            3714770955,
            3714771006,
            3714771058,
            3714772986,
            3714775604,
            3718996232,
        ],
        dtype=int,
    ),
    202103: np.array(
        [
            3697823090,
            3697811378,
            3697811469,
            3697811535,
            3697811578,
            3697811619,
            3698354168,
            3698354326,
            3697838811,
            3697831107,
            3698437171,
            3697815052,
            3697815000,
            3697815104,
            3697815436,
            3698815489,
            3697815576,
            3697816512,
            3697817250,
            3697827496,
            3697827548,
            3697829235,
            3697830538,
            3697831315,
            3698418524,
            3698442125,
            3698442296,
            3698442345,
            3698442396,
            3698442908,
            3697816778,
            3697817035,
            3697826103,
            3697826463,
            3697833048,
            3697833205,
            3697836732,
            3697840279,
            3697840695,
            3697813325,
            3697813378,
            3697813902,
        ],
        dtype=int,
    ),
}
