#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 12:14:10 2021

@author: Alexander Scheinker
"""

import time
import numpy as np
import matplotlib.pyplot as plt

import NDCXII

font_size = 12
plt.rcParams.update({"font.size": font_size})

# Useful Constants
ms = 1e-3
ns = 1e-9
kV = 1e3
nC = 1e-9
N_compr_cells = 7
N_BLs = 5
N_sols = 28


# ------------------------------------------------------------------------------
#    Utility Functions
# Define useful functions for calculations, making plots, or grabbing values.
# ------------------------------------------------------------------------------
def get_last_shot():
    """return the last shotnumber.

    Default to a working shotnumber for offline testing or if we
    haven't taken a shot yet today.

    """
    try:
        shotnumber = NDCXII.control.last_shot()
    except IndexError:
        shotnumber = 3683645181

    return shotnumber


def charge_near_peak(shotdata):
    """Cacluate charge delivered from current profile in 15 ns window"""

    # get time and current of last shotdata in the FCup
    current_t = shotdata.LargeFCupcollector.t
    current_I = shotdata.LargeFCupcollector.x

    dt_I = current_t[1] - current_t[0]

    # Find the peak of the current profile
    t_max_index = np.argmax(current_I)
    t_max_time = current_t[t_max_index]

    # Find all points within a 15 ns range around the peak
    dt_keep = 50 * ns
    t_good = (current_t >= t_max_time - dt_keep) & (current_t <= t_max_time + dt_keep)

    # Integrate the total charge in that window in nC
    beam_charge = dt_I * current_I[t_good].sum() / nC

    # fig, ax =plt.subplots()
    # ax.plot(current_t, current_I, color='black')
    # ax.plot(current_t[t_good], current_I[t_good], color='red')
    # plt.show(block=False)
    print(f"beam charge now = {beam_charge:.2f} [nC] {shotdata.shotnumber}")

    return beam_charge


def get_arcI_volt(shotdata, tselect=2 * ms):
    """Retrieve voltage reading for arcsource at specified time"""

    arcI = shotdata.sourcearcI
    dt = arcI.t[1] - arcI.t[0]
    tindex = np.where((arcI.t > 0.2 * ms - dt) & (arcI.t < 0.2 * ms + dt))[0][0]
    volt = arcI.x[tindex]

    return volt


def update_value(last, step, idx, dt, w, k, c_Q, a, rp, clip):
    if (-1) ** idx > 0:
        func = np.sin
    else:
        func = np.cos

    value = (
        last[idx]
        + a_scale
        * dt
        * func(step * dt * w[idx // 2] + rp[idx] - k * c_Q[step])
        * (a[idx] * w[idx // 2]) ** 0.5
    )
    value = np.clip(value, clip[0], clip[1])

    return value


# ------------------------------------------------------------------------------
#     ES Parameter Settings
# Here the various parameters: amplitude, freqeuncy, and cost are set.
# The first parameters are for the gaps which are 12 in total. The first 7 are
# compression cells followed by 5 blumleins. Only timing is varied.
# The solenoids parameters are then set for which there are 28 solenoids in
# total. Only voltage is varied.
# The metric for the cost function is the total charge delivered on target
# divided by the voltage reading from the arcsource. It was seen that the
# extremum seeking would fluctuate wildly (≈70% change in delivered charge).
# Investigation of the arc source showed that in cases with significant drop
# in delivered charge also had a relatively weak extraction from source.
# The correlation was approximately linear and normalized the total charge this
# way showed smooth/small oscillations.
# ------------------------------------------------------------------------------

# Total number of steps to take in tuning.
nES_steps = 100
a_scale = 1.0

# Keep track of the total cost function to be maximize which is the total charge
# in a 15 ns window around the current peak.
cES_Q = np.zeros(nES_steps)

# Track accumlated charge and voltage reading on arcsource
accum_Q = np.zeros(nES_steps)
arcI_voltage_read = np.zeros(nES_steps)

# Set up ES parameters for gaps
nES_params = N_compr_cells + N_BLs
gap_ES = np.zeros([nES_steps, nES_params])
wES = np.linspace(100, 175.149, nES_params // 2)
dtES = 2 * np.pi / (10.0 * np.max(wES))

# Array to hold amplitude scales for the cells and BLs. Values feed into the
# functional amplitude setting below.
d_cell = np.zeros(nES_params)
d_cell[0] = 0.0  # Compression Cell 1 typically not used.
d_cell[1:N_compr_cells] = 100 * ns  # Compression cells. Window for variation ≈100ns
d_cell[N_compr_cells:] = 20 * ns  # BL. Window for variation ≈70ns

# Set amplitude for gap timing variation
aES = 1.0 * np.max(wES) * (d_cell) ** 2

# Set feedback parameter on optimization of gaps.
kES = 0.3

# Set up parameter limits
pES_max = 3000
pES_min = 0.0


# Set up ES parameters for solenoids
nES_params_sol = 28
sol_ES = np.zeros([nES_steps, nES_params_sol])
wES_sol = np.linspace(100, 175.149, nES_params_sol // 2)
dtES_sol = 2 * np.pi / (21.0 * np.max(wES))

aES_sol = np.zeros(nES_params_sol) + 0 * 0.05 * np.max(wES_sol) * (100) ** 2
kES_sol = 0.0

# Set up parameter limits
pES_max_sol = 3000
pES_min_sol = 0

# Set random phase shifts for all gaps and solenoids.
rand_phase = np.pi * np.random.rand(nES_params) - np.pi / 2.0
rand_phase_sol = np.pi * np.random.rand(nES_params_sol) - np.pi / 2.0

# ------------------------------------------------------------------------------
#     Settings Initialization
# Pull data from selected shot and perform initial calculations to set initial
# conditions for main optimization loop.
# ------------------------------------------------------------------------------
# Set variable for retrieving last shot or starting from specific shot and then
# load settings into devices.
start_number = 3683645181
start_number = 3697819139
start_number = 3697826250
start_number = 3697827853
start_number = 3698421898
start_number = 3698426772
start_number = 3714743661
start_number = 3714753168
start_number = 3714764280
start_number = 3714770327
start_number = 3714773511
shotnumber = get_last_shot()
SRKs = NDCXII.control.load_setting(start_number, "SRKs")
timing_names = [f"Cell{i}" for i in range(1, 8)] + [f"BL{i}" for i in range(1, 6)]

# get initial values for timing and solenoids
gap_ES[0] = np.array(
    [NDCXII.control.get_timing(dev, start_number) for dev in timing_names]
)

# Good solenoid values to start with
sol_ES[0] = np.array(list(SRKs.values()))

# Initialize value of the cost function for optimization.
init_Q = charge_near_peak(NDCXII.data.Shotdata(start_number))
init_arcI_V = get_arcI_volt(NDCXII.data.Shotdata(start_number))
init_cES = init_Q / init_arcI_V
cES_Q[0] = init_cES
accum_Q[0] = init_Q
arcI_voltage_read[0] = init_arcI_V

# Save everything
np.save("cES_Q.npy", cES_Q)
np.save("gap_ES.npy", gap_ES)
np.save("sol_ES.npy", sol_ES)

# ------------------------------------------------------------------------------
#     Optimization/Control Main Loop
# Main loop that varies the controls of NDCXII based on the ES parameters and
# the cost function.
# ------------------------------------------------------------------------------

# Create two figures. First figure will hold the data for the total charge,
# voltage reading from arcsource, and cost. Second figure will hold the varying
# solenoid and gap settings.
cost_fig, cost_ax = plt.subplots(nrows=3, figsize=(8, 12), sharex=True)
cond_fig, cond_ax = plt.subplots(nrows=2, figsize=(8, 12), sharex=True)
Fcup_fig, Fcup_ax = plt.subplots(figsize=(8, 12))
plt.ion()
plt.show()

# Figure save strings
# today = datetime.datetime.today()
# date_string = today.strftime("%m-%d-%Y_%H-%M-%S_")

for jES in np.arange(nES_steps - 1):
    cost_fig.clf
    cond_fig.clf
    Fcup_fig.clf

    # Show progress
    print(f"starting ES step {jES+2}/{nES_steps}")

    # Update the gap parameters based on the Faraday cup
    for i in range(12):
        gap_ES[jES + 1, i] = update_value(
            gap_ES[jES],
            jES,
            i,
            dtES,
            wES,
            kES,
            cES_Q,
            aES,
            rand_phase,
            clip=[pES_min, pES_max],
        )

    # Update the solenoid parameters based on the Faraday cup
    for i in range(28):
        sol_ES[jES + 1, i] = update_value(
            sol_ES[jES],
            jES,
            i,
            dtES_sol,
            wES_sol,
            kES_sol,
            cES_Q,
            aES_sol,
            rand_phase_sol,
            clip=[pES_min_sol, pES_max_sol],
        )

    # Write solenoid voltage values to NDCX-II
    new_SRKs = {f"SRK{i+1:02d}": sol_ES[jES + 1, i] for i in range(28)}
    NDCXII.control.modify_SRK_voltages(new_SRKs)
    print(new_SRKs)

    # Create dictionary for CCs and BLs with updated timing values. Modify
    # these timings through the NDCXII controls.
    cc_dict = {f"Cell{i+1:1d}": gap_ES[jES + 1, i] for i in range(N_compr_cells)}
    bl_dict = {f"BL{i+1:1d}": gap_ES[jES + 1, i + N_compr_cells] for i in range(N_BLs)}

    # Combine dictionaries and feed into control modifications
    gap_dict = cc_dict.copy()
    gap_dict.update(bl_dict)
    NDCXII.control.modify_timing(gap_dict)

    # Print out new settings for gaps
    for key, val in zip(gap_dict.keys(), gap_dict.values()):
        print(f"{key} : {val / ns:.6f} [ns]")

    # Run NDCX-II
    print("Trigger NDCXII in 5 seconds", flush=True)
    time.sleep(5)
    NDCXII.control.trigger()
    print("Trigger!!!", flush=True)
    time.sleep(25)

    # Record data

    # Import the Faraday cup current profile and calculate charge in 3 ns window
    shotnumber = get_last_shot()
    shotdata = NDCXII.data.Shotdata(shotnumber)

    this_Q = charge_near_peak(shotdata)
    this_arcI_V = get_arcI_volt(shotdata) * 10
    cES_Q[jES + 1] = this_Q / this_arcI_V
    accum_Q[jES + 1] = this_Q
    arcI_voltage_read[jES + 1] = this_arcI_V

    # Save everything
    np.save("cES_Q.npy", cES_Q)
    np.save("gap_ES.npy", gap_ES)
    np.save("sol_ES.npy", sol_ES)

    # Vary the amplitude of oscillations each iteration
    # a_scale *= 0.995
    a_scale *= 1.0

    # Plot acuumulated charge, arcsource voltage and cost.
    cost_ax[0].plot(accum_Q[1 : jES + 2])
    cost_ax[1].plot(arcI_voltage_read[1 : jES + 2])
    cost_ax[2].plot(cES_Q[1 : jES + 2])
    cost_ax[2].set_xlabel("Step Number")

    cost_ax[0].set_ylabel("Q [nC]")
    cost_ax[1].set_ylabel("ArcI Voltage [V]")
    cost_ax[2].set_ylabel("Cost of Q/arcI_Volt [nC/V]")

    # Plot gap differences and solenoid voltages.
    cond_ax[0].plot((gap_ES[1 : jES + 2] - gap_ES[1]) / ns)
    cond_ax[1].plot((sol_ES[1 : jES + 2] - sol_ES[1]))
    cond_ax[1].set_xlabel("Step Number")
    cond_ax[0].set_ylabel(r"Gad Diff $\Delta$g [ns]")
    cond_ax[1].set_ylabel("Solenoid Voltage [V]")

    # Plot current profile of the Fcup
    Fcup_ax.set_title("Fcup Current Profile")
    shotdata.LargeFCupcollector.plot(ax=Fcup_ax)

    plt.tight_layout()
    plt.draw()
    plt.pause(0.25)

    # if jES > 20:
    # cost_fig.savefig(date_string + "cost", dpi=400)
    # cond_fig.savefig(date_string + "conductors", dpi=400)
    # fcup_fig.savefig(date_string + "Fcup", dpi=400)

    print("Waiting 20s befor next shot", flush=True)
    time.sleep(20)
