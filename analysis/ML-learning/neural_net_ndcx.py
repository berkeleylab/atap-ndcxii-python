# Neural Network for the NDCXII data that was adpated from an original script
# by Alex Scheinker. This particular script trains on data to predice the peak
# voltage and the time of the peak voltage from the Faraday cup voltage
# profiles.

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from matplotlib.backends.backend_pdf import PdfPages


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from sklearn.preprocessing import StandardScaler, QuantileTransformer

font_size = 12
plt.rcParams.update({"font.size": font_size})

os.environ["KMP_DUPLICATE_LIB_OK"] = "True"
PATH_TO_DATA = "/Users/nickvalverde/Desktop/"

# Initial data loading
# Load the voltage data
yyyymm = 202103
volt_profiles = np.load(PATH_TO_DATA + f"{yyyymm}_Fcup_volt_profile.npy")
# Load the time data
t_profiles = np.load(PATH_TO_DATA + f"{yyyymm}_Fcup_time_profile.npy")
# Load the data frame with solenoids, cells, and gaps
df = pd.read_csv(PATH_TO_DATA + f"{yyyymm}-filtered_dataframe.csv")
if yyyymm == 202103:
    # Drop negative Q shots for 202103. Quick fix for now.
    df.drop(index=[3, 4, 6], axis=0, inplace=True)
    volt_profiles = np.delete(volt_profiles, [3, 4, 6], axis=0)
    t_profiles = np.delete(t_profiles, [3, 4, 6], axis=0)
# ------------------------------------------------------------------------------
#     Constants and Parameters
# Useful constants for the script and general parameters are defined here.
# ------------------------------------------------------------------------------
ns = 1e-9
us = 1e-6
ms = 1e-3
nC = 1e-9
kV = 1e3
keV = 1e3

# Define number of compression cells, blumleins, and solenoids
N_compr_cells = 7
N_bls = 5
N_sols = 28
N_shots = len(volt_profiles)
# ------------------------------------------------------------------------------
#     Functions
# Utility functions such as plotting and analysis are defined here. Other
# functions that are required by the script should be put here.
# ------------------------------------------------------------------------------
def plot_bl_hist(data, title="Blumlein Data", nbins=50, xlabel="", ylabel="", save=""):
    """Make a Histogram of the data given for each blumlein

    The primary data for the blumleins are the peak voltage and the time when
    the voltage peaked. Either one of these array scan be input as data and this
    array is assumed to be a 1D array containg the float value.
    """
    # There are 5 blumleins so the data will be put on 5-panel plot
    fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(15, 7))
    for ax, bl in zip(axes.ravel(), data):
        ax.hist(bl, bins=nbins, edgecolor="k", lw=0.5)

    # Check labels. Only put labels on first plot since the other plots will
    # be the same.
    if len(xlabel) > 0:
        axes[0].set_xlabel(xlabel)
    if len(ylabel) > 0:
        axes[0].set_ylabel(ylabel)

    fig.suptitle(title)
    plt.tight_layout()
    if len(save) > 0:
        plt.savefig(save, dpi=400)


# ------------------------------------------------------------------------------
#     Data Shaping/Handling
# Here, the data is extracted form the data set and the data arrays for various
# objects (solenoids, cells, blumleins, etc) are defined. Any other data
# manipulations should be performed here, such as dropping data points, shifting
# values, normalization schemes, etc.
# ------------------------------------------------------------------------------
# Load all the solenoid voltage and time data
S_all_vpeak = np.zeros(shape=(N_shots, N_sols))
S_all_tpeak = np.zeros(shape=(N_shots, N_sols))
for n in range(N_sols):
    S_all_vpeak[:, n] = df[f"SRK{n+1:02d} Vpeak"].to_numpy()
    S_all_tpeak[:, n] = df[f"SRK{n+1:02d} tpeak"].to_numpy()

bl_vpeak = np.zeros(shape=(N_shots, N_bls))
bl_tpeak = np.zeros(shape=(N_shots, N_bls))
for n in range(N_bls):
    bl_vpeak[:, n] = df[f"BL{n+1:1d} Vpeak"].to_numpy()
    bl_tpeak[:, n] = df[f"BL{n+1:1d} tpeak"].to_numpy()

compr_cell_vpeak = np.zeros(shape=(N_shots, N_compr_cells))
compr_cell_tpeak = np.zeros(shape=(N_shots, N_compr_cells))
for n in range(N_compr_cells):
    compr_cell_vpeak[:, n] = df[f"Cell{n+1:1d} Vpeak"].to_numpy()
    compr_cell_tpeak[:, n] = df[f"Cell{n+1:1d} tpeak"].to_numpy()

Q_tot = df["Qtot"].to_numpy()
arcI = df["ArcI V"].to_numpy()

# Turn any NaN values to 0
S_all_vpeak = np.nan_to_num(S_all_vpeak)
S_all_tpeak = np.nan_to_num(S_all_tpeak)
bl_vpeak = np.nan_to_num(bl_vpeak)
bl_tpeak = np.nan_to_num(bl_tpeak)
compr_cell_vpeak = np.nan_to_num(compr_cell_vpeak)
compr_cell_tpeak = np.nan_to_num(compr_cell_tpeak)


# Find time at which the voltage waveform is maximum for each shot
t_max_index = np.argmax(volt_profiles, 1)
t_max = np.zeros(N_shots)

for i, index in enumerate(t_max_index):
    this_tprofile = t_profiles[i, :]
    t_max[i] = this_tprofile[index]

# ------------------------------------------------------------------------------
#     Visualize the data
# Here, the various data arrays are visualized primarily through histograms.
# This is useful in seeing the spread of data and seeing the extent of the
# parameter space. This is also useful in indicating how limited the parameter
# space is. For a comprehensive NN, there needs to be a spread of data availble
# for comprehensive learning. But, if the dataset is constrained to a limited
# range of sampling, the NN cannot be broadly trained.
# The plots are collected into a single pdf file at the end of this section.
# ------------------------------------------------------------------------------

# Set the save string for the pdf file
make_summary_pdfs = False
if make_summary_pdfs:
    pdf_save = "input_data_summary.pdf"
    with PdfPages(pdf_save) as pdf:
        # Create cover/title page with some information
        title = "Summary of input data before NN procedure"
        plt.figure()
        plt.axis("off")
        plt.text(0.5, 0.5, f"{title}", ha="center", va="center")
        plt.text(0.5, 0.3, f"Data from {yyyymm} Run", ha="center", va="center")
        pdf.savefig()
        plt.close()

        # Bin and display voltage and time data for the solenoids
        fig, axes = plt.subplots(nrows=7, ncols=4, figsize=(15, 7))
        for i, (ax, sol) in enumerate(zip(axes.ravel(), S_all_vpeak.T)):
            ax.hist(sol, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"Sol {i+1:02d}", fontsize="x-small")

        fig.suptitle(
            "Distribution of Recorded Peak Voltage [V] for Solenoids", fontsize="small"
        )
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Histogram solenoid times
        fig, axes = plt.subplots(nrows=7, ncols=4, figsize=(15, 7))
        for i, (ax, sol) in enumerate(zip(axes.ravel(), S_all_tpeak.T)):
            ax.hist(sol / us, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"Sol {i+1:02d}", fontsize="x-small")

        fig.suptitle(r"Time of Peak Voltage [$\mu$s] for Solenoids", fontsize="small")
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Histogram blumlein peak voltages and times
        fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(15, 7))
        for i, (ax, bl) in enumerate(zip(axes.ravel(), bl_vpeak.T)):
            ax.hist(bl / kV, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"BL {i+1:1d}", fontsize="x-small")

        # Turn off axis for blank plot
        axes[-1, -1].axis("off")

        fig.suptitle(
            "Distribution of Recorded Peak Voltage [kV] for Blumleins", fontsize="small"
        )
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(15, 7))
        for i, (ax, bl) in enumerate(zip(axes.ravel(), bl_tpeak.T)):
            ax.hist(bl / us, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"BL {i+1:1d}", fontsize="x-small")
        axes[-1, -1].axis("off")

        fig.suptitle(r"Time of Peak Voltage [$\mu$s] for Blumleins", fontsize="small")
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        # Histogram compression cell peak voltages and times
        fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(15, 7))
        for i, (ax, cell) in enumerate(zip(axes.ravel(), compr_cell_vpeak.T)):
            ax.hist(cell / kV, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"Cell {i+1:1d}", fontsize="x-small")

        # Turn off axis for blank plot
        axes[-1, -1].axis("off")

        fig.suptitle(
            "Distribution of Recorded Peak Voltage [kV] for Compression Cells",
            fontsize="small",
        )
        plt.tight_layout()
        pdf.savefig()
        plt.close()

        fig, axes = plt.subplots(nrows=4, ncols=2, figsize=(15, 7))
        for i, (ax, cell) in enumerate(zip(axes.ravel(), compr_cell_tpeak.T)):
            ax.hist(cell / us, bins=50, edgecolor="k", lw=0.5)
            ax.set_title(f"Cell {i+1:1d}", fontsize="x-small")
        axes[-1, -1].axis("off")

        fig.suptitle(
            r"Time of Peak Voltage [$\mu$s] for Compression Cells", fontsize="small"
        )
        plt.tight_layout()
        pdf.savefig()
        plt.close()

# ------------------------------------------------------------------------------
#     Neural Net Train/Split Input Data Preprocessing
# Here, the data is partitioned into training and validation data. A scaling
# function is then used to bound the data between -1 and 1 (typically) for
# improved learning. The data is then transformed using the scaling function.
# ------------------------------------------------------------------------------
# Specify number of points shot to use as the validation set
n_data = N_shots
n_val = 30

# Randomize the indices
indx_rand = np.arange(n_data)
np.random.shuffle(indx_rand)

# Creat arrays holding the indices for the training and validation
indx_train = indx_rand[0 : (n_data - n_val)]
indx_val = indx_rand[(n_data - n_val) :]

# Initialize training data array for solenoids, cells, and blumleins
x_sol_train_t = S_all_tpeak[indx_train, :]
x_cell_train_t = compr_cell_tpeak[indx_train, :]
x_bl_train_t = bl_tpeak[indx_train, :]

x_sol_train_v = S_all_vpeak[indx_train, :]
x_cell_train_v = compr_cell_vpeak[indx_train, :]
x_bl_train_v = bl_vpeak[indx_train, :]

# Initialize validation data array for solenoids, cells, and blumleins
x_sol_val_t = S_all_tpeak[indx_val, :]
x_cell_val_t = compr_cell_tpeak[indx_val, :]
x_bl_val_t = bl_tpeak[indx_val, :]

x_sol_val_v = S_all_vpeak[indx_val, :]
x_cell_val_v = compr_cell_vpeak[indx_val, :]
x_bl_val_v = bl_vpeak[indx_val, :]

# Initialize the scaling function to be used and transform the data
sol_ss_t = StandardScaler().fit(x_sol_train_t)
sol_ss_v = StandardScaler().fit(x_sol_train_v)
cell_ss_t = StandardScaler().fit(x_cell_train_t)
cell_ss_v = StandardScaler().fit(x_cell_train_v)
bl_ss_t = StandardScaler().fit(x_bl_train_t)
bl_ss_v = StandardScaler().fit(x_bl_train_v)

# Use scalers to normalize the training data
normed_x_sol_train_t = sol_ss_t.transform(x_sol_train_t)
normed_x_sol_train_v = sol_ss_v.transform(x_sol_train_v)
normed_x_sol_val_t = sol_ss_t.transform(x_sol_val_t)
normed_x_sol_val_v = sol_ss_v.transform(x_sol_val_v)

normed_x_cell_train_t = cell_ss_t.transform(x_cell_train_t)
normed_x_cell_train_v = cell_ss_v.transform(x_cell_train_v)
normed_x_cell_val_t = cell_ss_t.transform(x_cell_val_t)
normed_x_cell_val_v = cell_ss_v.transform(x_cell_val_v)

normed_x_bl_train_t = bl_ss_t.transform(x_bl_train_t)
normed_x_bl_train_v = bl_ss_v.transform(x_bl_train_v)
normed_x_bl_val_t = bl_ss_t.transform(x_bl_val_t)
normed_x_bl_val_v = bl_ss_v.transform(x_bl_val_v)

# ------------------------------------------------------------------------------
#     Neural Net Train/Split Target Data Preprocessing
# Here, the target data is identified, partitioned into training and validation,
# and normaled using some type of scaler. The target data will be the peak
# voltage and the time of the peak recorded on the Faraday cup. This can be
# changed, and most likely will be, but is a useful baseline target.
# ------------------------------------------------------------------------------

# Training and validation for voltage profiles
volt_profiles_train = volt_profiles[indx_train, :]
volt_profiles_val = volt_profiles[indx_val, :]

# Training and validation time profiles
t_profiles_train = t_profiles[indx_train, :]
t_profiles_val = t_profiles[indx_val, :]

# Create arrays holding max voltage and times for training and validation. Loop
# through corresponding profile arrays and append maxes
volt_max_train = np.zeros(len(indx_train))
volt_max_val = np.zeros(n_val)
t_max_train = np.zeros(len(indx_train))
t_max_val = np.zeros(n_val)

# Loop through training data
for i, (volt_profile, t_profile) in enumerate(
    zip(volt_profiles_train, t_profiles_train)
):
    # Grab max voltage and index of max
    max_index = np.argmax(volt_profile)
    this_volt_max = volt_profile[max_index]
    this_t_max = t_profile[max_index]

    volt_max_train[i] = this_volt_max
    t_max_train[i] = this_t_max

# Repeat process for validation data
for i, (volt_profile, t_profile) in enumerate(zip(volt_profiles_val, t_profiles_val)):
    # Grab max voltage and index of max
    max_index = np.argmax(volt_profile)
    this_volt_max = volt_profile[max_index]
    this_t_max = t_profile[max_index]

    volt_max_val[i] = this_volt_max
    t_max_val[i] = this_t_max

# Divide voltage data by the reading off the arcsource as an a priori normalizaton
volt_max_train /= arcI[indx_train]
volt_max_val /= arcI[indx_val]

# Turn 1D target arrays in 2D arrays to match expected shape of N-obs by N-features
volt_max_train = volt_max_train[:, np.newaxis]
volt_max_val = volt_max_val[:, np.newaxis]
t_max_train = t_max_train[:, np.newaxis]
t_max_val = t_max_val[:, np.newaxis]


# Normalize targets using Quantile Transformer. The QT transformer is good at
# treating outliers in the data which is why it is used here. The target arrays
# are maded into a 2D array in the transform to be commensurate with the
# expected shape of N-observations by N-features
volt_qt = QuantileTransformer(n_quantiles=700, output_distribution="normal").fit(
    volt_max_train
)
time_qt = QuantileTransformer(n_quantiles=700, output_distribution="uniform").fit(
    t_max_train
)
normed_volt_max_train = volt_qt.transform(volt_max_train)
normed_t_max_train = time_qt.transform(t_max_train)
normed_volt_max_val = volt_qt.transform(volt_max_val)
normed_t_max_val = time_qt.transform(t_max_val)

# Visualize target data before and after transformation.
fig, axes = plt.subplots(nrows=2, ncols=2)
axes[0, 0].hist(volt_max_train, bins=30, lw=0.5, edgecolor="k")
axes[0, 1].hist(t_max_train / us, bins=30, lw=0.5, edgecolor="k")
axes[0, 0].set_xlabel("FCup Peak Voltage Normed by Arcsource")
axes[0, 1].set_xlabel(r"Time of FCup Peak Voltage [$\mu$s]")

axes[1, 0].hist(normed_volt_max_train, bins=30, lw=0.5, edgecolor="k")
axes[1, 1].hist(normed_t_max_train, bins=30, lw=0.5, edgecolor="k")
axes[1, 0].set_xlabel("Transformed FCup Peak Voltage")
axes[1, 1].set_xlabel(r"Transformed Time of Peak FCup Voltage")

fig.suptitle("Target Data Prenormalization (Top Row) and After (Bottom)")
plt.tight_layout()
plt.show()

# ------------------------------------------------------------------------------
#     Neural Net Architecture
# Here, the neural net layers are createds
# ------------------------------------------------------------------------------
# Specify the input shapes for the input data which will be the solenoids,
# blumeleins, and compression cells.
# Solenoid value inputs
in_sol_volt = keras.Input(shape=(28,))
in_sol_t = keras.Input(shape=(28,))

# Blumlein value inputs
in_bl_volt = keras.Input(shape=(5,))
in_bl_t = keras.Input(shape=(5,))

# Compression Cell value inputs
in_cell_volt = keras.Input(shape=(7,))
in_cell_t = keras.Input(shape=(7,))

# Regularizer weight
wL2 = 1e-5

# Specify the length of the output vectors which will be the max voltage and time
len_out = len(volt_max_train)

# Create the layers for training the inputs
# Solenoid layers for voltage and time
dense_S1_volt = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_sol_volt)
dense_S2_volt = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S1_volt)
dense_S3_volt = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S2_volt)

dense_S1_t = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_sol_t)
dense_S2_t = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S1_t)
dense_S3_t = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_S2_t)


# Blumlein layers for voltage and time
dense_B1_volt = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_bl_volt)
dense_B2_volt = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_B1_volt)
dense_B3_volt = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_B2_volt)

dense_B1_t = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_bl_t)
dense_B2_t = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_B1_t)
dense_B3_t = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_B2_t)


# Compression Cell layers for voltage and time
dense_CC1_volt = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_cell_volt)
dense_CC2_volt = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_CC1_volt)
dense_CC3_volt = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_CC2_volt)

dense_CC1_t = layers.Dense(
    200, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2),
)(in_cell_t)
dense_CC2_t = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_CC1_t)
dense_CC3_t = layers.Dense(
    50, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_CC2_t)


# Combine inputs
dense_SG = tf.concat(
    [dense_S3_volt, dense_S3_t, dense_B3_volt, dense_B3_t, dense_CC3_volt, dense_CC3_t],
    1,
)

# Main layers to predict the peak voltage and time of max peak voltage
dense_SG1 = layers.Dense(
    100, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_SG)
dense_SG2 = layers.Dense(
    250, activation="selu", kernel_regularizer=tf.keras.regularizers.L2(l2=wL2)
)(dense_SG1)
dense_SG3 = layers.Dense(2, activation="linear")(dense_SG2)

model_NDCX_data = keras.Model(
    inputs=[in_sol_volt, in_sol_t, in_bl_volt, in_bl_t, in_cell_volt, in_cell_t],
    outputs=[dense_SG3],
    name="NDCX_II_data",
)
model_NDCX_data.summary()


# ------------------------------------------------------------------------------
#     Neural Net Training
# Here, the neural net is compiled and trained for a set number of epochs.
# ------------------------------------------------------------------------------
# Train the neural network
n_epochs = 60
b_size = 16

opt = keras.optimizers.Adam(learning_rate=5e-6)

model_NDCX_data.compile(
    loss="mse", optimizer=opt, metrics=[tf.keras.metrics.RootMeanSquaredError()]
)

# Specify the input data and targets for the training set
in_train = [
    normed_x_sol_train_v,
    normed_x_sol_train_t,
    normed_x_bl_train_v,
    normed_x_bl_train_t,
    normed_x_cell_train_v,
    normed_x_cell_train_t,
]
in_train_target = [normed_volt_max_train, normed_t_max_train]

# Specify the input data and targets for the validation set
in_val = [
    normed_x_sol_val_v,
    normed_x_sol_val_t,
    normed_x_bl_val_v,
    normed_x_bl_val_t,
    normed_x_cell_val_v,
    normed_x_cell_val_t,
]
in_val_target = [normed_volt_max_val, normed_t_max_val]

# Fit the model to the input data
history = model_NDCX_data.fit(
    x=in_train,
    y=in_train_target,
    batch_size=b_size,
    epochs=n_epochs,
    validation_data=(in_val, in_val_target),
)

print("*******************************")
print("*******************************")
print("*******************************")
print("Finished step 1 of training")
print("*******************************")
print("*******************************")
print("*******************************")

plt.figure()
plt.plot(history.history["loss"], label="$train$")
plt.plot(history.history["val_loss"], label="$val$")
plt.legend(frameon=False)
plt.xlabel("step")
plt.ylabel("loss")
plt.tight_layout()
plt.show()

# Show predictions against experiment
predictions = model_NDCX_data.predict(
    [
        normed_x_sol_val_v,
        normed_x_sol_val_t,
        normed_x_bl_val_v,
        normed_x_bl_val_t,
        normed_x_cell_val_v,
        normed_x_cell_val_t,
    ]
)
pred_volt, pred_t = predictions[:, 0], predictions[:, 1]

# Invert the transformation applied to input data.
inv_pred_volt = volt_qt.inverse_transform(pred_volt[:, np.newaxis])
inv_pred_t = time_qt.inverse_transform(pred_t[:, np.newaxis])

# Make scatter plot of predicted in red and experiment in black
fig, ax = plt.subplots()
ax.scatter(inv_pred_t / us, inv_pred_volt, c="r", label="Predicted")
ax.scatter(t_max_val / us, volt_max_val, c="b", label="Experiment")
ax.legend()
ax.set_xlabel(r"Time of Peak Voltage [$\mu$s]")
ax.set_ylabel(r"Peak Voltage / Arcsource Voltage")
plt.show()
