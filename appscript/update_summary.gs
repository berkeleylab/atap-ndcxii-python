function update_summary() {
  var doc = DocumentApp.getActiveDocument();
  var header = doc.getHeader() || doc.addHeader();
  var body = doc.getBody();

  body.clear();
  header.clear();

  var currentTime = new Date();
  var para = header.appendParagraph('This document is created automatically -- do not edit\n'+currentTime.toLocaleString());
  para.setAlignment(DocumentApp.HorizontalAlignment.CENTER)
  header.appendHorizontalRule();

  var ndcxiifolder = DriveApp.getFolderById('19lWlQOONJ4WSw30p_FHmvLMLzfFGyNhd');
  var files = ndcxiifolder.getFiles();
  var logbooks = [];
  while(files.hasNext()) {
    var file = files.next();
    var id = file.getId();
    Logger.log(id)
    try{
      var iterdoc = DocumentApp.openById(id);
    }
    catch(err){
      continue;
    };
    var iterbody = iterdoc.getBody();

    var searchType = DocumentApp.ElementType.PARAGRAPH;
    var searchHeading = DocumentApp.ParagraphHeading.HEADING1;
    var searchResult = null;

    var filename = iterdoc.getName();
    if(!filename.match(/20[0-9][0-9]-[0-9][0-9]/)) {
      continue;
    };

    while (searchResult = iterbody.findElement(searchType, searchResult)) {
      var par = searchResult.getElement().asParagraph();
      if (par.getHeading() == searchHeading)
        {
          var name = searchResult.getElement().asText().getText();
          var header = {};
          header['file'] = filename;
          header['id'] = iterdoc.getId();
          header['name'] = name;
          logbooks.push(header);
        };
    };
  };

  logbooks.sort(function(a,b){return a.name < b.name? 1: -1;});

  for (i=0, len=logbooks.length; i<len; i++) {
    var header = logbooks[i];
    var par = body.appendParagraph(' ');
    var file = par.insertText(0, header.file);
    file.setLinkUrl('https://docs.google.com/document/d/'+header.id);
    if (header.name)
      par.insertText(par.getNumChildren(), '  '+header.name);
    else
      par.insertText(par.getNumChildren(), '   no header name');
  }
}
