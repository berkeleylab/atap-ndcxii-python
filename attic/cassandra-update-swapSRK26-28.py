from NDCXII.db import Shot, Comments, Data, Setting
import NDCXII.db as DB
import NDCXII.data as ND
import NDCXII.helper as NDH

import cqlengine
import sys

from table_iterator import TableIterator
import json

DB.init()

j = 0
fixedSRKs = 0
added = 0

oldTS = 0
for i in TableIterator(Shot, blocksize=100):
    j += 1
    # keep some progress report
    if j % 100 == 0:
        print("x ", j, i.lvtimestamp, i.devicename, fixedSRKs)

    sn = i.lvtimestamp

    # add TS to daily comment
    if sn != oldTS:
        oldTS = sn

        # check for SRK values, swap 26<->28 due to bug in LV
        s = Shot.objects(lvtimestamp=sn, devicename="SRKs").first()
        if s:
            h = s.settinghash

            setting = Setting.objects(Setting.hash == h).first()
            data = setting.data

            # convert to int
            new = json.loads(data)
            new["SRK26"], new["SRK28"] = new["SRK28"], new["SRK26"]
            new = json.dumps(new)

            Setting.objects(hash=h).update(data=new)
            fixedSRKs += 1
